#!/bin/bash

SRCDIR=./images/

OUTDIR=././../../ooh/http/static/images/icons

python -m ooh.util.icon ${SRCDIR}

# Smaller icons on a white background that fit better in a bubble
inkscape -f "${SRCDIR}/clock-10-10.svg" -e "${OUTDIR}/logo.png" -a -20:-20:120:120 -b "#fff"
inkscape -f "${SRCDIR}/clock-dark-10-10.svg" -e "${OUTDIR}/logo-dark.png" -a -20:-20:120:120
inkscape -f "${SRCDIR}/clock-night-10-10.svg" -e "${OUTDIR}/logo-night.png" -a -20:-20:120:120

# Full text logos
inkscape -f "${SRCDIR}/ooh-phy.svg" -e "${OUTDIR}/ooh-phy.png" -h 50
inkscape -f "${SRCDIR}/ooh-phy-dark.svg" -e "${OUTDIR}/ooh-phy-dark.png" -h 50
inkscape -f "${SRCDIR}/ooh-phy-night.svg" -e "${OUTDIR}/ooh-phy-night.png" -h 50

# Full text logos
inkscape -f "${SRCDIR}/ooh-phy.svg" -e "${OUTDIR}/ooh-phy-lg.png" -h 400
inkscape -f "${SRCDIR}/ooh-phy-dark.svg" -e "${OUTDIR}/ooh-phy-dark-lg.png" -h 400
inkscape -f "${SRCDIR}/ooh-phy-night.svg" -e "${OUTDIR}/ooh-phy-night-lg.png" -h 400

# favicons and pngs
for src_file in ${SRCDIR}/clock-??-??.svg
do
    BASE=$(basename ${src_file} .svg)
    inkscape -f ${src_file} -e ico_x16.png -w16 -h16
    inkscape -f ${src_file} -e ico_x32.png -w32 -h32
    inkscape -f ${src_file} -e ico_x48.png -w48 -h48
    convert ico_x16.png ico_x32.png ico_x48.png -compress zip ${OUTDIR}/${BASE}.ico
    inkscape -f ${src_file} -e ${OUTDIR}/${BASE}.png -w152 -h152
    rm ico_x16.png ico_x32.png ico_x48.png
done

mv -v ${SRCDIR}/*.svg ${OUTDIR}

rmdir ${SRCDIR}
