"""
ooh.phy

Backend for building sign in system. Operates as a general Building Occupancy
Register, an Out of Hours sign in book for outside working hours, or as a
managed Visitor sign-in system.
"""

from setuptools import setup, find_packages

import ooh

IBISCLIENT_SRC = (
    "git+https://gitlab.developers.cam.ac.uk/uis/sysdev/devgroup/ibis/ibis-client.git"
)


setup(
    name="flask.ooh",
    version=ooh.__version__,
    description="flask.ooh.phy",
    long_description=__doc__,
    author="Tom Daff",
    author_email="tdd20@cam.ac.uk",
    license="BSD",
    url="https://codeshare.phy.cam.ac.uk/itservices.phy/ooh.phy/flask.ooh.phy",
    packages=find_packages(exclude=["tests"]),
    entry_points={"console_scripts": ["ooh = ooh.__main__:main"]},
    package_data={
        "ooh": [
            "http/templates/*",
            "http/static/*/*",
            "http/static/*/*/*",
            "http/static/*/*/*/*",
            "http/static/*/*/*/*/*",
            "migrations/*",
            "migrations/*/*",
            "migrations/*/*/*",
        ]
    },
    scripts=["ooh.wsgi"],
    install_requires=[
        "alembic",
        "bs4",
        "cachelib",
        "click>=7.0",
        "flask",
        "ibisclient @ {}".format(IBISCLIENT_SRC),
        "lxml",
        "pygtail",  # required for monitoring script
        "qrcode",
        "requests",
        "sqlalchemy",
    ],
    extras_require={
        "test": ["pytest>=3.9", "pytest-flask>=0.11"],
        "postgres": ["psycopg2"],
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: BSD License",
    ],
)
