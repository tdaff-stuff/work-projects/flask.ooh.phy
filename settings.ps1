$env:FLASK_APP = "ooh.http.app"
$env:SITE_CONFIG = "$PSScriptRoot\site_config.py"
Write-Host "FLASK_APP=$env:FLASK_APP"
Write-Host "SITE_CONFIG=$env:SITE_CONFIG"
if (Test-Path "$PSScriptRoot\venv\Scripts\Activate.ps1" -Type Leaf)
{
    &$PSScriptRoot\venv\Scripts\Activate.ps1
    Write-Host "VIRTUAL_ENV=$env:VIRTUAL_ENV"
}
elseif (Test-Path "$PSScriptRoot\.venv\Scripts\Activate.ps1" -Type Leaf )
{
    &$PSScriptRoot\.venv\Scripts\Activate.ps1
    Write-Host "VIRTUAL_ENV=$env:VIRTUAL_ENV"
}