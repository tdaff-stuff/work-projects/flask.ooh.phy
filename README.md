# Building Occupancy and Out of Hours Register with Visitor Sign-In

[ooh.phy](https://ooh.phy.cam.ac.uk/) is the system developed for use in
the Cavendish Laboratory for keeping track of people on-site, both
during and outside of working hours, in order to provide a register with
emergency contacts that can be used by the emergency printout system.

The name ooh.phy is inherited from the original purpose, which was
providing list of occupants during Out of Hours (OOH) working that could
be used by safety teams in the case of an emergency. It was later
generalised so that it could be used at all times to monitor building
occupancy, and would provide useful information for track and trace
procedures as part of pandemic re-opening protocols. The code still
references `ooh` as the core module, but the functionality is much
broader.

During the daytime, the system can be switched into a visitor management
mode that can store details associated with temporarily issued access
cards and managed by administrative staff.

Both modes are designed to be interacted with via our
[university-card-reading kiosks](https://codeshare.phy.cam.ac.uk/itservices.phy/kiosk.phy).
The individual card readers can be used to sign in and out, or manage
the system and users, and the touchscreen and onscreen keyboard can be
used to enter all the details required.

`flask.ooh.phy` is the server application written in Python using Flask
to provide the interface for kiosks and through a Raven authenticated
website.

## UPDATE: *Reduced contact modes*

As of May 2020 each kiosk can now be set to operate in a low-contact or
no-contact mode. Instead of prompting to update details each time a user
scans their card, they will be directed to use the website to edit their
details. In low-contact mode some interaction is still available, but
not encouraged; in no-contact mode all interaction is removed from the
kiosk.

These modes will hopefully reduce unnecessary contact as a safety
measure to aid in the re-opening of buildings.

## UPDATE: *Activity reporting*

A Flask CLI command has been added to generate and email activity
reports to be used for track and trace of people that are likely to have
been on a site at the same time. Reports contain an ordered list of
sign-in and sign-out events with their locations so that overlap between
people and where they have been can easily be seen. Remember to let your
users know that this data is being recorded and monitored!

The Ansible playbook can be used to create a daily job to generate
reports, or the CLI command can be run from a cron job or manually on
the server as required (note the setup applies to all CLI commands):

```shell script
source /path/to/venv/activate
export FLASK_APP=ooh.http.app
export SITE_CONFIG=/path/to/site_config.py
cd /path/to/data/dir
flask ooh send-report --span 1 abc123x@cam.ac.uk
```

## Installation

The most important steps are:

1. Create a configuration file for your system.
2. Deploy system (probably using Ansible).

### Configuration

- Create `files/site_config.py`
  - Most of the options that can be customised are included in the
    [demo `site_config.py`](https://codeshare.phy.cam.ac.uk/itservices.phy/ooh.phy/flask.ooh.phy/blob/master/deploy/ansible/files/site_config.py)
    which can be copied and customised to your needs.
  - `site_config.py` is a Python file.
  - If any of the options are not clearly explained, please
    [open an issue](https://codeshare.phy.cam.ac.uk/itservices.phy/ooh.phy/flask.ooh.phy/issues)
    to improve the documentation.
  - Any option not set will use the default instead (e.g. holidays).
  - `KIOSKS` is the most important option to enable your system to work.
    Create a dictionary with the IP address of all kiosks as the keys and
    the value is a tuple of the (`building`, `description`, `{options}`)
    where options can be used to set which operation modes it can use.
  - Other options can be used to customise the behaviour.
  - **Set `contact` to either `low` or `off` in the `options` to make the
    terminals operate in a reduced contact mode.**
- [Optional] create `files/logo.png`
  - An image for the top right corner, will be displayed at 64 px height.
- [Optional] create `files/policy.html`
  - A policy document can be displayed after pressing the "Policy" button
    on the interface. Since this is a template document, it is easiest to
    fill in the pre prepared html list in the
    [example `policy.html`](https://codeshare.phy.cam.ac.uk/itservices.phy/ooh.phy/flask.ooh.phy/blob/visitor_sign_in/deploy/ansible/files/policy.html)

### Ansible

Using Ansible to deploy to a server is the easiest option and can be
used to deploy software updates and apply changes to configuration that
are kept separate from the files on the server itself. It has been
tested on Debian but may also work with Ubuntu.

Create an inventory file that contains the variables for your server:

```yaml
all:
  hosts:
    ooh-server.department.cam.ac.uk:
  vars:
    server_admin: it@department.cam.ac.uk
    certbot_registration_email: it--certbot@department.cam.ac.uk
    enable_ssl: certbot
    kiosk_ips: "10.0.2.0/24 131.111.22.11"
```

Important variables:

- `hosts`: Should be the hostname that you will use to access the web
  interface if your server has more than one.
- `server_admin`: Contact information for the webserver (not the interface).
- `enable_ssl`: Generate an SSL certificate for this site. `certbot`
  is recommended. `no` to disable, `selfsigned` is available for testing or
  use `files` to provide your own (`files/fqdn.key` and `files/fqdn.pem`).
- `certbot_registration_email`: Required for `certbot`.
- `kiosk_ips`: Space separated list of IP ranges or addresses that will
  bypass Raven authentication to access the server. Required for kiosks.
- `reporting`: A data structure describing when activity reports should
  be sent, if required. The `address` is required and in the default
  configuration a daily report is sent at 11 pm.
  - `address`: One or more email addresses
  - `span`: Number of days to include in the activity report
  - `hour`: Hour of the day to send the report (or cron expression)
  - `minute`: Minute of the hour to send the report (or cron expression)
  - `weekday`: Only send reports on specific days (cron expression)
- `populate_institution`: If any institutions should have their members
  automatically added to the system, add their INSTID codes here. This can
  also be space separated list of multiple institutions e.g. "PHY IOA". Add
  `--children` to include all child institutions in the import.

Other options (not required):

- `production_data_location`: Points to custom files directory if not in
  `files/` in the current directory.
- `ooh_dir`, `ooh_venv_dir`: Choose a location other than `/var` to install
  on the server.
- `ooh_src`: Request a specific version of the software.
  If not set, then the latest packaged version will be used. Otherwise it
  can point to any `pip`-compatible source, e.g. a git branch of the code:
  `git+https://codeshare.phy.cam.ac.uk/itservices.phy/ooh.phy/flask.ooh.phy@branch_name`
  or an exact version of the software, `flask.ooh==2.3.3`.

Depending on how much customisation has been added, your working directory
will look something like:

```tree
.
├── files/
│   ├── logo.png
│   ├── policy.html
│   └── site_config.py
└── your-server.yml
```

Execute the playbook using your inventory file:

```bash
ansible-playbook /path/to/flask.ooh.phy/deploy/ansible/ooh-server.yml -i your-server.yml
```

The playbook can be re-run any time to change the configuration or update
the code.

### Manual installation

Servers are most easily deployed and maintained using Ansible, however
the code is just a simple Flask application that can be put behind an
Apache webserver running `mod_wsgi` and `mod_ucam_webauth`.

Requirements:

- Python 3.7
- Apache

### Backups

Using the default sqlite configuration, all the data is stored in the
`*.db` files in `/var/www/ooh`, or wherever the application has set to
home. Use of external databased is available but undocumented.

## Basic usage

- Tap the yellow reader with a university card to sign in.
- Tap the purple reader with a university card to sign out.
- If enabled, use the onscreen keyboard to edit details.

## Web usage

- Go to the web address hosting the server to sign in or out remotely.
- Edit details at any time using the Raven login.

### Door control integration

The system could theoretically be integrated with any door control
system that can call webhooks. Register the IP address of your door
controllers or middleware as kiosks and get them to make requests on
swipe actions:
`https://ooh.department.cam.ac.uk/action/?reader=<yellow|purple>&mifare=<mifare>&crsid=<crsid>`,
where `yellow` should be used for entry actions (e.g. outer doors) and
`purple` for exit actions. `mifare` value is required and `crsid` is
useful if available.

## Visitor Sign-in Usage

- Use an administrator card to register visitors or other administrators.
- Use the yellow and purple card readers to sign visitors in and out.
- Use the administrator interface to manage the visitors in the system remotely.
