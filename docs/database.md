# Database storage

The default deployment option is to store all data in local SQLite
database files. In Physics this setup has scaled to work with over 100
users constantly signed-in and nearly 20 active terminals maintaining
records for a month or more. Resilience in this configuration can be
provided by considering the application as a whole including the
database files.

Some installations will have different requirements and would benefit
from using a more client-server type database system. High availability,
data integrity and horizontal scaling would be easier to deploy by
making use of the features of database systems. In theory, the code is
database agnostic to the extent that the ORM used,
[SQLAlchemy](https://www.sqlalchemy.org/), has support for a number of
different database connectors, and no database-specific extensions are
being used. In practice, SQLite is used heavily in production and basic
functions have been tested with PostgreSQL. Any problems using other
configurations should be reported as bugs to help support those systems
better.

## Configuring databases

There are four independent databases that can be configured separately
(for example to isolate sensitive visitor information from other users):

- `ENTRIES_DB`: Records of all sign-in/out activity
- `LOOKUP_DB`: User profiles
- `TOKEN_DB`: Used for deferred actions and other tokens
- `VISITOR_DB`: All information relating to the visitor sign-in extension

The databases are configured either in the `site_config.py` or can be
set as environment variables for the process. Each should be a
SQLAlchemy
[database URL](https://docs.sqlalchemy.org/en/13/core/engines.html#database-urls).
For example,

- `"ENTRIES_DB": "sqlite:///entries.db"`: SQLite database in the working directory;
- `"LOOKUP_DB": "postgresql://ooh_user:xxpasswordxx@database-hosting.dept.cam.ac.uk:5432/ooh_lookup"`:
  PostgreSQL database with default (psycopg2) connector.

## Starting from scratch

The easiest option is to start with the required database from the
initial setup. By setting the correct database URLs and ensuring any
extra packages for the database are installed in the virtual environment
for the application, the `flask database migrate` will initialise the
databases as required.

## Migrating data

The recommended path for migrating data is to use
`flask database migrate` to initialise empty target databases with the
correct schemas and import only the data from the existing database.
Some of the data will need to be modified to allow it to be imported
properly.

### SQLite -> PostgreSQL

- Use `sqlite3 lookup.db .dump > lookup.sql` to generate dumps of the
  data.
- Remove the `CREATE TABLE` directives since you've already created
  them.
- Remove `INSERT` of alembic keys (both databases must be fully migrated
  first!).
- Ensure that `building_occupancy` inserts are after the `people`
  inserts as columns might not respect `DEFERRED` foreign key checks.
- Convert integers for the `invisible` field in `people` to boolean
  compatible values, (e.g. `:%s/,0);/,0::boolean);/`)
- Import with psql, e.g.: `psql -d ooh_lookup < lookup.sql
- Reset all the `SEQUENCE` columns in each database:
  - `SELECT setval(pg_get_serial_sequence('entry', 'id'), coalesce(max(id),0) + 1, false) FROM entry;`
  - `SELECT setval(pg_get_serial_sequence('usage', 'id'), coalesce(max(id),0) + 1, false) FROM usage;`
  - `SELECT setval(pg_get_serial_sequence('people', 'id'), coalesce(max(id),0) + 1, false) FROM people;`
  - `SELECT setval(pg_get_serial_sequence('cards', 'id'), coalesce(max(id),0) + 1, false) FROM cards;`
  - `SELECT setval(pg_get_serial_sequence('admins', 'id'), coalesce(max(id),0) + 1, false) FROM admins;`
  - `SELECT setval(pg_get_serial_sequence('visitors', 'id'), coalesce(max(id),0) + 1, false) FROM visitors;`
  - `SELECT setval(pg_get_serial_sequence('visitor_scans', 'id'), coalesce(max(id),0) + 1, false) FROM visitor_scans;`
