"""
Lookup application wsgi script.

For plugging in to mod_wsgi with apache. A simple apache conf would
look like:

<VirtualHost *>
    ServerName server.name.com

    SetEnv SITE_CONFIG "/path/to/site_config.py"

    WSGIDaemonProcess lookup user=www group=www threads=8 python-home=/path/to/virtualenv
    WSGIScriptAlias / /path/to/ooh.wsgi

    <Directory /path/to/ooh/source/>
        WSGIProcessGroup ooh
        WSGIApplicationGroup %{GLOBAL}
        Require all granted
    </Directory>
</VirtualHost>
"""

import os


# set up mod_wsgi to use a virtual environment
# and install the app in there so you can just
# import it here
def application(request_environ, start_response):
    # Enable setting the SITE_CONFIG through Apache SetEnv.
    if 'SITE_CONFIG' in request_environ:
        os.environ['SITE_CONFIG'] = request_environ['SITE_CONFIG']
    # Must be imported after the environment setting
    # otherwise it doesn't see it
    from ooh.http.app import create_app
    return create_app()(request_environ, start_response)

# vim: ft=python
