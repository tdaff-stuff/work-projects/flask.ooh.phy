#!/bin/bash
cd "$(dirname "${0}")"
set -ex
virtualenv ooh-env -p "$(which python3)"
set +x
source ooh-env/bin/activate
set -x
pip install --upgrade pip
pip install -e .
