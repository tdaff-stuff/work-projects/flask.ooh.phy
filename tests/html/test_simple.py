"""Test pages can be served by flask."""

import pytest
from flask import url_for

# All tests use flask app fixture, defined in conftest.py


def test_up(client):
    """Test that flask app is able to run."""
    test_url = url_for("view.index")
    res = client.get(test_url)
    assert res.status_code == 200


def test_print(client):
    """Test that flask app is able to run."""
    test_url = url_for("view.printout")
    res = client.get(test_url)
    assert res.status_code == 200


def test_404_error(client):
    """Non existing URL."""
    test_url = "nothing_here"

    res = client.get(test_url)
    assert res.status_code == 404
