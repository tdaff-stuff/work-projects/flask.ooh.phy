"""Test that someone can sign in, update details and then out."""

from flask import url_for


# All tests use flask app fixture, defined in conftest.py


def test_in_out(client, clean_db, force_ooh):
    """Test that flask app is able to run."""
    # needs to start from an empty database

    test_crsid = "test001"
    test_mifare = 123456
    test_name = "Test Person"
    test_telephone = "x3728"
    test_telephone_hidden = "07854-979532"

    index_url = url_for("view.index")
    sign_in_url = url_for("view.scan")
    update_url = url_for("view.update")
    print_url = url_for("view.printout")

    # index loads
    index = client.get(index_url)
    assert index.status_code == 200

    # make a scan
    scan = {"crsid": test_crsid, "mifare": test_mifare, "reader": "yellow"}
    sign_in = client.post(sign_in_url, data=scan)
    sign_in_page = sign_in.get_data(as_text=True)
    assert "Signed in as" in sign_in_page

    # check index
    index = client.get(index_url)
    assert test_crsid in index.get_data(as_text=True)

    # update details
    details = {
        "crsid": test_crsid,
        "mifare": test_mifare,
        "name": test_name,
        "telephone": test_telephone,
        "emergency_telephone": test_telephone_hidden,
    }
    update = client.post(update_url, data=details)

    # check index again
    index = client.get(index_url)
    index_page = index.get_data(as_text=True)

    # visible details
    assert test_name in index_page
    assert test_telephone in index_page
    # invisible details
    assert test_telephone_hidden not in index_page
    assert test_crsid not in index_page

    # check printout
    printout = client.get(print_url)
    printout_page = printout.get_data(as_text=True)

    # visible details
    assert test_name in printout_page
    assert test_telephone in printout_page
    assert test_telephone_hidden in printout_page
    assert test_crsid in printout_page

    # scan out
    scan = {"crsid": test_crsid, "mifare": test_mifare, "reader": "purple"}
    sign_in = client.post(sign_in_url, data=scan)
    sign_in_page = sign_in.get_data(as_text=True)
    assert "Signed out" in sign_in_page

    # check index again
    index = client.get(index_url)
    index_page = index.get_data(as_text=True)

    # not signed in
    assert test_name not in index_page
    assert test_crsid not in index_page
