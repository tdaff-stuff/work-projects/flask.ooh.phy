"""
Custom test configuration setup for pytest.

Defines:

- An ``app`` for use with flask tests, used via the ``client`` fixture.
- Option to force a clean database for the test using ``clean_db``.
- Option to force out-of-hours mode using ``force_ooh``.

"""

import pytest

from ooh.http.app import create_app
from ooh.interface.database import OOHDatabase
from ooh.interface.lookup import PeopleDatabase
from ooh.interface.tokens import TokenDatabase
from ooh.interface.visitors import VisitorDatabase


@pytest.fixture
def app():
    """
    Fixture required by pytest-flask that provides
    the main app interface to any classes that require it.
    """

    flask_app = create_app()
    flask_app.debug = True

    return flask_app


@pytest.fixture(autouse=True)
def shared_db(tmp_path_factory, monkeypatch):
    """
    Set database paths to be in a shared location for tests where
    having a clean database is not required. Just uses the shared
    temporary directory.
    """

    basetemp = tmp_path_factory.getbasetemp()
    monkeypatch.setattr(
        "ooh.interface.database.ENTRIES_DB",
        "sqlite:///{}".format(basetemp / "entries.db"),
    )
    monkeypatch.setattr(
        "ooh.interface.lookup.LOOKUP_DB", "sqlite:///{}".format(basetemp / "lookup.db")
    )
    monkeypatch.setattr(
        "ooh.interface.tokens.TOKEN_DB", "sqlite:///{}".format(basetemp / "token.db")
    )
    monkeypatch.setattr(
        "ooh.interface.visitors.VISITOR_DB",
        "sqlite:///{}".format(basetemp / "visitor.db"),
    )

    # Create the empty databases
    for interface in [OOHDatabase, PeopleDatabase, TokenDatabase, VisitorDatabase]:
        database = interface()
        database.base.metadata.create_all(database.engine)


@pytest.fixture
def clean_db(tmp_path, monkeypatch):
    """
    Set the default database paths to be somewhere in the temporary
    directory for the current test. This will have the have the effect
    of creating a clean database for the test.

    Since creating a database is relatively expensive, this can be used
    to override the default only when required.
    """

    monkeypatch.setattr(
        "ooh.interface.database.ENTRIES_DB",
        "sqlite:///{}".format(tmp_path / "entries.db"),
    )
    monkeypatch.setattr(
        "ooh.interface.lookup.LOOKUP_DB", "sqlite:///{}".format(tmp_path / "lookup.db")
    )
    monkeypatch.setattr(
        "ooh.interface.tokens.TOKEN_DB", "sqlite:///{}".format(tmp_path / "token.db")
    )
    monkeypatch.setattr(
        "ooh.interface.visitors.VISITOR_DB",
        "sqlite:///{}".format(tmp_path / "visitor.db"),
    )

    # Create the empty databases
    for interface in [OOHDatabase, PeopleDatabase, TokenDatabase, VisitorDatabase]:
        database = interface()
        database.base.metadata.create_all(database.engine)


@pytest.fixture
def force_ooh(monkeypatch) -> None:
    """
    Bypass time check and make views act as if they are
    outside working hours.
    """
    monkeypatch.setattr("ooh.http.views.working_hours", lambda: False)
