"""
Server CLI

Run commands to manage the software instance, particularly
database creation and migrations.

Also implements some administration commands that would be run
e.g. during initialisation, on demand or periodic cron jobs.

Requires the environment to be set up correctly so that it can
pull in any extra configuration information.

FLASK_APP=ooh.http.app
SITE_CONFIG=/path/to/site_config.py
"""
import logging
import smtplib
from csv import DictWriter
from datetime import datetime, timedelta, timezone
from email.headerregistry import Address
from email.message import EmailMessage
from io import StringIO
from typing import Dict, Union, Type, List

import click
from alembic import command, migration
from alembic.config import Config
from flask import render_template
from flask.cli import AppGroup

from ooh.data.configuration import KIOSKS, BUILDINGS
from ooh.data.configuration import CONTACT_EMAIL, CONTACT_NAME, SITE_TITLE
from ooh.data.configuration import RETENTION_PROFILES
from ooh.interface.database import OOHDatabase
from ooh.interface.lookup import PeopleDatabase
from ooh.interface.tokens import TokenDatabase
from ooh.interface.visitors import VisitorDatabase
from ooh.util.cam import child_insts_recursive

Database = Union[OOHDatabase, PeopleDatabase, TokenDatabase, VisitorDatabase]

TARGETS = {
    "database": OOHDatabase,
    "lookup": PeopleDatabase,
    "tokens": TokenDatabase,
    "visitors": VisitorDatabase,
}  # type: Dict[str, Type[Database]]

database_choice = click.Choice(TARGETS.keys(), case_sensitive=False)

FILE_TEMPLATE = "%%(year)d-%%(month)02d-%%(day)02d_%%(rev)s-%%(slug)s"

db_cli = AppGroup("database", help="Commands to manage databases")
ooh_cli = AppGroup("ooh", help="Commands to manage the Sign-In system")


@db_cli.command("migrate")
@click.option("--verbose", "-v", count=True)
def migrate(verbose: int = 0) -> None:
    """
    Carry out any migrations, including initialisation. Applies
    to all databases.

    \b
    Parameters
    ----------
    verbose
        Turn on all logging levels.
    """
    # Output is minimal without logging
    if verbose > 0:
        logging.basicConfig(level=logging.DEBUG)

    # iterate over all databases
    for target, interface in TARGETS.items():
        database = interface()  # type: Database
        # Alembic setup using current configuration
        config = Config()
        config.set_main_option("target", target)
        config.set_main_option("script_location", "ooh:migrations")
        config.set_main_option("sqlalchemy.url", database.url)

        # Check whether the database is initialised by alembic
        context = migration.MigrationContext.configure(database.engine.connect())
        current_rev = context.get_current_revision()

        # Database does not exist, or not yet migrated
        if current_rev is None:
            # Will create a database or any missing tables
            database.base.metadata.create_all(database.engine)
            # Stamp the current alembic revision
            command.stamp(config, "head")

        command.upgrade(config, "head")
        # Show the final rev if it's changed
        final_rev = context.get_current_revision()
        if verbose > 0 or final_rev != current_rev:
            print("{}: {} -> {}".format(target, current_rev, final_rev))


@db_cli.command("makemigrations")
@click.argument("target", type=database_choice)
@click.argument("message")
def revision(target: str, message: str) -> None:
    """
    Use autogenerate to create migrations for the selected database.

    \b
    Parameters
    ----------
    target
        Name of the database to inspect, choice of "database", "lookup",
        "tokens" or "visitors".
    message
        Description of changes, also appears as a slug in the filename.
    """
    # Work with the desired database
    database = TARGETS[target]()

    # Alembic setup using current configuration
    config = Config()
    config.set_main_option("target", target)
    config.set_main_option("script_location", "ooh:migrations")
    config.set_main_option("sqlalchemy.url", database.url)

    # Mostly sequential migration files
    config.set_main_option("file_template", FILE_TEMPLATE)

    # Run the command
    command.revision(config, message=message, autogenerate=True)


@db_cli.command("downgrade", context_settings={"ignore_unknown_options": True})
@click.argument("target", type=database_choice)
@click.argument("rev")
@click.option("--verbose", "-v", count=True)
def downgrade(rev: str, target: str, verbose: int = 0) -> None:
    """
    Downgrade a database to an earlier revision. Most common
    is probably `-1`, but an exact revision can also be specified.

    The database to downgrade must be given on the commandline as
    not all databases get upgraded concurrently.

    \b
    Parameters
    ----------
    target
        Name of the database to downgrade, choice of "database", "lookup",
        "tokens" or "visitors".
    rev
        Revision slug or relative change, e.g. -1.
    verbose
        Turn on all logging levels.
    """
    # `ignore_unknown_options` required to stop negative numbers getting
    # eaten by the parser

    # Output is minimal without logging
    if verbose > 0:
        logging.basicConfig(level=logging.DEBUG)

    # Work with the desired database
    database = TARGETS[target]()

    # Alembic setup using current configuration
    config = Config()
    config.set_main_option("target", target)
    config.set_main_option("script_location", "ooh:migrations")
    config.set_main_option("sqlalchemy.url", database.url)

    # Run the command
    command.downgrade(config, rev)


@ooh_cli.command("clear-inactive")
@click.option("--verbose/--quiet", "-v/-q", default=False)
def clear_inactive(verbose: bool = False) -> None:
    """
    Find inactive user profiles in the system and remove them.
    The duration of RETENTION_PROFILES must be configured
    in the settings to align with the desired site policy.

    Resolution of activity is around a month, so the command
    only needs to be run more frequently than that (e.g. cron).

    \b
    Parameters
    ----------
    verbose
        Whether to print output when running the import.

    """

    # Ensure nobody with active records is removed
    scan_db = OOHDatabase()
    active_user_crsids = scan_db.active_crsids()
    active_user_mifare = scan_db.active_mifare()

    # Remove people according to the policy. Defaults are all
    # to remove nobody in the case of mis-configuration.
    people_db = PeopleDatabase()
    removed = people_db.delete_inactive_profiles(
        retention=RETENTION_PROFILES,
        exclude_crsid=active_user_crsids,
        exclude_mifare=active_user_mifare,
    )

    if verbose:
        click.echo("Deleted {} profiles.".format(len(removed)))


@ooh_cli.command("populate-inst")
@click.argument("institutions", nargs=-1, required=True)
@click.option("--children/--no-children", default=False)
@click.option("--verbose/--quiet", "-v/-q", default=False)
def populate_inst(
    institutions: List[str], children: bool = False, verbose: bool = False
) -> None:
    """
    Add all members of the given institutions to the database and populate
    with their names from lookup.cam. Existing entries with no names
    will also be updated with their visible names from lookup.

    \b
    Parameters
    ----------
    institutions
        A list of INSTID of the institutions to import e.g. PHY for physics.
    children
        Whether to recursively include all child institutions of those
        requested.
    verbose
        Whether to print output when running the import.

    """

    from ibisclient.methods import InstitutionMethods
    from ibisclient.connection import createConnection, IbisException

    # lookup.cam api working with members of institutions
    api = InstitutionMethods(createConnection())

    database = PeopleDatabase()

    # Exception being caught is when the API fails to connect, usually
    # when
    try:
        if children:
            # Include parent institutions
            all_institutions = list(institutions)
            for instid in institutions:
                all_institutions.extend(child_insts_recursive(instid, api=api))
            # Deduplicate the list
            institutions = tuple(set(all_institutions))

        for instid in institutions:
            count_created = 0
            count_modified = 0

            institution = api.getInst(instid=instid, fetch="all_members")

            if institution is None:
                if verbose:
                    print("Unknown institution {}".format(instid))
                continue

            for person in institution.members:
                # CRSid needs to be extracted from the attribute
                crsid = person.identifier.value

                # Check if the person already exists in the database
                existing = database.get_person(crsid=crsid)

                if existing is None:
                    database.create_person(crsid=crsid, name=person.visibleName)
                    count_created += 1
                elif not existing.name:
                    database.update_person(crsid=crsid, name=person.visibleName)
                    count_modified += 1

            if verbose:
                print("Institution: ", institution.name)
                print("Created: ", count_created)
                print("Modified: ", count_modified)

    except IbisException:
        raise SystemExit("Error connecting to lookup! Are you on the CUDN?")


@ooh_cli.command("send-report")
@click.argument("address", nargs=-1, required=True)
@click.option("-s", "--span", type=float, default=1, show_default=True)
def send_report(address: List[str], span: float = 1) -> None:
    """
    Generate a usage report as a list of sign in and sign out actions
    and send it by email to each of the ADDRESS recipients.

    Report lists each action and the person's details and is generated
    as an HTML table as the message body and a CSV file.

    \b
    Parameters
    ----------
    address
        A list of email addresses to which the report will be sent.
    span
        Timespan in days to generate the usage report for.
    """

    # Requested time span - work in local times
    now = datetime.now()
    since = now - timedelta(days=span)

    # Data requirements are different enough from other functions
    # of the code that we will do the processing here.
    # Work mostly with db objects (e.g. the admin report works
    # with dictionaries) and build custom data structures.
    scan_db = OOHDatabase()
    people_db = PeopleDatabase()

    # Obtain all the relevant data for processing.
    scan_events = scan_db.scans(since=since.timestamp())
    people = people_db.get_people(
        crsid=[scan_event.crsid for scan_event in scan_events],
        mifare=[scan_event.mifare for scan_event in scan_events],
    )

    # Index by both identifiers, as either may be missing.
    people_by_crsid = {person.crsid: person for person in people if person.crsid}
    people_by_mifare = {person.mifare: person for person in people if person.mifare}

    # Data as a list of dictionaries that can be given to the CSV reader
    # or used directly in the HTML report.
    rows = []

    # Build the list of dictionaries with all the relevant fields
    for scan_event in scan_events:
        # Need to have a person associated with the record
        if scan_event.crsid in people_by_crsid:
            scan_person = people_by_crsid[scan_event.crsid]
        elif scan_event.mifare in people_by_mifare:
            scan_person = people_by_mifare[scan_event.mifare]
        else:
            continue

        # Use proper datetime object.
        # timezone can be omitted for Python >= 3.6
        timestamp = datetime.fromtimestamp(scan_event.timestamp, timezone.utc)

        # Simple fields
        report_row = {
            # Localtime in a format that is parsed as a date in Excel
            "timestamp": timestamp.astimezone().strftime("%Y-%m-%d %H:%M:%S"),
            "name": scan_person.name,
            "crsid": scan_person.crsid,
            # trim the long mifare numbers as they mess up table formatting
            "mifare": (scan_person.mifare or "")[:12],
            "telephone": scan_person.telephone,
            "emergency_telephone": scan_person.emergency_telephone,
            "rooms": scan_person.rooms,
        }

        # Other fields need a bit of processing
        if scan_event.reader == "yellow":
            report_row["action"] = "In"
        else:
            report_row["action"] = "Out"

        # Reporting locations and buildings depends on whether the
        # site is multi building. Always report that the person was
        # in a building that they scanned into in addition to any
        # reported buildings.
        scan_location = KIOSKS.get(scan_event.location)
        if scan_location is None:
            report_row["location"] = "Remote"
        elif len(BUILDINGS) > 1:
            report_row["location"] = "{0.building} - {0.location}".format(scan_location)
            report_row[scan_location.building] = "X"
        else:
            report_row["location"] = scan_location.location

        # Note all buildings that the user works in.
        if len(BUILDINGS) > 1:
            for building in scan_person.buildings:
                report_row[building.name] = "X"

        # Done!
        rows.append(report_row)

    # Build a CSV file from the rows using the standard library.
    # Skip building reporting if there's only one building.
    if len(BUILDINGS) > 1:
        building_fields = BUILDINGS
    else:
        building_fields = []

    # Fields are ordered to put most important information first about
    # who is where. Contact details just stuck at the end, and get mangled
    # by Excel anyway.
    fieldnames = (
        ["timestamp", "action", "name", "crsid", "location", "rooms"]
        + building_fields
        + ["mifare", "telephone", "emergency_telephone"]
    )

    # CSV module requires something to operate on, can't just get text
    with StringIO() as dummy_file:
        dr = DictWriter(dummy_file, fieldnames=fieldnames, extrasaction="ignore")
        dr.writeheader()
        dr.writerows(rows)
        csv_report = dummy_file.getvalue()

    # HTML report is just a Jinja2 template document
    html_report = render_template(
        "report.html",
        rows=rows,
        buildings=BUILDINGS,
        since=since,
        now=now,
        title=SITE_TITLE,
    )

    # Build and send message, no bounce check as failed delivery reports
    # will be more useful and leak less data
    message = EmailMessage()

    message["From"] = Address(CONTACT_NAME, addr_spec=CONTACT_EMAIL)
    message["To"] = ", ".join(address)
    message["Subject"] = "{} Usage Report - {}".format(SITE_TITLE, now.date())
    message.set_content(html_report, subtype="html")
    message.add_attachment(
        csv_report, subtype="csv", filename="report-{}.csv".format(now.date())
    )

    # Just send with ppsw as it's easy
    with smtplib.SMTP("ppsw.cam.ac.uk") as s:
        s.send_message(message)
