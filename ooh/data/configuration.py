"""
Configuration information for the complete application.

"""

import os
from importlib import util as import_util
from importlib.machinery import SourceFileLoader
from typing import Optional, Sequence, List
from uuid import uuid4

from collections import namedtuple
from datetime import datetime, time, timedelta
from ipaddress import ip_address, ip_network
from time import mktime

from ooh.data.defaults import CONFIG, EMAIL_TEMPLATES

# Custom data type for kiosk information
Kiosk = namedtuple("Kiosk", ["ip", "building", "location", "options"])

# Is there a user specified configuration?
# If so, update existing values with the contents of user config
SITE_CONFIG = os.getenv("SITE_CONFIG")

if SITE_CONFIG is not None:
    # import user configuration directly from the file and
    # merge it with defaults, overwriting any that are set.
    spec = import_util.spec_from_file_location("site_config", SITE_CONFIG)
    assert isinstance(spec.loader, SourceFileLoader)
    site_config = import_util.module_from_spec(spec)
    spec.loader.exec_module(site_config)
    # Merge the CONFIG dict
    CONFIG.update(getattr(site_config, "CONFIG", {}))
    # Merge any email templates that are set as a whole
    # take whole item in case only plain text is defined without html
    for template_name, text in getattr(site_config, "EMAIL_TEMPLATES", {}).items():
        EMAIL_TEMPLATES[template_name] = text

# Is general building sign in turned on?
ENABLE_SIGN_IN = CONFIG["ENABLE_SIGN_IN"]  # type: bool
# Is visitor sign-in turned on?
ENABLE_VISITOR_SIGN_IN = CONFIG["ENABLE_VISITOR_SIGN_IN"]  # type: bool

# Databases can be specified in environment variables
# (helpful for testing)
ENTRIES_DB = os.getenv("ENTRIES_DB", CONFIG["ENTRIES_DB"])  # type: str
LOOKUP_DB = os.getenv("LOOKUP_DB", CONFIG["LOOKUP_DB"])  # type: str
TOKEN_DB = os.getenv("TOKEN_DB", CONFIG["TOKEN_DB"])  # type: str
VISITOR_DB = os.getenv("VISITOR_DB", CONFIG["VISITOR_DB"])  # type: str

# Flask session key for session cookies, default
# to random (i.e. resets all sessions on server restart)
SECRET_KEY = CONFIG["SECRET_KEY"] or uuid4().hex

# Create the Kiosk list from the data. The IP is the identifying
# feature of the Kiosk.
KIOSKS = {
    ip: Kiosk(ip, building, location, options)
    for ip, (building, location, options) in CONFIG["KIOSKS"].items()
}

# Site identification and branding
SITE_NAME = CONFIG["SITE_NAME"]
SITE_NAME_SUFFIX = CONFIG["SITE_NAME_SUFFIX"]
SITE_LOGO = CONFIG["SITE_LOGO"]
SITE_POLICY = CONFIG["SITE_POLICY"]
SITE_TITLE = CONFIG["SITE_TITLE"]

# Only used in emergencies, red banner below the nav bar.
EMERGENCY_MESSAGE = CONFIG["EMERGENCY_MESSAGE"]

# List of on-site locations
BUILDINGS = CONFIG["BUILDINGS"]  # type: List[str]

# Sending emails, just print in DEBUG mode
DEBUG_MAIL = CONFIG["DEBUG_MAIL"]
# Email sender (and contact on page)
CONTACT_EMAIL = CONFIG["CONTACT_EMAIL"]
CONTACT_NAME = CONFIG["CONTACT_NAME"]
# Email sender and recipient for undeliverable addresses
BOUNCE_EMAIL = CONFIG["BOUNCE_EMAIL"]
# Whether to enable nagging for missing emergency phone numbers
NAG_EMERGENCY_PHONE = CONFIG["NAG_EMERGENCY_PHONE"]
# Which fields to highlight
HIGHLIGHT_FIELDS = CONFIG["HIGHLIGHT_FIELDS"]
# Which fields to nag if empty, empty list means none
NAG_MISSING_FIELDS = CONFIG["NAG_MISSING_FIELDS"]

# Times to use for sign out of all users
START_OF_DAY = [
    datetime.strptime(time_string, "%H:%M").time()
    for time_string in CONFIG["START_OF_DAY"]
]

# Times to use for sign out of all users
END_OF_DAY = [
    datetime.strptime(time_string, "%H:%M").time()
    for time_string in CONFIG["END_OF_DAY"]
]

# Calendar of days when the department is CLOSED and no reset of the
# occupancy is required (i.e. out of hours).
# Parse from yyyy-mm-dd format
NON_WORKDAYS = [
    datetime.strptime(date_string, "%Y-%m-%d").date()
    for date_string in CONFIG["NON_WORKDAYS"]
]

WEEKEND = CONFIG["WEEKEND"]  # datetime.weekday is 0-based from Monday

# Cavendish on-site ip-addresses
# Parse from any of the formats accepted by ip_network
NETWORKS = [ip_network(network) for network in CONFIG["NETWORKS"]]

# Whether to ever show list of signed in users during normal working
# and outside of working hours.
SHOW_LIST_WORKDAY = CONFIG["SHOW_LIST_WORKDAY"]
SHOW_LIST_OOH = CONFIG["SHOW_LIST_OOH"]

# Whether users can hide their presence in the building
ENABLE_INVISIBILITY = CONFIG["ENABLE_INVISIBILITY"]

# Show individual usage to users and admins
ENABLE_TRACKING = CONFIG["ENABLE_TRACKING"]

# Days to keep records after a new day starts
RETENTION = CONFIG["RETENTION"]

# Months to keep profiles after their last activity
RETENTION_PROFILES = CONFIG["RETENTION_PROFILES"]

# CRSids of admin users
ADMINS = CONFIG["ADMINS"]

# Bus routes, if configured
BUS_ROUTES = CONFIG["BUS_ROUTES"]

# Different site name and title when in visitor mode
VISITOR_SITE_NAME = CONFIG["VISITOR_SITE_NAME"] or SITE_NAME
VISITOR_SITE_NAME_SUFFIX = CONFIG["VISITOR_SITE_NAME_SUFFIX"] or SITE_NAME_SUFFIX
VISITOR_SITE_TITLE = CONFIG["VISITOR_SITE_TITLE"]

# Super-admins should be pre-seeded so they can create new
# admins in the interface. Always treat them as strings, even
# if they are actually integers.
VISITOR_SUPER_ADMIN_MIFARE = [
    str(mifare) for mifare in CONFIG["VISITOR_SUPER_ADMIN_MIFARE"]
]

SESSION_IDLE_TIMEOUT = CONFIG["SESSION_IDLE_TIMEOUT"]


#
# Functions that provide more useful interfaces to configuration values
#
def on_site_ip(ip_addr):
    """
    Determine if an IP address is considered on-site.

    Parameters
    ==========
    ip_addr: str
        The IP address to be analysed.

    Returns
    =======
    on_site: bool
        True if the ip address is considered on-site, False if not.
    """

    # Python3 IP address convenience library
    this_ip = ip_address(ip_addr)

    for network in NETWORKS:
        if this_ip in network:
            return True

    return False


def start_of_day(hour: int = 7, minute: int = 30) -> float:
    """
    Return the UNIX timestamp of the beginning of the last working
    day beginning at the specified time. Uses the configured WEEKEND
    and NON_WORKDAYS to determine if a day is counted as a working
    day. If every day is a weekend, the timestamp will be zero.

    Parameters
    ==========
    hour
        Hour to begin the working day
    minute
        Minute to begin the working day

    Returns
    =======
    timestamp
        UNIX timestamp of the beginning of the last working day
    """
    now = datetime.now()
    # datetime is immutable so generate a copy at a workday start
    workday_start = now.replace(hour=hour, minute=minute, second=0, microsecond=0)
    # If we are before work starts today, go to yesterday
    if now < workday_start:
        workday_start -= timedelta(days=1)

    # If every day is a weekend, don't scan all time
    if set(WEEKEND).issuperset({0, 1, 2, 3, 4, 5, 6}):
        return 0

    # Go backwards to previous workday
    while workday_start.weekday() in WEEKEND or workday_start.date() in NON_WORKDAYS:
        workday_start -= timedelta(days=1)

    return mktime(workday_start.timetuple())


def last_start_of_day(start_times: Optional[Sequence[time]] = None) -> float:
    """
    Return the UNIX timestamp of the beginning of the last working
    day considering all the given times. Defaults to the series of
    day starts set in the configuration.

    Parameters
    ==========
    start_times
        Series of times that will be considered for sign out

    Returns
    =======
    timestamp
        UNIX timestamp of the beginning of the last working day
    """
    if start_times is None:
        start_times = START_OF_DAY

    # No start of day is given, so go back to the beginning of time
    if not start_times:
        return 0

    return max(start_of_day(st.hour, st.minute) for st in start_times)


def working_hours(timestamp: Optional[float] = None) -> bool:
    """
    Determine if it is currently "working hours" either
    for the given timestamp, if given, otherwise calculate
    for the current time. Working hours are determined from
    configuration of `START_OF_DAY` and `END_OF_DAY`

    Parameters
    ==========
    timestamp
        Timestamp to check, defaults to `now`.

    Returns
    =======
    working_hours
        True if during the workday, otherwise False

    """

    if timestamp is None:
        now = datetime.now()
    else:
        now = datetime.fromtimestamp(timestamp)

    # Not a working day at all
    if now.weekday() in WEEKEND or now.date() in NON_WORKDAYS:
        return False

    # It is a working day!
    # Are we after all start of day, and before all end of day
    if any([now.time() < start for start in START_OF_DAY]):
        return False
    if any([now.time() > end for end in END_OF_DAY]):
        return False

    # Must be during work hours
    return True
