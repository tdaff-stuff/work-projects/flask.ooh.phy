# Application configuration defaults
# All of these can be overridden in production these examples are
# used for the Cavendish deployment
from typing import Any, Dict

CONFIG = {
    # Whether to enable building sign in
    "ENABLE_SIGN_IN": True,
    # Whether to enable visitor sign in mode
    "ENABLE_VISITOR_SIGN_IN": False,
    # SQLAlchemy URLs to the databases
    # Can also be overridden in environment variables
    "ENTRIES_DB": "sqlite:///entries.db",
    "LOOKUP_DB": "sqlite:///lookup.db",
    "TOKEN_DB": "sqlite:///tokens.db",
    "VISITOR_DB": "sqlite:///visitors.db",
    # Session key is required for tracking data between requests.
    # If this is blank, a random value will be generated on each
    # restart, sessions are not really used for building occupancy,
    # but this value must be set for visitor sign in to work.
    "SECRET_KEY": None,
    # Components of the name, generally NAME will be emphasised more than
    # the _SUFFIX. Include a space if they need to be separated.
    "SITE_NAME": "register",
    "SITE_NAME_SUFFIX": ".phy",
    # What's shown at the top of the page to identify the mode
    # of operation.
    "SITE_TITLE": "Buildings Occupancy Register",
    # In case of an emergency message, this message will be displayed
    # as a red banner below the title
    "EMERGENCY_MESSAGE": None,
    # Full path to an image that replaces the IT Services logo,
    # leave as None to use the default
    "SITE_LOGO": None,
    # Full path to a policy document as a html template, leave as
    # None for the default Cavendish policy
    "SITE_POLICY": None,
    # Each Kiosk is identified by its IP address and has a
    # ("Building", "Location", {options}) tuple as the data.
    # Valid options in the options dict are:
    #   - "default":   Mode to use when none is selected by the user,
    #                  if unset, will switch during the workday
    #   - "exclusive": Only allow a single mode of operation.
    #   - "contact": Can be set to "low" to reduce the prompts for
    #                interaction or "off" to disable most interactions
    #                on the interface.
    # Valid modes are: "sign-in", "visitor-sign-in", "print".
    "KIOSKS": {
        "172.24.4.150": (
            "Bragg",
            "Near Common Room",
            {"exclusive": "sign-in", "contact": "low"},
        ),
        "172.24.4.152": (
            "Bragg",
            "Pippard foyer",
            {"exclusive": "sign-in", "contact": "low"},
        ),
        "172.24.4.165": (
            "Bragg",
            "Teaching labs",
            {"exclusive": "sign-in", "contact": "low"},
        ),
        "172.24.4.154": (
            "Bragg",
            "NP office suite",
            {"exclusive": "sign-in", "contact": "low"},
        ),
        "172.24.4.200": (
            "Bragg",
            "Reception",
            {"exclusive": "sign-in", "contact": "low"},
        ),
        "172.24.4.173": (
            "Bragg",
            "Reception Printer",
            {"exclusive": "sign-in", "contact": "low"},
        ),
        "172.24.4.155": (
            "Mott",
            "Ground floor",
            {"exclusive": "sign-in", "contact": "low"},
        ),
        "172.24.4.198": (
            "Mott",
            "Ground floor",
            {"exclusive": "sign-in", "contact": "low"},
        ),
        "172.24.4.156": (
            "Rutherford",
            "Ground floor",
            {"exclusive": "sign-in", "contact": "low"},
        ),
        "172.24.4.197": (
            "Rutherford",
            "West entrance",
            {"exclusive": "sign-in", "contact": "low"},
        ),
        "172.24.4.158": (
            "Maxwell",
            "Level 0 east door",
            {"exclusive": "sign-in", "contact": "low"},
        ),
        "172.24.4.164": (
            "Maxwell",
            "Reception",
            {"exclusive": "sign-in", "contact": "low"},
        ),
        "172.24.4.195": (
            "Maxwell",
            "Reception Printer",
            {"exclusive": "sign-in", "contact": "low"},
        ),
        "172.24.4.161": (
            "PoM",
            "Ground floor",
            {"exclusive": "sign-in", "contact": "low"},
        ),
        "172.24.4.162": (
            "Kapitza",
            "Ground floor",
            {"exclusive": "sign-in", "contact": "low"},
        ),
        "172.24.4.153": (
            "Battcock",
            "Entrance",
            {"exclusive": "sign-in", "contact": "low"},
        ),
        "172.24.4.160": (
            "MRC",
            "University entrance",
            {"exclusive": "sign-in", "contact": "low"},
        ),
        "172.24.4.166": ("Bragg", "ITS Testing Pi", {"exclusive": "sign-in"}),
        "172.24.4.167": ("Bragg", "ITS Testing Pi", {"exclusive": "sign-in"}),
        "172.24.4.168": ("Bragg", "ITS Testing Pi", {"exclusive": "sign-in"}),
        "172.24.4.169": ("Bragg", "ITS Testing Pi", {}),
        "172.24.4.99": ("Bragg", "ITS Testing Pi", {"exclusive": "sign-in"}),
        "172.24.4.174": ("Bragg", "Testing Mark II Kiosk", {}),
        "172.24.4.175": ("Bragg", "Testing Mark II Kiosk", {}),
        "172.24.4.176": ("Bragg", "Testing Mark II Kiosk", {}),
        "131.111.73.91": ("Bragg", "ITS Testing Pi", {"exclusive": "sign-in"}),
        "172.24.109.175": ("Bragg", "Monitoring", {"exclusive": "sign-in"}),
        "127.0.0.1": ("Bragg", "Testing PC", {"contact": "low"}),
    },
    # All buildings, in order
    "BUILDINGS": [
        "Mott",
        "Bragg",
        "Link",
        "Rutherford",
        "PoM",
        "Maxwell",
        "Kapitza",
        "MRC",
        "Battcock",
    ],
    # Print emails only (no sending)
    "DEBUG_MAIL": True,
    # Contact details for whoever is running the system
    "CONTACT_EMAIL": "it.services@phy.cam.ac.uk",
    "CONTACT_NAME": "Cavendish Buildings Occupancy Register",
    # Send undeliverable mail here
    "BOUNCE_EMAIL": "ooh-bounces@phy.cam.ac.uk",
    # Nag users for missing emergency contact number
    # by sending email reminders
    "NAG_EMERGENCY_PHONE": True,
    # Show a big red warning if any of these fields are empty when
    # completing details for a self updating user
    "HIGHLIGHT_FIELDS": ["name", "rooms", "telephone", "emergency_telephone", "crsid"],
    # If any of the listed fields are empty, send a
    # nagging email reminder
    "NAG_MISSING_FIELDS": ["name", "rooms"],
    # The start of the day is the time at which all people
    # in the building are marked as signed out. It is a list
    # of times to allow for multiple sign-outs "HH:MM" 24h format
    "START_OF_DAY": ["7:30", "8:30"],
    # Time at which the day ends and an out of hours policy comes
    # into effect. The display of the signed in list can be switched
    # at this time.
    # On dual mode terminals, this is when visitor sign in becomes
    # the secondary mode and building sign in is the default for
    # out of hours monitoring.
    "END_OF_DAY": ["18:00"],
    # Any date for which the whole day should be considered out-of-hours
    # will not automatically sign out on these days
    # Format for dates is "yyyy-mm-dd"
    "NON_WORKDAYS": [
        "2019-12-25",  # Christmas Day
        "2019-12-26",  # Boxing Day
        "2019-12-27",  # Christmas closure (Friday)
        "2019-12-30",  # Christmas closure (Monday)
        "2019-12-31",  # Christmas closure (Tuesday)
        "2020-1-1",  # New Year's Day
        "2020-4-10",  # Good Friday
        "2020-4-13",  # Easter Monday
        "2020-5-8",  # Early May bank holiday
        "2020-5-25",  # Spring bank holiday
        "2020-8-31",  # Summer Bank Holiday
    ],
    # Weekend days where 0 is Monday
    "WEEKEND": [5, 6],
    # IP ranges that are considered on-site and given access to the
    # list of currently signed in people
    "NETWORKS": [
        "10.0.10.0/255.255.255.0",
        "10.0.7.0/255.255.255.0",
        "10.0.9.0/255.255.255.0",
        "131.111.67.0/255.255.255.192",
        "131.111.72.0/255.255.255.0",
        "131.111.73.0/255.255.255.0",
        "131.111.74.0/255.255.255.128",
        "131.111.76.0/255.255.255.0",
        "131.111.77.0/255.255.255.0",
        "131.111.79.0/255.255.255.128",
        "131.111.79.128/255.255.255.128",
        "172.20.191.0/255.255.255.0",
        "172.20.91.0/255.255.255.0",
        "172.24.106.0/255.255.255.0",
        "172.24.107.0/255.255.255.0",
        "172.24.108.0/255.255.255.0",
        "172.24.109.0/255.255.255.0",
        "172.24.216.0/255.255.255.0",
        "172.24.226.0/255.255.255.0",
        "172.24.227.0/255.255.255.0",
        "172.24.228.0/255.255.255.128",
        "172.24.252.0/255.255.255.192",
        "172.24.26.0/255.255.255.0",
        "172.24.36.0/255.255.255.0",
        "172.24.37.0/255.255.255.0",
        "172.24.4.0/255.255.255.0",
        "172.24.57.0/255.255.255.0",
        "172.24.58.0/255.255.255.0",
        "172.24.59.0/255.255.255.0",
        "172.24.60.0/255.255.255.0",
        "172.24.61.0/255.255.255.0",
        "172.24.62.0/255.255.255.0",
        "172.24.63.0/255.255.255.0",
        "172.24.64.0/255.255.255.0",
        "172.24.65.0/255.255.255.0",
        "172.31.102.0/255.255.255.0",
        "192.168.0.0/255.255.255.0",
        "193.60.89.48/255.255.255.240",
    ],
    # Whether to show signed-in list during the working day,
    # or during out-of-hours periods
    "SHOW_LIST_WORKDAY": True,
    "SHOW_LIST_OOH": True,
    # Allow users to hide their signed in status
    "ENABLE_INVISIBILITY": False,
    # Show events to users and admins
    "ENABLE_TRACKING": False,
    # How long to keep sign-in sign-out events, in days
    "RETENTION": 1,
    # How long to retain personal details of inactive profiles, in months
    "RETENTION_PROFILES": -1,
    # CRSid of users that can view additional information such as
    # stats and the system configuration
    "ADMINS": [],
    # Routes to show on the bus timetables
    "BUS_ROUTES": [
        {
            "stop_reference": "0500CCITY474",
            "title": ["JJ Thompson Avenue", "Towards Centre, Train Station"],
        },
        {
            "stop_reference": "0500CCITY425",
            "title": ["JJ Thompson Avenue", "Towards Eddington"],
        },
        {
            "stop_reference": "0500CCITY199",
            "title": ["Madingley Road", "Towards City Centre"],
        },
        {
            "stop_reference": "0500CCITY050",
            "title": ["Madingley Road", "Towards Bar Hill, Cambourne"],
        },
    ],
    ##
    # Visitor sign-in options
    ##
    # Show a different site name in visitor mode. Leave empty to
    # keep the same name.
    "VISITOR_SITE_NAME": None,
    "VISITOR_SITE_NAME_SUFFIX": None,
    # Site title for visitors should always be set as it's used to
    # differentiate between modes
    "VISITOR_SITE_TITLE": "Visitor Sign-In",
    # Super admins are pre-seeded in the system so they can add other
    # people from the card-reader interface
    "VISITOR_SUPER_ADMIN_MIFARE": [],
    # When interacting using as an admin, how much idle time to wait
    # before automatically logging out (in minutes)
    "SESSION_IDLE_TIMEOUT": 5,
}  # type: Dict[str, Any]

##
# Standard email responses
#
# Emails are sent for various actions, each template will be filled in
# with certain values:
# * EMERGENCY_NUMBER
#   Sent when emergency telephone number is missing, fills in
#   {name} and {url} pointing to edit page for the user
# * MISSING_DETAILS
#   Sent when the configured details are missing, fills in
#   {name}, {url} pointing to edit page for the user,
#   and {missing} as a list of the names of the missing
#   fields
# * CONFIRM_CRSID
#   Single use link for confirming a CRSid is linked to a card
#   fills in {crsid}, {name} and {url} pointing to the token

_EMAIL_EMERGENCY_NUMBER = """
Dear {name},

Thank you for using the Buildings Occupancy Register.

The system shows that you have not yet filled in your
emergency contact telephone number. This information
is essential in the event of an evacuation emergency.
The emergency number will only be used by the lab
emergency team and is not visible on any of the
terminals. If you need more information about this,
you can call me on the numbers below.

Please update your details through the web interface
at (Raven login required): {url}

Regards,

Saba Alai
Departmental Safety Officer
Room 250 Bragg Building
DD 44 (0)1223 337397 | Mob  44(0)7817 602858
"""

_EMAIL_EMERGENCY_NUMBER_HTML = """
<html>
  <head></head>
  <body>
    <p>Dear {name},</p>
    <p>Thank you for using the Buildings Occupancy Register.</p>
    <p>The system shows that you have not yet filled in your
       emergency contact telephone number. This information
       is <strong>essential</strong> in the event of an
       evacuation emergency.
       The emergency number will only be used by the lab
       emergency team and is not visible on any of the
       terminals. If you need more information about this,
       you can call me on the numbers below.
    </p>

    <p>Please update your details through the web interface
       at (Raven login required):
       <a href="{url}">
         {url}
       </a>
    </p>
    <p>Regards,</p>
    <p><strong>Saba Alai</strong><br />
       Departmental Safety Officer<br />
       Room 250 Bragg Building<br />
       DD 44 (0)1223 337397 | Mob  44(0)7817 602858
    </p>
  </body>
</html>
"""


_EMAIL_MISSING_DETAILS = """
Dear {name},

Thank you for using the Buildings Occupancy Register.

The system shows that your personal details are
currently incomplete. Your details are necessary to
help safety personnel quickly identify and locate
everyone that is on site in the event of an emergency.

The missing details you must complete are:
{missing}.

Please update your details through the web interface
at (Raven login required): {url}

Regards,

Saba Alai
Departmental Safety Officer
Room 250 Bragg Building
DD 44 (0)1223 337397 | Mob  44(0)7817 602858
"""


_EMAIL_MISSING_DETAILS_HTML = """
<html>
  <head></head>
  <body>
    <p>Dear {name},</p>
    <p>Thank you for using the Buildings Occupancy Register.</p>
    <p>The system shows that your personal details are
       currently incomplete. Your details are necessary to
       help safety personnel quickly identify and locate
       everyone that is on site in the event of an emergency.
    </p>
    <p><strong>The missing details you must complete are:
       {missing}.
    </strong></p>
    <p>Please update your details through the web interface
       at (Raven login required):
       <a href="{url}">
         {url}
       </a>
    </p>
    <p>Regards,</p>
    <p><strong>Saba Alai</strong><br />
       Departmental Safety Officer<br />
       Room 250 Bragg Building<br />
       DD 44 (0)1223 337397 | Mob  44(0)7817 602858
    </p>
  </body>
</html>
"""


_EMAIL_CONFIRM_CRSID = """
Dear {name},

Thank you for using the Buildings Occupancy Register.

You have requested for your CRSid, {crsid}, to be
linked to the card recently used at a kiosk.

Please follow the link below to confirm this action.
The link will expire in 24 hours. If you have any
questions, IT Services will be happy to help.

{url}

Regards,

IT Services
Cavendish Laboratory
"""

_EMAIL_CONFIRM_CRSID_HTML = """
<html>
  <head></head>
  <body>
    <p>Dear {name},</p>
    <p>Thank you for using the Buildings Occupancy Register.</p>
    <p>You have requested for your CRSid, <code>{crsid}</code>, to be
       linked to the card recently used at a kiosk.
    </p>

    <p>Please follow the link below to confirm this action.
       The link will expire in 24 hours. If you have any
       questions, IT Services will be happy to help.
    </p>
    <p>
      <a href="{url}">
        {url}
      </a>
    </p>
    <p>Regards,</p>
    <p>IT Services<br />
       Cavendish Laboratory</p>
  </body>
</html>
"""


EMAIL_TEMPLATES = {
    "EMERGENCY_NUMBER": {
        "PLAIN": _EMAIL_EMERGENCY_NUMBER,
        "HTML": _EMAIL_EMERGENCY_NUMBER_HTML,
    },
    "MISSING_DETAILS": {
        "PLAIN": _EMAIL_MISSING_DETAILS,
        "HTML": _EMAIL_MISSING_DETAILS_HTML,
    },
    "CONFIRM_CRSID": {"PLAIN": _EMAIL_CONFIRM_CRSID, "HTML": _EMAIL_CONFIRM_CRSID_HTML},
}
