"""
Send emails for nagging and verifying
"""

import smtplib

from email.message import EmailMessage
from email.headerregistry import Address
from typing import Optional

from ooh.data.configuration import EMAIL_TEMPLATES, SITE_TITLE
from ooh.data.configuration import DEBUG_MAIL
from ooh.data.configuration import CONTACT_EMAIL, CONTACT_NAME, BOUNCE_EMAIL


def send_mail(
    msg, server="ppsw.cam.ac.uk", bounce_addr=BOUNCE_EMAIL, debug_mail=DEBUG_MAIL
):
    """
    Decorate a message with the "From" header and and
    send it using the outgoing mailserver.

    Parameters
    ==========
    msg: EmailMessage
        A complete email message with headers already added for
        "Subject" and "To". "From" will be added before sending.
    server: str
        Address of the SMTP server to use for sending messages.
    bounce_addr: str or None
        If set, messages to unknown email addresses will be
        redirected to the specified address. If unset unknown
        addresses will be ignored
    debug_mail: bool
        If True, message will be `print`ed and not sent.

    Returns
    =======
    sent: bool
        True if the mail was sent, False if a problem was
        encountered.
    """

    # message is coming from us
    msg["From"] = Address(CONTACT_NAME, addr_spec=CONTACT_EMAIL)
    # Will be not valid if bounced, but start as okay
    valid = True

    # debugging, don't spam with bounces
    if debug_mail:
        print(msg)
        # Has been "Sent"
        return valid

    # Check the recipient can be resolved to a real address
    with smtplib.SMTP(server) as s:
        # empty payload required before testing address
        s.mail("")
        # probe for email validity
        if s.rcpt(msg["To"])[0] not in [250, 251]:
            if bounce_addr is None:
                return False  # don't bother to bounce, just finish
            # Make it into a bounce instead. __setitem__ is an append
            # for emails so replace all headers completely.
            new_subject = "Bounce: ({}) {}".format(msg["To"], msg["Subject"])
            bounce_address = Address(
                "{} Bounces".format(SITE_TITLE), addr_spec=BOUNCE_EMAIL
            )
            msg.replace_header("Subject", new_subject)
            msg.replace_header("To", bounce_address)
            msg.replace_header("From", bounce_address)
            valid = False

    # Send the message via whichever SMTP server is chosen.
    # Use clean connection as send_message expects it.
    with smtplib.SMTP(server) as s:
        try:
            s.send_message(msg)
        except smtplib.SMTPException:
            # some error when sending mail
            valid = False

    return valid


def nag_emergency_number(crsid, url, name=None):
    """
    Send a nag email to the user if they have not filled in their
    emergency phone number.

    Parameters
    ==========
    crsid: str
        User's CRSid, used to make an @cam.ac.uk email address.
    url: str
        Link to the user's edit page on the server, get this from
        url_for('view.scan', crsid=crsid, reader='edit', _external=True).
    name: str
        If supplied, this will be added to personalise the contents
        of the email.

    Returns
    =======
    sent: bool
        True if email was sent successfully, False if a problem was
        encountered (e.g. bounced).
    """
    # Ignore blanked names
    name = (name or "").strip() or crsid

    content = {
        subtype: text.format(name=name, url=url)
        for subtype, text in EMAIL_TEMPLATES["EMERGENCY_NUMBER"].items()
    }

    msg = EmailMessage()
    msg.set_content(content["PLAIN"])
    if "HTML" in content:
        msg.add_alternative(content["HTML"], subtype="html")
    msg["Subject"] = "Please add emergency contact details to {}".format(SITE_TITLE)
    msg["To"] = Address(name, crsid, "cam.ac.uk")

    return send_mail(msg)


def nag_incomplete_profile(crsid, missing_fields, url, name=None):
    """
    Send a nag email to the user if they have not filled in required
    fields in their profile.

    Parameters
    ==========
    crsid: str
        User's CRSid, used to make an @cam.ac.uk email address.
    url: str
        Link to the user's edit page on the server, get this from
        url_for('view.scan', crsid=crsid, reader='edit', _external=True).
    missing_fields: list of str
        Names of fields that are missing from the user's details; short
        names will be converted to long names.
    name: str
        If supplied, this will be added to personalise the contents
        of the email.

    Returns
    =======
    sent: bool
        True if email was sent successfully, False if a problem was
        encountered (e.g. bounced).
    """
    # Ignore blanked names
    name = (name or "").strip() or crsid

    field_names = {
        "name": "Display Name",
        "rooms": "Room Numbers",
        "telephone": "Contact Phone",
        "emergency_telephone": "Emergency Phone",
    }

    missing = ", ".join(
        [field_names.get(field_name, field_name) for field_name in missing_fields]
    )

    content = {
        subtype: text.format(name=name, missing=missing, url=url)
        for subtype, text in EMAIL_TEMPLATES["MISSING_DETAILS"].items()
    }

    msg = EmailMessage()
    msg.set_content(content["PLAIN"])
    if "HTML" in content:
        msg.add_alternative(content["HTML"], subtype="html")
    msg["Subject"] = "Please complete your details for {}".format(SITE_TITLE)
    msg["To"] = Address(name, crsid, "cam.ac.uk")

    return send_mail(msg)


def confirm_crsid(crsid: str, url: str, name: Optional[str] = None) -> bool:
    """
    Send a confirmation email for linking a CRSid to an account.

    Parameters
    ==========
    crsid
        User's CRSid, used to make an @cam.ac.uk email address
    url
        Link to the confirmation page, get this from
        url_for('view.confirm', key=key, _external=True)
    name
        If supplied, this will be added to personalise the contents
        of the email.

    Returns
    =======
    sent
        True if email was sent successfully, False if a problem was
        encountered (e.g. bounced).
    """

    # Ignore blanked names
    name = (name or "").strip() or crsid

    content = {
        subtype: text.format(name=name, crsid=crsid, url=url)
        for subtype, text in EMAIL_TEMPLATES["CONFIRM_CRSID"].items()
    }

    msg = EmailMessage()
    msg.set_content(content["PLAIN"])
    if "HTML" in content:
        msg.add_alternative(content["HTML"], subtype="html")
    msg["Subject"] = "Please confirm your CRSid for {}".format(SITE_TITLE)
    msg["To"] = Address(name, crsid, "cam.ac.uk")

    # Don't bounce these as might not exist
    return send_mail(msg, bounce_addr=None)
