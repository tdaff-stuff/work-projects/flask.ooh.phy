"""
Shared functions for web interface.


"""

from datetime import datetime, timedelta
from io import BytesIO
from typing import Optional, Any, Union, Callable, Type, List

import qrcode
import qrcode.image.svg
from flask import request, session, Request

from ooh.data.configuration import KIOSKS, SESSION_IDLE_TIMEOUT, Kiosk
from ooh.util.icon import clock_svg


def request_get(
    field: str, type: Union[Callable, Type] = str.strip, req: Request = request
) -> Optional[Union[str, Any]]:
    """
    Get a value from either the `form` or `args` field.

    Calls the get methods for the form data and args data
    on the request object. Empty values are returned as None.

    Parameters
    ==========
    field
        Name of the field to be retrieved
    type
        Method to call on the returned data, e.g. a type
        conversion or processing function.
    req
        A request object
    """
    # POSTed as form response
    value = req.form.get(field, type=type)
    if value:
        return value
    # Send as URL encoded parameters
    value = req.args.get(field, type=type)
    if value:
        return value

    # Empty value is None
    return None


def request_getlist(
    field: str, type: Union[Callable, Type] = str.strip, req: Request = request
) -> List[Union[str, Any]]:
    """
    Get a list of values from either the `form` or `args` field.
    Works with the MultiDict class.

    Calls the getlist methods for the form data and args data
    on the request object. Empty values are returned as an empty list.

    Parameters
    ==========
    field
        Name of the field to be retrieved
    type
        Method to call on each element of the returned data,
        e.g. a type conversion or processing function.
    req
        A request object
    """
    # POSTed as form response
    value = req.form.getlist(field, type=type)
    if value:
        return value
    # Send as URL encoded parameters
    value = req.args.getlist(field, type=type)
    if value:
        return value

    # Empty value is empty list
    return []


def get_raven_user(req=request):
    """
    Get the currently authenticated Raven user. Return None if not
    Raven authenticated.
    """
    if "X-AAPrincipal" in req.headers:
        # raven default for AAHeaders
        # crypto hash, single space, value
        remote_user = req.headers.get("X-AAPrincipal").split(" ", 1)[-1]
    else:
        remote_user = req.environ.get("REMOTE_USER", None)

    if remote_user in ["(null)"]:  # Kiosks bypass Raven
        remote_user = None

    return remote_user


def refresh_session(
    timeout: float = SESSION_IDLE_TIMEOUT,
    force: bool = False,
    referrer_required: bool = True,
) -> Optional[datetime]:
    """
    Refresh the session cookie by resetting the expiry to the idle timeout.
    If the session is expired, clear it completely unless the action
    is forced, for which the session is given the timeout (e.g. when
    initialising a new session.

    Parameters
    ----------
    timeout
        Length of the session in minutes.
    force
        Don't check for an existing session before adding the expiry.
        Use this to initialise a new session.
    referrer_required
        Enable hack that stops the forced refresh on kiosks from resetting
        the timout by ignoring requests with no referrer.

    Returns
    -------
    expiry
        The expire time that was set for the session.
    """
    expiry = datetime.now() + timedelta(minutes=timeout)

    if force:
        # Set a timeout without checking anything
        session["expiry"] = expiry
    elif "expiry" in session:
        # Check if the current session is still alive,
        # refresh if it is, otherwise clear it
        try:
            if session["expiry"] < datetime.now():
                session.clear()
            elif request.referrer or not referrer_required:
                # Only reset if we've come from a clicked link
                session["expiry"] = expiry
        except TypeError:
            # not a valid expiry, assume malicious and clear
            session.clear()
    elif session:
        # no session expiry found, but other data in
        # the session, clear it since it shouldn't be there
        # without an expiry
        session.clear()

    return session.get("expiry", None)


def get_current_kiosk(req: Request = request) -> Optional[Kiosk]:
    """
    Get the current kiosk. Return None if not a kiosk.
    """
    if req.remote_addr in KIOSKS:
        return KIOSKS[req.remote_addr]
    else:
        return None


def get_icon(base: str = "clock", resolution: int = 10, ext: str = "png") -> str:
    """
    Generate the url path name for an icon based on the current
    time, rounded to the given resolution.
    """
    now = datetime.now()
    #  round to the nearest resolution
    minute = (
        resolution * round((now.minute * 60 + now.second) / (60 * resolution))
    ) % 60
    # check if the hour has been rounded up
    if minute == 0 and now.minute > resolution:
        hours = (now.hour + 1) % 12 or 12
    else:
        hours = now.hour % 12 or 12

    return "{}-{:02d}-{:02d}.{}".format(base, hours, minute, ext)


def get_svg_icon(theme: Optional[str] = None) -> str:
    """
    Create the icon for the current time and return as a
    string version of the svg.
    """
    now = datetime.now()
    return clock_svg(hour=now.hour, minute=now.minute, modifier=theme, unique_id=True)


def get_svg_qr_code(text: str) -> str:
    """
    Create a qr code for the given text as an embeddable svg string.
    """
    # The .make_path() method only gets called implicitly in a .save()
    # so use a stream to extract the constructed svg image from the
    # saved version.
    stream = BytesIO()
    qr_code = qrcode.make(text, image_factory=qrcode.image.svg.SvgPathImage)
    qr_code.save(stream)
    qr_code_svg = stream.getvalue().decode("utf-8")
    return qr_code_svg
