"""
Generate IT services shield with clock face inside.
"""

import os
import sys
from pathlib import Path
from typing import Optional
from uuid import uuid4

from math import sin, cos, radians
from xml.etree import ElementTree as ETree


def clock_svg(
    hour: int,
    minute: int,
    clock_radius: float = 33,
    tick_size: float = 7,
    minute_size: float = 30,
    hour_size: float = 15,
    stroke_width: float = 6,
    modifier: Optional[str] = None,
    add_text: bool = False,
    unique_id: bool = False,
) -> str:
    """
    Create an svg file containing an IT Services shield with a clock
    face transparency mask.
    """

    # middle of shield
    cx = 50
    cy = 45

    if unique_id:
        mask_id = uuid4().hex[:8]
    else:
        mask_id = "glyph-mask"

    # create SVG XML element (see the SVG specification for attribute details)
    doc = ETree.Element(
        "svg",
        height="100%",
        width="100%",
        xmlns="http://www.w3.org/2000/svg",
        viewBox="0 0 100 100",
    )

    # mask group is the transparency mask
    mask = ETree.SubElement(ETree.SubElement(doc, "mask", id=mask_id), "g")

    # full opaque background
    ETree.SubElement(
        mask, "rect", x="0", y="0", width="100", height="100", fill="white"
    )

    clock = ETree.SubElement(mask, "g")

    # clock face
    ETree.SubElement(
        clock,
        "circle",
        attrib={
            "cx": str(cx),
            "cy": str(cy),
            "r": str(clock_radius),
            "stroke": "black",
            "fill": "none",
            "stroke-width": str(stroke_width),
        },
    )

    # make internal ticks
    for tick_angle in range(0, 360, 30):
        ETree.SubElement(
            clock,
            "line",
            attrib={
                "x1": "{:.3f}".format(
                    cx + (clock_radius - tick_size) * sin(radians(tick_angle))
                ),
                "y1": "{:.3f}".format(
                    cy + (clock_radius - tick_size) * cos(radians(tick_angle))
                ),
                "x2": "{:.3f}".format(
                    cx + (clock_radius - 1) * sin(radians(tick_angle))
                ),
                "y2": "{:.3f}".format(
                    cy + (clock_radius - 1) * cos(radians(tick_angle))
                ),
                "stroke": "black",
                "stroke-width": str(stroke_width),
                "stroke-linecap": "round",
            },
        )

    # Hands
    hour_angle = 30 * (hour + (minute / 60.0))
    minute_angle = 6 * minute

    # Minute Hand
    ETree.SubElement(
        clock,
        "line",
        attrib={
            "x1": "{:.3f}".format(cx),
            "y1": "{:.3f}".format(cy),
            "x2": "{:.3f}".format(cx + (minute_size * sin(radians(minute_angle)))),
            "y2": "{:.3f}".format(cy - (minute_size * cos(radians(minute_angle)))),
            "stroke": "black",
            "stroke-width": str(stroke_width),
            "stroke-linecap": "round",
            "class": "minute-hand",
        },
    )

    # Hour Hand
    ETree.SubElement(
        clock,
        "line",
        attrib={
            "x1": "{:.3f}".format(cx),
            "y1": "{:.3f}".format(cy),
            "x2": "{:.3f}".format(cx + (hour_size * sin(radians(hour_angle)))),
            "y2": "{:.3f}".format(cy - (hour_size * cos(radians(hour_angle)))),
            "stroke": "black",
            "stroke-width": str(stroke_width),
            "stroke-linecap": "round",
            "class": "hour-hand",
        },
    )

    if modifier == "dark":
        fill = "#503872"
    elif modifier == "night":
        fill = "#fff"
    else:
        fill = "#422e5d"

    # IT Services Shield
    ETree.SubElement(
        doc,
        "polygon",
        points="7,0 93,0 93,78.6 50,100 7,78.6",
        fill=fill,
        mask="url(#{})".format(mask_id),
    )

    if add_text:
        # wider box to include text
        doc.set("width", "370")
        doc.set("viewBox", "0 0 370 100")
        # bold chat
        chat = ETree.SubElement(doc, "text", x="120", y="75", fill=fill)
        chat.set("font-weight", "bold")
        chat.set("font-family", "Lato")
        chat.set("font-size", "64")
        chat.text = "ooh"
        # normal phy
        chat = ETree.SubElement(doc, "text", x="230", y="75", fill=fill)
        chat.set("font-family", "Lato")
        chat.set("font-size", "64")
        chat.text = ".phy"
        # different name

    return ETree.tostring(doc, encoding="unicode")


def create_all(out_dir: str) -> None:
    """Create all"""

    os.makedirs(out_dir, exist_ok=True)

    suffix = {None: "", "dark": "-dark", "night": "-night"}
    fmt = "clock{}-{:02d}-{:02d}.svg"
    out_path = Path(out_dir)

    for theme in [None, "dark", "night"]:
        out_file = out_path / "ooh-phy{}.svg".format(suffix[theme])
        out_file.write_text(clock_svg(10, 10, modifier=theme, add_text=True))
        for hour in range(1, 13):
            for minute in range(0, 60, 10):
                out_file = out_path / fmt.format(suffix[theme], hour, minute)
                out_file.write_text(clock_svg(hour, minute, modifier=theme))


if __name__ == "__main__":
    # Specify the output directory on the commandline
    if len(sys.argv) == 2:
        write_dir = sys.argv[1]
    else:
        write_dir = "."

    create_all(write_dir)
