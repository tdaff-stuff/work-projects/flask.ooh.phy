"""
General utilities for working with .cam data.
"""

from typing import Optional, List

from ibisclient.methods import PersonMethods, InstitutionMethods
from ibisclient.connection import createConnection, IbisException


def crsid_to_name(crsid: str) -> Optional[str]:
    """
    Get the name that is set for the given CRSid at lookup.cam.
    Hopefully this is the name they've chosen and the way they
    like it shown.
    If there is any issue, then just return None.
    """
    try:
        api = PersonMethods(createConnection())
        person = api.getPerson(scheme="crsid", identifier=crsid)
        return person.visibleName
    except (IbisException, AttributeError, KeyError):
        # Catch either an error connecting to lookup,
        # or a non-existent person, and just return None.
        return None


def child_insts_recursive(
    instid: str,
    all_children: Optional[List[str]] = None,
    api: Optional[InstitutionMethods] = None,
) -> List[str]:
    """
    Find all the child institutions of the given institution and add them
    to a list. Also add their children recursively.

    Pulls the list from the lookup.cam with API calls for each institution
    so it's best to cache the result.

    Parameters
    ----------
    instid
        Target institution to be queried.
    all_children
        Working list of children. Modified in place with the results. Leave
        unset to create an empty list at the top level. Pass an empty
        list to include the parent.
    api
        If an API has already been created, it can be reused here.
        If not, one will be created and passed to any recursive calls.

    Returns
    -------
    all_children
        The full ist of all children of the institution.
    """
    # Lookup API
    if api is None:
        api = InstitutionMethods(createConnection())

    # Find the given institution
    institution = api.getInst(instid, fetch="child_insts")

    # If the institution does not exist, then we skip it
    if institution is None:
        return all_children or []

    # Create a working list here if one is not given so a user does not
    # need to supply one.
    if all_children is None:
        all_children = []
    else:
        all_children.append(instid)

    for child in institution.childInsts:
        if not child.cancelled and child.instid not in all_children:
            child_insts_recursive(child.instid, all_children=all_children, api=api)

    return all_children
