"""Add a timestamp for tracking last activity of a user

Revision ID: 1993c2c292be
Revises: 702741414074
Create Date: 2020-09-02 15:38:10.112832

"""
from datetime import datetime

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "1993c2c292be"
down_revision = "702741414074"
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table("people", schema=None) as batch_op:
        batch_op.add_column(sa.Column("last_active", sa.Integer(), nullable=True))

    # ### end Alembic commands ###

    # Ensure that timestamp is initialised (to current time)
    op.execute("UPDATE people SET last_active = {}".format(datetime.now().timestamp()))


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    with op.batch_alter_table("people", schema=None) as batch_op:
        batch_op.drop_column("last_active")

    # ### end Alembic commands ###
