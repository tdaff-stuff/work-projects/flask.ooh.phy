"""
Database of reset tokens.
"""

from datetime import datetime, timedelta
from typing import Optional
from uuid import uuid4

from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, scoped_session

from ooh.data.configuration import TOKEN_DB

Base = declarative_base()


class Token(Base):
    """
    A one-time token to store a piece of information.
    Has a hexadecimal request identifier which is used
    to retrieve the stored information.
    """

    __tablename__ = "tokens"

    key = Column(String, primary_key=True)  # request identifier (use UUID!)
    value = Column(String)  # stored information released with the key
    expiry = Column(DateTime)  #
    uses = Column(Integer)  # token removed after so many uses


class TokenDatabase(object):
    """Creating, managing and using tokens."""

    def __init__(self, db_url: Optional[str] = None):
        if db_url is None:
            db_url = TOKEN_DB

        # stop thread errors
        if db_url.startswith("sqlite://"):
            connect_args = {"check_same_thread": False}
        else:
            connect_args = {}

        # Properties accessible as attributes
        self.url = db_url
        self.base = Base
        # Engine used to connect to the database
        self.engine = create_engine(db_url, connect_args=connect_args)
        # Bind a session instance to the instance -- easier to query itself
        self.session = scoped_session(sessionmaker(bind=self.engine, autoflush=True))

    def generate_token(self, value, valid_hours=None, max_uses=None):
        """
        Generate a new token and return the id.

        Parameters
        ==========
        value: str
            Information to be stored.
        valid_hours: Number
            If set, token will remain valid for only for the specified
            length of time.
        max_uses: int
            If set, will expire the token once it has been accessed
            the set number of times.
        """
        token_key = uuid4().hex

        # collision detection, should be 1 in 10^37 or something improbable
        if self.session.query(Token).get(token_key):
            return self.generate_token(value, valid_hours, max_uses)

        # timed expiry
        if valid_hours is not None and valid_hours > 0:
            expiry = datetime.now() + timedelta(hours=valid_hours)
        else:
            expiry = None

        # usage expiration
        if max_uses is not None and max_uses > 0:
            uses = max_uses
        else:
            uses = None

        new_token = Token(key=token_key, value=value, expiry=expiry, uses=uses)

        self.session.add(new_token)
        self.session.commit()

        return new_token.key

    def get_value(self, key):
        """
        Retrieve the value associated with the key.

        Parameters
        ==========
        key: str
            Hexadecimal key associated with the value.

        Returns
        =======
        value: str
            The value stored in the database.

        Raises
        ======
        KeyError
            No value associated with the key
        """

        # Pre-remove any expired tokens
        # other methods can assume all remaining tokens are valid
        self.flush_expired_tokens()

        db_token = self.session.query(Token).get(key)

        if db_token is None:
            raise KeyError("Token not found or expired")

        if db_token.uses is not None:
            if db_token.uses <= 1:
                self.session.delete(db_token)
            else:
                db_token.uses -= 1
            self.session.commit()

        return db_token.value

    def flush_expired_tokens(self):
        """
        Remove all tokens that are expired. Returns number of
        expired tokens.
        """
        expired = (
            self.session.query(Token)
            .filter(Token.expiry.isnot(None))
            .filter(Token.expiry < datetime.now())
            .delete()
        )
        used = (
            self.session.query(Token)
            .filter(Token.uses.isnot(None))
            .filter(Token.uses < 1)
            .delete()
        )

        self.session.commit()

        return expired + used

    def expire_token(self, key):
        """
        Remove a specific key, even if it has not expired yet.

        Parameters
        ==========
        key: str
            Hexadecimal key associated with the value to be removed.

        Returns
        =======
        removed: bool
            True if something has been removed.
        """

        expired = self.session.query(Token).filter(Token.key == key).delete()
        self.session.commit()

        return expired
