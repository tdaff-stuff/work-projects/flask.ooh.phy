"""
Storage of events

"""
import datetime
import time
from typing import Optional, List, Set, Dict, Union

from sqlalchemy import Column, Integer, String, Float, Enum, DateTime
from sqlalchemy import create_engine
from sqlalchemy.exc import IntegrityError
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, scoped_session

from ooh.data.configuration import last_start_of_day, ENTRIES_DB

Base = declarative_base()


class Scan(Base):
    """
    A card scan event on the kiosk.
    """

    __tablename__ = "entry"

    id = Column(Integer, primary_key=True)
    crsid = Column(String)
    mifare = Column(String)  # can be non-integer for test cards
    reader = Column(Enum("yellow", "purple", name="reader_enum"))
    timestamp = Column(Float)
    location = Column(String)

    def to_dict(self):
        as_dict = {
            "crsid": self.crsid,
            "mifare": self.mifare,
            "reader": self.reader,
            "timestamp": self.timestamp,
            "location": self.location,
        }
        return as_dict


class Usage(Base):
    """
    Anonymous usage statistics - one data point per entry.
    """

    __tablename__ = "usage"

    id = Column(Integer, primary_key=True)
    timestamp = Column(DateTime, default=datetime.datetime.now, nullable=False)
    total = Column(Integer, default=0, nullable=False)
    unique = Column(Integer, default=0, nullable=False)
    actions = Column(Integer, default=0, nullable=False)


class OOHDatabase(object):
    """Abstraction querying a database."""

    def __init__(self, db_url: Optional[str] = None):
        if db_url is None:
            db_url = ENTRIES_DB

        # stop thread errors
        if db_url.startswith("sqlite://"):
            connect_args = {"check_same_thread": False}
        else:
            connect_args = {}

        # Properties accessible as attributes
        self.url = db_url
        self.base = Base
        # Engine used to connect to the database
        self.engine = create_engine(db_url, connect_args=connect_args)
        # Bind a session instance to the instance -- easier to query itself
        self.session = scoped_session(sessionmaker(bind=self.engine, autoflush=True))

    def register(self, crsid, mifare, reader, timestamp, location):
        """
        Register a scan event and return the event data.
        """
        try:
            new_scan = Scan(
                crsid=crsid,
                mifare=mifare,
                reader=reader,
                timestamp=timestamp,
                location=location,
            )

            self.session.add(new_scan)
            self.session.commit()
        except IntegrityError:  # invalid value for reader
            new_scan = None

        return new_scan

    def register_stats(self, since: Optional[float] = None) -> None:
        """
        Log anonymous stats for the current time. Just appends a new data
        point to the series.
        """
        if since is None:
            since = last_start_of_day()

        # timestamp defaults to current time

        usage = Usage(
            # The number of users that are currently signed in
            total=len(self.signed_in(since=since)),
            # The number of users that have used the system today
            unique=self.session.query(Scan.crsid, Scan.mifare)
            .filter(Scan.timestamp > since)
            .distinct()
            .count(),
            # The number of interactions with the system today
            actions=self.session.query(Scan).filter(Scan.timestamp > since).count(),
        )
        self.session.add(usage)
        self.session.commit()

    def backfill_users(self, crsid, mifare):
        """
        Ensure that crsid and mifare pairs are the same in
        each row. Add the given values for each entry in the
        database where one is missing.

        Parameters
        ==========
        crsid: str
            Persons CRSid
        mifare: str
            Card's MIFARE number
        """
        # Don't change if one is missing
        if not all([crsid, mifare]):
            return None

        backfill = (
            self.session.query(Scan)
            .filter((Scan.crsid == crsid) | (Scan.mifare == mifare))
            .update({Scan.crsid: crsid, Scan.mifare: mifare})
        )
        self.session.commit()

        return backfill

    def clear_records(self, retention: float = 0, since: Optional[float] = None) -> int:
        """
        Purge any event records that are older than the
        number of retention days before the current start
        of day.

        As an alternative can just nuke the database
        during a working day.

        Parameters
        ==========
        retention
            Number of days to keep records from the start
            of the current day.
        since
            Timestamp to override the "start_of_day". Default
            is current start of day.

        """
        if retention is None or retention < 0:
            # Do not purge any records
            return 0

        if since is None:
            since = last_start_of_day()
            # If start of day is not configured, try and find a workday,
            # otherwise just use retention from now.
            if since == 0:
                since = last_start_of_day([datetime.time(0, 0)]) or time.time()

        retention_timestamp = since - retention * 86400  # in seconds

        cleared = (
            self.session.query(Scan)
            .filter(Scan.timestamp < retention_timestamp)  # find old events
            .delete()
        )

        self.session.commit()

        return cleared

    def purge_user_records(
        self, crsid: Optional[str] = None, mifare: Optional[str] = None
    ) -> int:
        """
        Remove all activity records referring to the given user.

        Parameters
        ==========
        crsid
            Persons CRSid
        mifare
            Card's MIFARE number

        Returns
        =======
        purged
            Number of records that have been removed
        """

        purged = (
            self.session.query(Scan)
            .filter(
                (Scan.crsid.isnot(None) & (Scan.crsid == crsid))
                | (Scan.mifare.isnot(None) & (Scan.mifare == mifare))
            )
            .delete()
        )
        self.session.commit()

        return purged

    def signed_in(
        self, reader: str = "yellow", since: Optional[float] = None
    ) -> List[Scan]:
        """
        List people who last registered as signing in. Will ignore events
        older than the last working day, by default. Otherwise will cutoff
        at the given timestamp.

        Assumes the database has been backfilled so that CRSid and MIFARE
        will be consistent on all events for the same person.

        Parameters
        ==========
        reader
            Name of action to find, 'yellow' for sign in, 'purple'
            for sign out.
        since
            UNIX timestamp to cut off all events. Anything before that
            time will be ignored.

        Returns
        =======
        scanned
            All scan events that correspond to a most recent event within
            the timescale.
        """

        if since is None:
            since = last_start_of_day()

        # Unordered list of most recent scan on desired reader
        latest_scans = []

        # Retrieve all scans from today that have a CRSid, and
        # separately scans with only MIFARE values
        crsid_scans = (
            self.session.query(Scan)  # find a scan event
            .filter(Scan.crsid.isnot(None))  # Non empty CRSid
            .filter(Scan.timestamp > since)  # ignore old events
            .order_by(Scan.timestamp.desc())  # most recent first
        )

        mifare_scans = (
            self.session.query(Scan)  # find a scan event
            .filter(Scan.crsid.is_(None))  # Empty CRSid
            .filter(Scan.timestamp > since)  # ignore old events
            .order_by(Scan.timestamp.desc())  # most recent first
        )

        # Only look at the first scan for a person and only store it
        # if it is the correct reader. This skips people whose last
        # event was an "out" action
        seen = set()  # type: Set[Scan]
        for scan in crsid_scans:
            if scan.crsid not in seen:
                seen.add(scan.crsid)
                if scan.reader == reader:
                    latest_scans.append(scan)

        # MIFARE entries can be treated separately as long as they
        # are consistent with CRSids
        seen = set()
        for scan in mifare_scans:
            if scan.mifare not in seen:
                seen.add(scan.mifare)
                if scan.reader == reader:
                    latest_scans.append(scan)

        return latest_scans

    def user_signed_in(self, crsid=None, mifare=None, since=None):
        """
        Determine if a user is currently considered signed in or not.

        Parameters
        ==========
        crsid: str
            Persons CRSid
        mifare: str
            Card's MIFARE number
        since: float
            Timestamp of earliest event to consider, otherwise
            use the previous start of day.

        Returns
        =======
        signed_in: Scan or None
            If the user is signed in, return the last scan event,
            otherwise return None.
        """

        if since is None:
            since = last_start_of_day()

        last = (
            self.session.query(Scan)
            .filter(
                ((Scan.crsid.isnot(None)) & (Scan.crsid == crsid))
                | ((Scan.mifare.isnot(None)) & (Scan.mifare == mifare))
            )
            .order_by(Scan.timestamp.desc())
            .filter(Scan.timestamp > since)
        ).first()

        # only return if the last event today was a yellow
        if last is None or last.reader == "purple":
            return None
        else:
            return last

    def scans(
        self, crsid: Optional[str] = None, since: Optional[float] = None
    ) -> Optional[List[Dict]]:
        """
        Get all scan events for everyone or a specific person.

        Parameters
        ----------
        crsid
            If given, only show events for the given user.
        since
            Restrict to a given time range. If not set, events from
            all time are returned.

        Returns
        -------
        events
            Chronological list of scan events. Newest first.
        """

        # Just get all scans in the database
        events = self.session.query(Scan).order_by(Scan.timestamp.desc())

        # Only apply user filter if required
        if crsid is not None:
            events = events.filter(Scan.crsid == crsid)

        if since is not None:
            events = events.filter(Scan.timestamp > since)

        return events.all()

    def active_crsids(self) -> List[str]:
        """Get a list of CRSids that have records active in the system."""
        crsid_query = self.session.query(Scan.crsid).distinct()
        # Results are tuples of one item -- the CRSid
        return [result[0] for result in crsid_query if result[0]]

    def active_mifare(self) -> List[str]:
        """Get a list of MIFARE numbers that have records in the system."""
        mifare_query = self.session.query(Scan.mifare).distinct()
        # Results are tuples of one item -- the MIFARE
        return [result[0] for result in mifare_query if result[0]]

    def usage(
        self, since: Optional[datetime.datetime] = None
    ) -> Dict[str, Union[List[str], List[int]]]:
        """
        Get the anonymous, ordered historic usage stats for the
        building sign in service.

        Parameters
        ----------
        since
            Only report as far back as the requested date.

        Returns
        -------
        stats
            A dictionary containing the items, "timestamp", "total",
            "unique" and "actions"
        """
        # Gather all data points in chronological order
        series = self.session.query(Usage).order_by(Usage.timestamp.asc())

        if since is not None:
            series = series.filter(Usage.timestamp > since)

        data = series.all()

        # Plotly requires isoformat dates
        return {
            "timestamp": [item.timestamp.isoformat() for item in data],
            "total": [item.total for item in data],
            "unique": [item.unique for item in data],
            "actions": [item.actions for item in data],
        }
