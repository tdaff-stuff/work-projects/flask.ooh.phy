"""
Database of people.
"""
from datetime import datetime, timedelta
from typing import Optional, List, Any, Dict, Iterable

from sqlalchemy import Table, Column, Integer, String, ForeignKey, Boolean
from sqlalchemy import create_engine, or_
from sqlalchemy.exc import IntegrityError
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, joinedload
from sqlalchemy.orm import sessionmaker, scoped_session

from ooh.data.configuration import LOOKUP_DB
from ooh.util.cam import crsid_to_name

Base = declarative_base()

# Number of seconds in an average month (365.2425 days a year)
MONTH_ISH = 31556952 // 12


def now_ish(dt: Optional[datetime] = None, resolution: float = MONTH_ISH) -> int:
    """
    Reduced resolution timestamp.

    Defaults to approximately now, with a resolution of around a month,
    and truncated division. It is returned as a UNIX timestamp.

    Parameters
    ----------
    dt
        The datetime object to use. Leave empty to use the current
        time.
    resolution
        The granularity of the timestamp, in seconds.
    """
    if dt is None:
        dt = datetime.now()

    return int(resolution * (dt.timestamp() // resolution))


# two way relationships with people and buildings
building_occupancy = Table(
    "building_occupancy",
    Base.metadata,
    Column("people_id", Integer, ForeignKey("people.id")),
    Column("buildings_name", String, ForeignKey("buildings.name")),
)


class Building(Base):
    """
    A building in the Cavendish. For people associated with more than
    one building.
    """

    __tablename__ = "buildings"

    name = Column(String, primary_key=True, unique=True)


class Person(Base):
    """
    A person and any information needed for the building sign in system.

    A cambridge person will have a crsid, and possibly a mifare if they
    have scanned their card. Someone with just a card will only have
    a mifare.
    """

    __tablename__ = "people"

    id = Column(Integer, primary_key=True)  # internal identifier
    crsid = Column(String, unique=True)  # most people should have one
    mifare = Column(String, unique=True)  # test cards non-integer
    cardholder = Column(String, unique=True)  # TODO: visible on card?
    name = Column(String)  # Name as the person would like it displayed
    rooms = Column(String)  # Freeform info on where they can be found
    telephone = Column(String)  # Freeform contact information
    emergency_telephone = Column(String)  # Hidden phone number
    invisible = Column(Boolean, default=False)
    last_active = Column(Integer, default=now_ish)  # Granular timestamp
    buildings = relationship(
        "Building", secondary=building_occupancy, backref="people", uselist=True
    )

    def update_values(
        self,
        crsid: Optional[str] = None,
        mifare: Optional[str] = None,
        cardholder: Optional[str] = None,
        name: Optional[str] = None,
        rooms: Optional[str] = None,
        telephone: Optional[str] = None,
        emergency_telephone: Optional[str] = None,
        invisible: Optional[bool] = None,
        buildings: Optional[List[Building]] = None,
    ) -> None:
        """
        Update any values that are different from the current ones.

        Parameters
        ==========
        crsid
            Person's CRSid identifier
        mifare
            Smartcard identifying number or string
        cardholder
            Some visible string on the card?
        name
            Person's name as they like it displayed
        rooms
            Rooms that the person will be found in
        telephone
            Contact information
        emergency_telephone
            Hidden contact information - emergency only
        invisible
            Whether user should be hidden from displayed lists
        buildings: list of Building
            Names of buildings where the person may be found
        """
        if crsid is not None and crsid != self.crsid:
            self.crsid = crsid.strip()
        if mifare is not None and mifare != self.mifare:
            self.mifare = mifare.strip()
        if cardholder is not None and cardholder != self.cardholder:
            self.cardholder = cardholder.strip()
        if name is not None and name != self.name:
            self.name = " ".join(name.split())
        if rooms is not None and rooms != self.rooms:
            self.rooms = " ".join(rooms.split())
        if telephone is not None and telephone != self.telephone:
            self.telephone = " ".join(telephone.split())
        if (
            emergency_telephone is not None
            and emergency_telephone != self.emergency_telephone
        ):
            self.emergency_telephone = " ".join(emergency_telephone.split())
        if invisible is not None and invisible != self.invisible:
            self.invisible = invisible
        if buildings is not None:
            self.buildings = buildings

    def touch(self) -> int:
        """
        Update the last active time of the profile to now-ish.

        Resolution of the timestamp is reduced to around a month,
        so the timestamp recorded will be sometime within the
        preceding ~30.4 days.
        """
        self.last_active = now_ish()
        return self.last_active

    def to_dict(self) -> Dict[str, Any]:
        """JSON-able representation of the person."""
        as_dict = {
            "crsid": self.crsid,
            "mifare": self.mifare,
            "cardholder": self.cardholder,
            "name": self.name,
            "rooms": self.rooms,
            "telephone": self.telephone,
            "emergency_telephone": self.emergency_telephone,
            "invisible": self.invisible,
            "buildings": [building.name for building in self.buildings],
        }
        return as_dict


class PeopleDatabase(object):
    """Abstraction querying a database of crsid/name."""

    def __init__(self, db_url: Optional[str] = None):
        if db_url is None:
            db_url = LOOKUP_DB

        # stop thread errors
        if db_url.startswith("sqlite://"):
            connect_args = {"check_same_thread": False}
        else:
            connect_args = {}

        # Properties accessible as attributes
        self.url = db_url
        self.base = Base
        # Engine used to connect to the database
        self.engine = create_engine(db_url, connect_args=connect_args)
        # Bind a session instance to the instance -- easier to query itself
        self.session = scoped_session(sessionmaker(bind=self.engine, autoflush=True))

    def get_person(
        self,
        crsid: Optional[str] = None,
        mifare: Optional[str] = None,
        cardholder: Optional[str] = None,
        update_identifiers: bool = False,
        touch: bool = False,
    ) -> Optional[Person]:
        """
        Find a person, given either their CRSid or MIFARE number. If both
        are provided, and update_identifiers is set, then update to include
        both values. Users may have either or both identifiers, check CRSid
        first.

        Parameters
        ==========
        crsid
            Person's CRSid
        mifare
            Unique smartcard chip identifier.
        cardholder
            String that appears on some cards (not implemented).
        update_identifiers
            If True, mifare will be updated to match the CRSid.
        touch
            If True the person's last active date will be updated.
        """
        person_q = self.session.query(Person)
        person: Optional[Person] = None

        # CRSid known, use it preferentially
        if crsid is not None:
            person = person_q.filter(Person.crsid == crsid).first()

        # not found with CRSid, try MIFARE
        if person is None and mifare is not None:
            person = person_q.filter(Person.mifare == mifare).first()

        # not currently in use
        if person is None and cardholder is not None:
            person = person_q.filter(Person.cardholder == cardholder).first()

        # if found a person, and identifiers provided,
        # safe to use update_identifiers as it will not update blank values
        if person is not None and update_identifiers:
            person.update_values(crsid=crsid, mifare=mifare, cardholder=cardholder)

        if person is not None and touch:
            person.touch()

        if person is not None and self.session.is_modified(person):
            try:
                self.session.commit()
            except IntegrityError:  # constraint broken
                self.session.rollback()

        return person

    def get_people(
        self,
        crsid: Optional[Iterable[str]] = None,
        mifare: Optional[Iterable[str]] = None,
        cardholder: Optional[Iterable[str]] = None,
    ) -> List[Person]:
        """
        Find all people that match any of the CRSids or MIFARE numbers.
        Implemented as a single query to reduce the scaling issues
        with one query per person.

        Parameters
        ==========
        crsid
            A list of all CRSid values to search for in the database
        mifare
            A list of all MIFARE values to search for in the database
        cardholder
            A list of all cardholder values to search for in the database

        Returns
        =======
        people
            Any people matching any of the identifiers given
        """
        if crsid is None:
            crsid = []
        if mifare is None:
            mifare = []
        if cardholder is None:
            cardholder = []

        # Use filter(None, x) to remove empty values
        # Query gives us all the people we are looking for,
        # using any of the identifiers they have
        # Convert all values to set to remove duplicates and avoid hitting
        # sqlite limits (as low as 999 parameter substitutions).
        person_query = (
            self.session.query(Person)
            .options(joinedload(Person.buildings))  # always load buildings
            .filter(
                or_(
                    Person.crsid.in_(filter(None, set(crsid))),
                    Person.mifare.in_(filter(None, set(mifare))),
                    Person.cardholder.in_(filter(None, set(cardholder))),
                )
            )
        )
        return person_query.all()

    def delete_inactive_profiles(
        self,
        retention: float = -1,
        exclude_crsid: Optional[List[str]] = None,
        exclude_mifare: Optional[List[str]] = None,
    ) -> List[Person]:
        """
        Find all the people that have not been active within the specified
        timeframe and remove their profiles from the database. Note that
        the retention period is an absolute cutoff for the inactive time,
        so that anything longer than that will always be included and, due
        to the resolution of the timestamps recorded, anything up to a month
        sooner may also be included. E.g. a retention of 12 months means
        that any profiles have been inactive for 12 months or more are
        guaranteed to be removed and any profiles that have
        been active within the past 11 months are guaranteed to not be
        removed.

        A retention of less than 0 will disable the check. A retention of 0
        will remove everyone.

        It is expected that a list of all users with active sign-in entries
        is compiled and passed to the exclude_ arguments to prevent any
        of them being removed.

        Parameters
        ----------
        retention
            Maximum age in months to consider a profile inactive.
        exclude_crsid, exclude_mifare
            A list of CRSids and MIFARE numbers to ignore even if
            they are marked as inactive.
            Probably obtained from the entries database.
        """
        # No activity cutoff
        if retention < 0:
            return []

        # Last active is quantised, so can check against continuous
        # value for the cutoff relative to now.
        age = timedelta(seconds=retention * MONTH_ISH)
        active_cutoff = (datetime.now() - age).timestamp()
        people = self.session.query(Person).filter(Person.last_active < active_cutoff)

        # Filter out people that have identifiers in the exclusion list
        # People with empty identifiers need to be explicitly included
        if exclude_crsid is None:
            exclude_crsid = []
        if exclude_mifare is None:
            exclude_mifare = []

        people = people.filter(
            Person.crsid.is_(None) | Person.crsid.notin_(exclude_crsid)
        ).filter(Person.mifare.is_(None) | Person.mifare.notin_(exclude_mifare))

        deleted = []
        for person in people:
            deleted.append(person)
            self.session.delete(person)

        self.session.commit()

        return deleted

    def get_db_buildings(
        self, buildings: Optional[List[str]], create: bool = True
    ) -> Optional[List[Building]]:
        """
        For methods that require a list of buildings as references to
        objects in the database, convert from a list of strings.
        """
        db_buildings = None
        if buildings is not None and create:
            db_buildings = [
                self.session.query(Building).get(building) or Building(name=building)
                for building in buildings
            ]
        elif buildings is not None and not create:
            db_buildings = [
                self.session.query(Building).get(building) for building in buildings
            ]
        return db_buildings

    def create_person(
        self,
        crsid: Optional[str] = None,
        mifare: Optional[str] = None,
        cardholder: Optional[str] = None,
        name: Optional[str] = None,
        rooms: Optional[str] = None,
        telephone: Optional[str] = None,
        emergency_telephone: Optional[str] = None,
        invisible: Optional[bool] = None,
        buildings: Optional[List[str]] = None,
    ) -> Optional[Person]:
        """
        Create a new person in the database. Creates a blank person then
        updates their details before committing to the database.

        Parameters
        ==========
        crsid
            Person's CRSid identifier
        mifare
            Smartcard identifying number
        cardholder
            Some visible string on the card?
        name
            Person's name as the like it displayed
        rooms
            Rooms that the person will be found in
        telephone
            Contact information
        emergency_telephone
            Hidden contact information - emergency only
        invisible
            Whether user should be hidden from displayed lists
        buildings
            Names of buildings where the person may be found
        """
        try:
            new_person = Person()
            new_person.update_values(
                crsid=crsid,
                mifare=mifare,
                cardholder=cardholder,
                name=name or crsid_to_name(crsid),
                rooms=rooms,
                telephone=telephone,
                emergency_telephone=emergency_telephone,
                invisible=invisible,
                buildings=self.get_db_buildings(buildings),
            )
            self.session.add(new_person)
            self.session.commit()
        except IntegrityError:  # unique constraint on crsid broken?
            self.session.rollback()
            return None

        return new_person

    def update_person(
        self,
        create: bool = True,
        crsid: Optional[str] = None,
        mifare: Optional[str] = None,
        cardholder: Optional[str] = None,
        name: Optional[str] = None,
        rooms: Optional[str] = None,
        telephone: Optional[str] = None,
        emergency_telephone: Optional[str] = None,
        invisible: Optional[bool] = None,
        buildings: Optional[List[str]] = None,
    ) -> Optional[Person]:
        """
        Update a person in the database. Optionally create that person.

        Parameters
        ==========
        create
            If True, a new person will be created if one does not exist
        crsid
            Person's CRSid identifier
        mifare
            Smartcard identifying number
        cardholder
            Some visible string on the card?
        name
            Person's name as the like it displayed
        rooms
            Rooms that the person will be found in
        telephone
            Contact information
        emergency_telephone
            Hidden contact information - emergency only
        invisible
            Whether user should be hidden from displayed lists
        buildings
            List of the names of buildings the person is found in
        """

        # Use any credentials to get the person.
        # Timestamp user as active when updating the profile.
        person = self.get_person(
            crsid=crsid, mifare=mifare, cardholder=cardholder, touch=True
        )
        if person is None and create:
            person = Person()
        elif person is None:
            raise KeyError("Person not found, and creation disabled.")

        try:
            person.update_values(
                crsid=crsid,
                mifare=mifare,
                cardholder=cardholder,
                name=name,
                rooms=rooms,
                telephone=telephone,
                emergency_telephone=emergency_telephone,
                invisible=invisible,
                buildings=self.get_db_buildings(buildings),
            )
            self.session.add(person)
            self.session.commit()
        except IntegrityError:  # unique constraint on crsid broken?
            self.session.rollback()
            person = None

        return person

    def delete_person(
        self,
        crsid: Optional[str] = None,
        mifare: Optional[str] = None,
        cardholder: Optional[str] = None,
    ) -> Optional[Person]:
        """
        Remove a person from the database. Does not delete
        their activity records.

        Parameters
        ==========
        crsid
            Person's CRSid identifier
        mifare
            Smartcard identifying number
        cardholder
            Some visible string on the card?

        Returns
        =======
        deleted
            The person that has been deleted (if any). No longer in the
            database.

        """

        people = self.session.query(Person)
        to_del = set()

        # Find all profiles connected to identifiers
        if crsid:
            to_del.add(people.filter(Person.crsid == crsid).first())

        if mifare:
            to_del.add(people.filter(Person.mifare == mifare).first())

        if cardholder:
            to_del.add(people.filter(Person.cardholder == cardholder).first())

        # Remove empty values from those found
        to_del = to_del.difference([None])

        # sanity checks on the data
        if not to_del:
            return None
        elif len(to_del) > 1:
            raise KeyError("Identifiers do not describe a unique person")

        person = to_del.pop()
        self.session.delete(person)
        self.session.commit()

        return person

    def confirm_crsid(
        self, crsid: Optional[str], mifare: Optional[str]
    ) -> Optional[Person]:
        """
        Assign a new CRSid to an existing record identified by the
        MIFARE. Assume that CRSid has been confirmed and combine any
        records that only have a CRSid.

        Parameters
        ==========
        crsid
            Person's CRSid identifier
        mifare
            Smartcard identifying number

        Returns
        =======
        updated_person
            The combined person with the updated details
        """

        # Nothing to do for "empty" values.
        if not crsid or not mifare:
            return None

        # Look if a record exists with the CRSid, e.g. manually imported
        # and used from the web interface
        existing = self.session.query(Person).filter(Person.crsid == crsid).first()
        # Get the record for the scanned card
        target = self.session.query(Person).filter(Person.mifare == mifare).first()

        # A record with the card does not exist, can't do anything
        if target is None:
            return None

        # It's already the same record, don't need to do anything
        if existing == target:
            return target

        # Data has already been associated with the CRSid, so carry
        # transfer it to the new card where values are not set.
        if existing is not None:
            if not target.name:
                target.name = existing.name
            if not target.rooms:
                target.rooms = existing.rooms
            if not target.telephone:
                target.telephone = existing.telephone
            if not target.emergency_telephone:
                target.emergency_telephone = existing.emergency_telephone
            if not target.buildings:
                target.buildings = existing.buildings[:]
            # delete first as there is a unique constraint on crsid
            self.session.delete(existing)
            self.session.commit()

        # Now set the CRSid to the new value
        target.crsid = crsid
        self.session.commit()

        return target
