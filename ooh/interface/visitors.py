"""
Database of visitors and admins.

Admins have management rights and can add visitors and each other.
Visitors are records of all the details needed to keep track of
outside people within the building.
"""

from typing import Optional, Sequence, Set

from time import time

from sqlalchemy import Column, DateTime, Enum, Float, ForeignKey, Integer
from sqlalchemy import String
from sqlalchemy import create_engine
from sqlalchemy.exc import IntegrityError
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import relationship, RelationshipProperty
from sqlalchemy.orm import sessionmaker, scoped_session, Session
from sqlalchemy.sql import func

from ooh.data.configuration import VISITOR_DB, last_start_of_day, KIOSKS
from ooh.data.configuration import VISITOR_SUPER_ADMIN_MIFARE

Base = declarative_base()


class Card(Base):
    """
    A specific card in the system. Identified by the MIFARE number
    and given a sequential identifier that should not be changed
    and possible a label.
    """

    __tablename__ = "cards"

    # Fixed card information. Primary key needs to be id so that
    # it will auto increment.
    id = Column(Integer, primary_key=True)
    mifare = Column(String, index=True, unique=True)
    # Maybe add some more info
    label = Column(String)
    # Relationships
    admin = relationship("Admin", back_populates="card", uselist=False)
    visitor = relationship("Visitor", back_populates="card", uselist=False)


class Admin(Base):
    """
    A user that can administrate. Will be identified by their
    MIFARE number on their card, but can also be associated
    with a specific CRSid for remote management.
    """

    __tablename__ = "admins"

    # Identifying information
    id = Column(Integer, primary_key=True)
    mifare = Column(String, ForeignKey("cards.mifare"))
    card = relationship(
        "Card", back_populates="admin"
    )  # type: RelationshipProperty[Optional[Card]]
    crsid = Column(String, index=True)
    # Personal information
    name = Column(String)
    telephone = Column(String)
    # Internal database data
    owned = relationship("Visitor", back_populates="owner")

    is_admin = True


class Visitor(Base):
    """
    A visitor is a non-permanent person who is present in the Cavendish.

    Sign-in book is used for non-university card holders so
    they will not have CRSids. Provide them with a visitor card
    so they can be linked to a Card.
    """

    __tablename__ = "visitors"

    # Identifying information
    id = Column(Integer, primary_key=True)
    mifare = Column(String, ForeignKey("cards.mifare"))
    card = relationship(
        "Card", back_populates="visitor"
    )  # type: RelationshipProperty[Optional[Card]]
    # Personal information
    # Fields from the day visitor sign in book
    name = Column(String)
    organisation = Column(String)
    car_registration = Column(String)
    car_location = Column(String)
    host = Column(String)
    location = Column(String)
    purpose = Column(String)
    # Optional information for the modern worker
    telephone = Column(String)
    email = Column(String)
    # Internal database data
    created = Column(DateTime, server_default=func.now())

    owner_id = Column(Integer, ForeignKey("admins.id"))
    owner = relationship("Admin", back_populates="owned")
    scans = relationship(
        "VisitorScan",
        back_populates="visitor",
        order_by="VisitorScan.timestamp.desc()",
        cascade="all, delete-orphan",
        uselist=True,
    )

    is_admin = False

    def signed_in(
        self, reader: str = "yellow", since: Optional[float] = None
    ) -> Optional["VisitorScan"]:
        """If the user is signed in, return their most recent scan."""
        if since is None:
            since = last_start_of_day()

        if not self.scans:
            return None

        latest = self.scans[0]
        if latest.reader == reader and latest.timestamp > since:
            return latest
        else:
            return None

    @hybrid_property
    def last_active(self):
        if self.scans:
            return self.scans[0].timestamp
        else:
            return 0

    @hybrid_property
    def expired(self):
        return not self.card


class VisitorScan(Base):
    """
    A card scan event on the kiosk.

    Events are associated with a visitor.id, not a specific Card
    so relationship persists after the card is disassociated from
    the visitor.
    """

    __tablename__ = "visitor_scans"

    id = Column(Integer, primary_key=True)
    visitor_id = Column(Integer, ForeignKey("visitors.id"))
    visitor = relationship("Visitor", back_populates="scans")
    reader = Column(Enum("yellow", "purple", name="reader_enum"))
    location = Column(String)
    timestamp = Column(Float, nullable=False)

    @property
    def building(self) -> str:
        """Shortcut to the building with a check for if location exists"""
        if self.location in KIOSKS:
            return KIOSKS[self.location].building
        else:
            return "Remote"


class VisitorDatabase:
    """Abstraction querying a database of crsid/name."""

    def __init__(self, db_url: Optional[str] = None):
        if db_url is None:
            db_url = VISITOR_DB

        # stop thread errors
        if db_url.startswith("sqlite://"):
            connect_args = {"check_same_thread": False}
        else:
            connect_args = {}

        # Properties accessible as attributes
        self.url = db_url
        self.base = Base
        # Engine used to connect to the database
        self.engine = create_engine(db_url, connect_args=connect_args)
        # Bind a session instance to the instance -- easier to query itself
        self.session = scoped_session(
            sessionmaker(bind=self.engine, autoflush=True)
        )  # type: Session

    def update_visitor(
        self,
        id: Optional[int] = None,
        mifare: Optional[str] = None,
        name: Optional[str] = None,
        organisation: Optional[str] = None,
        car_registration: Optional[str] = None,
        car_location: Optional[str] = None,
        host: Optional[str] = None,
        location: Optional[str] = None,
        purpose: Optional[str] = None,
        telephone: Optional[str] = None,
        email: Optional[str] = None,
    ) -> Visitor:
        # Find or create the user
        if id is not None:
            # ID uniquely defines a visitor but can't create one
            visitor = self.session.query(Visitor).get(id)
            if visitor is None:
                raise ValueError("Unknown visitor")
            # When selecting a specific user, can update
            # their MIFARE
            if mifare and visitor.mifare != mifare:
                visitor.card = self.get_card(mifare=mifare, create=True)
        elif mifare:
            # Can create a new user from their MIFARE
            visitor = (
                self.session.query(Visitor).filter(Visitor.mifare == mifare).first()
            )
            if visitor is None:
                visitor = Visitor()
                visitor.card = self.get_card(mifare=mifare, create=True)
        else:
            raise ValueError("Invalid identifier")

        if name is not None:
            visitor.name = name
        if organisation is not None:
            visitor.organisation = organisation
        if car_registration is not None:
            visitor.car_registration = car_registration
        if car_location is not None:
            visitor.car_location = car_location
        if host is not None:
            visitor.host = host
        if location is not None:
            visitor.location = location
        if purpose is not None:
            visitor.purpose = purpose
        if telephone is not None:
            visitor.telephone = telephone
        if email is not None:
            visitor.email = email

        self.session.add(visitor)

        # In case there is something that fails
        try:
            self.session.commit()
        except IntegrityError:
            self.session.rollback()
            raise ValueError("Invalid data")

        return visitor

    def delete_visitor(
        self, id: Optional[int] = None, mifare: Optional[str] = None
    ) -> Optional[Visitor]:
        """Delete a visitor from the database. """
        visitor = self.get_visitor(id=id, mifare=mifare)

        if not visitor:
            return None

        # Delete the visitor
        self.session.delete(visitor)
        self.session.commit()

        return visitor

    def archive_visitor(
        self, id: Optional[int] = None, mifare: Optional[str] = None
    ) -> Optional[Visitor]:
        """
        Archive a user by dissociating their profile from the
        assigned MIFARE
        """
        visitor = self.get_visitor(id=id, mifare=mifare)

        if not visitor:
            return None

        # Remove association with card
        visitor.card = None

        self.session.add(visitor)
        self.session.commit()

        return visitor

    def reactivate_visitor(
        self,
        visitor_id: int,
        card_id: Optional[int] = None,
        mifare: Optional[str] = None,
    ) -> Optional[Visitor]:
        """Assign an unused card to an archived visitor."""
        visitor = self.get_visitor(id=visitor_id)
        card = self.get_card(id=card_id, mifare=mifare)

        # Check both exist and are unassigned
        if visitor is None:
            raise KeyError("Visitor not found")

        if visitor.card:
            raise ValueError("Visitor already assigned to a card")

        if card is None:
            raise KeyError("Unknown card")

        if card.visitor:
            raise ValueError("Card is already assigned to a visitor")

        visitor.card = card
        self.session.add(visitor)
        self.session.commit()

        return visitor

    def update_admin(
        self,
        id: Optional[int] = None,
        mifare: Optional[str] = None,
        name: Optional[str] = None,
        crsid: Optional[str] = None,
        telephone: Optional[str] = None,
    ):
        """Complete details for an admin user."""
        admin = self.get_admin(id=id, mifare=mifare)

        if not admin and mifare:
            admin = Admin()
            admin.card = self.get_card(mifare=mifare, create=True)
        elif not admin:
            raise ValueError("Invalid Identifier")

        if name is not None:
            admin.name = name
        if crsid is not None:
            admin.crsid = crsid
        if telephone is not None:
            admin.telephone = telephone

        self.session.add(admin)
        self.session.commit()

        return admin

    def register(
        self,
        id: Optional[int] = None,
        mifare: Optional[str] = None,
        reader: str = "yellow",
        location: str = "127.0.0.1",
        timestamp: Optional[float] = None,
    ) -> VisitorScan:

        # MIFARE will be unique so select the first
        visitor = self.get_visitor(id=id, mifare=mifare)

        if visitor is None:
            raise KeyError("Visitor not found")

        if timestamp is None:
            # default to current time
            timestamp = time()

        new_scan = VisitorScan(
            visitor=visitor, reader=reader, location=location, timestamp=timestamp
        )
        self.session.add(new_scan)
        self.session.commit()

        return new_scan

    def get_card(
        self,
        id: Optional[int] = None,
        mifare: Optional[str] = None,
        create: bool = False,
    ) -> Optional[Card]:
        """Get a specific card, identified by the Card ID or MIFARE."""
        if id is not None:
            return self.session.query(Card).get(id)
        elif mifare:
            # Fetch from the database if card exists
            card = self.session.query(Card).filter(Card.mifare == mifare).first()
            # If the card doesn't exist, we can create one
            if card is None and create:
                card = Card(mifare=mifare)
                self.session.add(card)
                self.session.commit()
            # Possible to return None
            return card
        else:
            return None

    def get_admin(
        self,
        id: Optional[int] = None,
        mifare: Optional[str] = None,
        crsid: Optional[str] = None,
    ) -> Optional[Admin]:
        """Admin referenced by their MIFARE only."""
        if id is not None:
            return self.session.query(Admin).get(id)
        elif mifare:
            # Fetch from the database if they exist
            admin = self.session.query(Admin).filter(Admin.mifare == mifare).first()
            # If the admin doesn't exist but is a super admin, create a real
            # entry in the database for them.
            if admin is None and mifare in VISITOR_SUPER_ADMIN_MIFARE:
                admin = Admin(mifare=mifare, name="Super Administrator")
                admin.card = self.get_card(mifare=mifare, create=True)
                self.session.add(admin)
                self.session.commit()
            # An admin from the db or None
            return admin
        elif crsid:
            return self.session.query(Admin).filter(Admin.crsid == crsid).first()
        else:
            return None

    def delete_admin(
        self, id: Optional[int] = None, mifare: Optional[str] = None
    ) -> Optional[Admin]:
        """Delete an admin from the database."""
        admin = self.get_admin(id=id, mifare=mifare)

        if not admin:
            return None

        self.session.delete(admin)
        self.session.commit()

        return admin

    def get_visitor(
        self, id: Optional[int] = None, mifare: Optional[str] = None
    ) -> Optional[Visitor]:
        """Find a single visitor from their identifiers. Non users return None."""
        if id is not None:
            visitor = self.session.query(Visitor).get(id)
        elif mifare:
            visitor = (
                self.session.query(Visitor).filter(Visitor.mifare == mifare).first()
            )
        else:
            visitor = None

        return visitor

    def visitors(self) -> Sequence[Visitor]:
        """
        Retrieve a list of all non-expired visitors.

        Returns
        -------
        visitors
            Any visitor registered in the system that is not currently
            archived.
        """
        return self.session.query(Visitor).filter(Visitor.mifare.isnot(None)).all()

    def archive(self) -> Sequence[Visitor]:
        """
        Retrieve a list of all archived visitors.

        Returns
        -------
        visitors
            Any visitor that has been archived and removed from their
            card.
        """
        return self.session.query(Visitor).filter(Visitor.mifare.is_(None)).all()

    def signed_in(
        self, reader: str = "yellow", since: Optional[float] = None
    ) -> Sequence[VisitorScan]:
        """
        Retrieve a list of all the visitors currently signed in.

        Parameters
        ----------
        reader
            Name of action to find, "yellow" for sign in, "purple"
            for sign out.
        since
            Unix timestamp for most recent event to consider. Defaults
            to the start of the workday.

        Returns
        -------
        scans
            Scan events that correspond to a most recent event in the
            given time period.

        """
        if since is None:
            since = last_start_of_day()

        visitor_scans = (
            self.session.query(VisitorScan)
            .filter(VisitorScan.timestamp > since)
            .order_by(VisitorScan.timestamp.desc())
        )

        # Unordered list of most recent scan on desired reader
        latest_scans = []

        # Only look at the most recent scan for a visitor and only store it
        # if it is the correct reader. This skips visitors whose last
        # event was an "out" action
        seen = set()  # type: Set[VisitorScan]
        for scan in visitor_scans:
            if scan.visitor not in seen:
                seen.add(scan.visitor)
                if scan.reader == reader and not scan.visitor.expired:
                    latest_scans.append(scan)

        return latest_scans

    def admins(self) -> Sequence[Admin]:
        """
        Retrieve a list of all the admins known to the system.

        Returns
        -------
        admins
            A list of the admins pulled directly from the database.
        """
        return self.session.query(Admin).order_by(Admin.name).all()
