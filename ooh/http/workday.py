"""
Rendered html output of visitor management information

Push information to Jinja2 templates
"""

from __future__ import division

from typing import Callable, Union

from flask import g, session
from flask import request, render_template, redirect, url_for
from werkzeug import Response

from ooh.data import configuration
from ooh.util.web import request_get, refresh_session
from ooh.interface import visitors


WAIT_MESSAGES = {
    "edit_visitor": (
        "Scan visitor card\n"
        "on yellow reader\n"
        "to create a visitor\n"
        "or edit their details."
    ),
    "reactivate_visitor": (
        "Scan visitor card\n" "on yellow reader\n" "to reactivate the visitor."
    ),
    "edit_administrator": (
        "Scan administrator's card\n" "on yellow reader\n" "to edit details."
    ),
    "delete": (
        "Scan card of either\n"
        "visitor or administrator\n"
        "on purple reader\n"
        "to remove details."
    ),
    "archive": (
        "Scan visitor card\n"
        "on purple reader\n"
        "to clear the card\n"
        "and archive the activity."
    ),
}


def workday_route(original: Callable) -> Callable:
    """
    Return the workday, visitor sigh-in view function
    for the given route.

    Parameters
    ==========
    original
        The calling route must be provided for pages that are aware
        of the current mode but have the same page to display.

    Raises
    ======
    KeyError
        The current request has an unknown path. Should not be seen
        as the calling decorator must only be used on known paths.

    """
    # Change the title of the page to reflect that it is
    # in visitor sign-in mode
    g.mode = "visitor-sign-in"
    g.title = configuration.VISITOR_SITE_TITLE
    g.site_name = configuration.VISITOR_SITE_NAME
    g.site_name_suffix = configuration.VISITOR_SITE_NAME_SUFFIX

    # Call the desired function
    if request.path in ["/", "/action/"]:
        return workday
    elif request.path == "/update/":
        return workday_update
    elif request.path in ["/admin/"]:
        return original
    else:
        raise KeyError("Unknown Route {}".format(request.path))


def error(message: str) -> str:
    """Go to the error page with the given message"""
    return render_template("workday.html", mode="error", message=message, timeout=10)


def success(message: str) -> str:
    """Go to the success page with the given message"""
    return render_template("workday.html", mode="success", message=message, timeout=10)


def workday() -> str:
    """
    Daytime Landing page. Decide where to send the response based
    on what is in the session and args.
    """
    # session has ben pre-processed by before_request hook, don't need
    # to worry about expiry
    if not g.kiosk:
        # Remote management mode
        return workday_remote()
    elif "admin" in session:
        # Management mode
        return workday_admin()
    else:
        # General use
        return workday_landing()


def workday_update() -> Union[Response, str]:
    """
    Process an update action when a form is submitted.
    """
    # Ensure that we are authorised for an update action
    if "admin" not in session and not g.remote_admin:
        return error("Not authorised for this action.")

    # Put in the request parameters so it can be used in when remote
    flow = request_get("flow")

    if flow == "edit_visitor":
        # add/update visitor
        try:
            visitor = g.visitor_db.update_visitor(
                id=request_get("visitor_id", int),
                mifare=request_get("mifare"),
                name=request_get("name") or "",
                organisation=request_get("organisation") or "",
                car_registration=request_get("car_registration") or "",
                car_location=request_get("car_location") or "",
                host=request_get("host") or "",
                location=request_get("location") or "",
                purpose=request_get("purpose") or "",
                telephone=request_get("telephone") or "",
                email=request_get("email") or "",
            )
        except ValueError:
            return error("Invalid data.")

        # Clear session flow if using a kiosk
        session.pop("flow", None)

        # Go to the view of the edited details
        return render_template(
            "workday_admin.html", mode="view_visitor", visitor=visitor, timeout=5
        )
    elif flow == "edit_administrator":
        # add/update administrator
        try:
            admin = g.visitor_db.update_admin(
                id=request_get("admin_id", int),
                mifare=request_get("mifare"),
                name=request_get("name") or "",
                crsid=request_get("crsid") or "",
                telephone=request_get("telephone") or "",
            )
        except ValueError:
            return error("Invalid data.")

        # Clear session flow if using a kiosk
        session.pop("flow", None)

        # Go to the view of the edited details
        return render_template(
            "workday_admin.html", mode="view_administrator", admin=admin, timeout=5
        )
    else:
        return redirect(url_for("view.index"))


def workday_admin() -> str:
    # management mode
    session_flow = session.get("flow", None)
    request_flow = request_get("flow")
    visitor_id = request_get("visitor_id", int)
    admin_id = request_get("admin_id", int)
    mifare = request_get("mifare")
    reader = request_get("reader")

    # Admin logout, tapped purple or pressed button
    if reader == "purple" and (
        mifare == session["admin"] or admin_id == session["admin_id"]
    ):
        admin = g.visitor_db.get_admin(id=admin_id, mifare=mifare) or visitors.Admin()
        session.clear()
        return render_template(
            "workday.html", mode="admin_out_action", admin=admin, timeout=5
        )

    # Clear the current flow and return to the start
    # if the request comes through the "back" button
    if request_flow == "back":
        # Reset all stored information so default flow is selected
        session_flow = None
        session.pop("flow", None)
        session.pop("visitor_id", None)

    # Can get to edit screens from either a scan or a button so flow
    # selection can be from either source
    flow = session_flow or request_flow
    flows = ["edit_visitor", "view_visitor", "edit_administrator", "delete", "archive"]

    if flow in flows:
        # populate the cookie to stay in the flow
        session["flow"] = flow
        # If there's a MIFARE then go straight to editing
        if mifare or visitor_id or admin_id:
            if flow in ["edit_visitor", "view_visitor"]:
                visitor = (
                    g.visitor_db.get_visitor(id=visitor_id, mifare=mifare)
                    or visitors.Visitor()
                )
                # Give the card an ID if it's a new visitor
                if not visitor.card:
                    visitor.card = g.visitor_db.get_card(mifare=mifare, create=True)
                return render_template(
                    "workday_admin.html", mode=flow, visitor=visitor, mifare=mifare
                )
            elif flow == "edit_administrator":
                admin = (
                    g.visitor_db.get_admin(id=admin_id, mifare=mifare)
                    or visitors.Admin()
                )
                if not admin.card:
                    admin.card = g.visitor_db.get_card(mifare=mifare, create=True)
                return render_template(
                    "workday_admin.html",
                    mode="edit_administrator",
                    admin=admin,
                    mifare=mifare,
                )
            elif flow == "delete":
                # Completely remove the user from the database
                session.pop("flow", None)
                if g.visitor_db.delete_visitor(id=visitor_id, mifare=mifare):
                    return success("Visitor deleted.")
                elif g.visitor_db.delete_admin(id=admin_id, mifare=mifare):
                    return success("Admin deleted.")
                else:
                    return error("Unknown user!")
            elif flow == "archive":
                # Disassociate the user from the card and archive
                # their sign-in activity
                session.pop("flow", None)
                if g.visitor_db.archive_visitor(id=visitor_id, mifare=mifare):
                    return success("Visitor archived.")
                else:
                    return error("Unknown visitor!")
            else:
                # Invalid flow in the session; clear it and restart
                session.pop("flow", None)
                return workday()
        else:
            # A flow has been selected, but no data has been passed yet
            # Flows require an additional scanned card
            return render_template(
                "workday_admin.html", mode="wait", message=WAIT_MESSAGES[flow]
            )
    elif flow == "reactivate_visitor":
        # Reactivation needs two pieces of information so must be handled
        # as separate flow.
        #
        # visitor_id in args overrides any stored visitor data,
        # then gets stored in a cookie with the flow state.
        visitor_id = visitor_id or session.get("visitor_id")
        session["visitor_id"] = visitor_id
        session["flow"] = flow
        if mifare:
            # Process ends here no matter what, so remove the flow info
            session.pop("visitor_id", None)
            session.pop("flow", None)
            # Try the reactivation using all the supplied information
            try:
                visitor = g.visitor_db.reactivate_visitor(
                    visitor_id=visitor_id, mifare=mifare
                )
            except ValueError:
                return error("Visitor or card already assigned")
            except KeyError:
                return error("Visitor or card do not exist")
            # Success, so show the visitor
            return render_template(
                "workday_admin.html",
                mode="edit_visitor",
                visitor=visitor,
                mifare=visitor.mifare,
            )
        else:
            # Flow selected, but must wait for the card to reactivate onto
            return render_template(
                "workday_admin.html", mode="wait", message=WAIT_MESSAGES[flow]
            )
    elif request_flow == "view_status":
        signed_in = g.visitor_db.signed_in()
        return render_template(
            "workday_admin.html", mode="view_status", signed_in=signed_in, timeout=60
        )
    elif request_flow == "view_visitors":
        visitor_list = g.visitor_db.visitors()
        return render_template(
            "workday_admin.html",
            mode="view_visitors",
            visitors=visitor_list,
            timeout=60,
        )
    elif request_flow == "view_archive":
        archive = g.visitor_db.archive()
        return render_template(
            "workday_admin.html", mode="view_archive", archive=archive, timeout=60
        )
    elif request_flow == "view_admins":
        admins = g.visitor_db.admins()
        return render_template(
            "workday_admin.html", mode="view_admins", admins=admins, timeout=60
        )
    elif reader in ["yellow", "purple"]:
        # Able to sign any visitor in or out from the interface
        person = g.visitor_db.get_visitor(id=visitor_id, mifare=mifare)
        if not person:
            return error("Unknown card")
        else:
            event = g.visitor_db.register(
                id=person.id,
                mifare=person.mifare,
                reader=reader,
                location=request.remote_addr,
            )
            return render_template(
                "workday.html", mode="inout_action", event=event, timeout=10
            )
    else:
        # Admin dashboard
        return render_template(
            "workday_admin.html", mode="admin_flow_select", timeout=60
        )


def workday_landing() -> str:
    """
    Default landing page.

    No admin user has been set so process any URL arguments as
    actions for logging in as a visitor or an admin.
    """
    # Args determine the action if they are set
    visitor_id = request_get("visitor_id", int)
    admin_id = request_get("admin_id", int)
    mifare = request_get("mifare")
    reader = request_get("reader")

    # Nothing to do, just go to the landing page
    if not mifare:
        return render_template("workday.html", timeout=600, timeout_hidden=True)

    # Action depends on the type of person and the card reader
    # they used.
    person = g.visitor_db.get_admin(
        id=admin_id, mifare=mifare
    ) or g.visitor_db.get_visitor(id=visitor_id, mifare=mifare)

    if not person:  # Unknown, don't do anything with them
        return error(
            "Card not recognised!\n"
            "Please visit Reception\n"
            "or contact an administrator."
        )
    elif person.is_admin:  # Admin, known in the system
        if reader == "yellow":  # Signed in, open a session
            session["admin"] = person.mifare
            session["admin_id"] = person.id
            session["admin_name"] = person.name
            refresh_session(force=True)  # Set timeout
            return render_template(
                "workday.html", mode="admin_in_action", admin=person, timeout=0
            )
        elif reader == "purple":
            session.clear()  # clear cookie to log out
            return render_template(
                "workday.html", mode="admin_out_action", admin=person, timeout=5
            )
        else:
            return error("Invalid action!\nScan your card on a reader.")
    else:
        if reader in ["yellow", "purple"]:
            event = g.visitor_db.register(
                id=person.id,
                mifare=person.mifare,
                reader=reader,
                location=request.remote_addr,
            )
            return render_template(
                "workday.html", mode="inout_action", event=event, timeout=5
            )
        else:
            return error("Invalid action!\nScan your card on a reader.")


def workday_remote() -> str:
    """
    Remote management interface, with Raven login.

    Remote access to the visitor system is only available to admins
    that have registered their CRSid. Provides a simple view and
    buttons for management of people.
    """
    # This shouldn't happen as admin is checked before allowing
    # switcher to appear
    if not g.remote_admin:
        return error("You do not have permission to access this site.")

    # Request-only flow based interactions
    flow = request_get("flow")
    visitor_id = request_get("visitor_id", int)
    admin_id = request_get("admin_id", int)
    card_id = request_get("card_id", int)
    mifare = request_get("mifare")
    reader = request_get("reader")

    # Most flows are accessible in remote mode
    flows = ["edit_visitor", "view_visitor", "edit_administrator", "delete", "archive"]
    if flow in flows and (mifare or visitor_id or admin_id):
        if flow in ["edit_visitor", "view_visitor"]:
            visitor = g.visitor_db.get_visitor(id=visitor_id, mifare=mifare)
            return render_template(
                "workday_admin.html", mode=flow, visitor=visitor, mifare=mifare
            )
        elif flow == "edit_administrator":
            admin = g.visitor_db.get_admin(id=admin_id, mifare=mifare)
            return render_template(
                "workday_admin.html",
                mode="edit_administrator",
                admin=admin,
                mifare=mifare,
            )
        elif flow == "delete":
            # Completely remove the user from the database
            if g.visitor_db.delete_visitor(id=visitor_id, mifare=mifare):
                return success("Visitor deleted.")
            elif g.visitor_db.delete_admin(id=admin_id, mifare=mifare):
                return success("Admin deleted.")
            else:
                return error("Unknown user!")
        elif flow == "archive":
            # Disassociate the user from the card and archive
            # their sign-in activity
            if g.visitor_db.archive_visitor(id=visitor_id, mifare=mifare):
                return success("Visitor archived.")
            else:
                return error("Unknown visitor!")
        else:
            return error("Something went wrong")
    elif flow in ["assign_visitor"] and card_id:
        card = g.visitor_db.get_card(id=card_id)
        if not card:
            return error("Invalid Card ID")
        # If there is already a visitor on that card, get their details
        visitor = g.visitor_db.get_visitor(mifare=card.mifare)
        if not visitor:
            # Empty visitor for the template, include the card so it
            # shows on the form
            visitor = visitors.Visitor()
            visitor.card = card
        return render_template(
            "workday_admin.html",
            mode="edit_visitor",
            visitor=visitor,
            mifare=card.mifare,
        )
    elif flow in ["reactivate_visitor"] and visitor_id and card_id:
        # Assign an archived visitor to a card to reactivate them
        try:
            visitor = g.visitor_db.reactivate_visitor(
                visitor_id=visitor_id, card_id=card_id
            )
        except ValueError:
            return error("Visitor or card already assigned")
        except KeyError:
            return error("Visitor or card do not exist")
        # Success, so show the visitor
        return render_template(
            "workday_admin.html",
            mode="edit_visitor",
            visitor=visitor,
            mifare=visitor.mifare,
        )
    elif reader in ["purple", "yellow"]:
        # Sign a visitor in or out from a click
        person = g.visitor_db.get_visitor(id=visitor_id, mifare=mifare)
        if not person:
            return error("Unknown visitor")
        else:
            event = g.visitor_db.register(
                id=person.id,
                mifare=person.mifare,
                reader=reader,
                location=request.remote_addr,
            )
            return render_template(
                "workday.html", mode="inout_action", event=event, timeout=5
            )

    else:
        # Landing page is lists of people
        signed_in = g.visitor_db.signed_in()
        visitor_list = g.visitor_db.visitors()
        archive = g.visitor_db.archive()
        admins = g.visitor_db.admins()

        return render_template(
            "workday_remote.html",
            admin=g.remote_admin,
            signed_in=signed_in,
            visitors=visitor_list,
            archive=archive,
            admins=admins,
        )
