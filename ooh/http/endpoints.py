"""
API access to information. Not used.

"""

from flask import Blueprint
from flask import request, jsonify, abort

from ooh.interface import database, lookup


api_blueprint = Blueprint("api", __name__, url_prefix="/api")

# databases
scan_db = database.OOHDatabase()
people_db = lookup.PeopleDatabase()


def authorised():
    return False


@api_blueprint.route("/scan/", methods=["POST"])
def scan():
    """
    URL encoded POST request format:
        crsid=xyz100&mifare=123456&reader=[yellow|purple]
        &timestamp=[Unix seconds since 1/1/70]&location=[text]
    """

    if not authorised():
        return abort(403)

    crsid = request.args.get("crsid")
    mifare = request.args.get("mifare")
    reader = request.args.get("reader")
    timestamp = request.args.get("timestamp", type=float)
    location = request.args.get("location")

    if not all([crsid, mifare, reader, timestamp, location]):
        abort(400)  # bad request

    scan_event = scan_db.register(
        crsid=crsid,
        mifare=mifare,
        reader=reader,
        timestamp=timestamp,
        location=location,
    )

    if scan_event is None:
        return jsonify({}), 400
    else:
        scan_data = scan_event.to_dict()

    # get person information
    scanned_person = people_db.get_person(crsid=crsid, mifare=mifare)

    if scanned_person is None:
        person_data = {}
    else:
        person_data = scanned_person.to_dict()

    response = {"scan": scan_data, "person": person_data}

    return jsonify(response)


@api_blueprint.route("/signed_in/", methods=["GET"])
def get_signed_in():
    """
    Get a list of people marked as in.
    """

    if not authorised():
        return abort(403)

    scans_in = scan_db.signed_in()

    people_data = []
    for scan in scans_in:
        person = people_db.get_person(crsid=scan.crsid, mifare=scan.mifare)
        if person is None:
            person_data = {}
        else:
            person_data = person.to_dict()
        people_data.append({"scan": scan.to_dict(), "person": person_data})

    return jsonify(people_data)
