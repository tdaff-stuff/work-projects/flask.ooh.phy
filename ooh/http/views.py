"""
Rendering requested pages.

Making use of the view blueprint gives the final page access to
connections to all the databases, information about who or what
is making the request and a few convenience variables including
the site configuration.

Due to the way the kiosks work, most functionality is accessed
through only a couple of endpoints (mainly `/action/`) and the
actions taken and views presented are determined from several
checks:

Any view can be given the option to switch between general sign
in and managed visitor sign in. Decorating the view ensures
that globals are set for the available modes and whichever is
currently selected.

Kiosks (with different levels of contact mode), remote users
and remote admins are all shown different components and
functionality.

"""

from __future__ import division

import textwrap
from datetime import datetime
from functools import wraps
from pathlib import Path
from time import time
from typing import Callable, Any, Union, Dict, List, Optional

from flask import g, Blueprint
from flask import request, render_template, render_template_string, redirect
from flask import url_for, abort, send_file, Markup
from flask.json import dumps
from werkzeug import Response

from ooh.interface import database, lookup, tokens, visitors
from ooh.data import configuration
from ooh.data.configuration import SITE_LOGO, SITE_POLICY, working_hours
from ooh.data.configuration import SHOW_LIST_OOH, SHOW_LIST_WORKDAY, ENABLE_INVISIBILITY
from ooh.data.configuration import KIOSKS, BUILDINGS, RETENTION, on_site_ip
from ooh.data.configuration import NAG_EMERGENCY_PHONE, NAG_MISSING_FIELDS
from ooh.http.extras import extras
from ooh.http.workday import workday_route
from ooh.util.email import nag_emergency_number, nag_incomplete_profile
from ooh.util.email import confirm_crsid
from ooh.util.web import get_current_kiosk, get_raven_user, get_svg_qr_code
from ooh.util.web import get_icon, get_svg_icon
from ooh.util.web import request_get, request_getlist
from ooh.util.web import refresh_session


view_blueprint = Blueprint("view", __name__)


def workday_redirect(route: Callable) -> Callable:
    """
    Add a workday redirect to the route.

    If visitor sign-in is enabled check various conditions to determine
    whether it is during the workday and whether to process the request
    in visitor or general building sign in mode.

    Visitor mode is only available through Raven for people that can
    be identified as visitor admins.
    """

    # Don't do anything to the routes if visitor mode is
    # disabled
    if not configuration.ENABLE_VISITOR_SIGN_IN:
        return route

    @wraps(route)
    def workday_checked_route(*args: Any, **kwargs: Any) -> Callable:
        """
        Decide what mode we are operating in and call the correct
        route.
        """
        # Force workday route when general sign-in is not enabled
        if not configuration.ENABLE_SIGN_IN:
            g.sign_in_available = False
            return workday_route(route)(*args, **kwargs)

        # Default views for a kiosk if not explicitly chosen
        default_view = None

        # Visitor can always be available on kiosks, but only
        # available to Admins through Raven.
        kiosk = g.kiosk
        if kiosk:
            default_view = kiosk.options.get("default", None)
            if kiosk.options.get("exclusive", None) == "visitor-sign-in":
                g.sign_in_available = False
                return workday_route(route)(*args, **kwargs)
            elif kiosk.options.get("exclusive", None) == "sign-in":
                g.visitor_available = False
                return route(*args, **kwargs)
            else:
                g.visitor_available = True
        else:
            g.remote_admin = g.visitor_db.get_admin(crsid=g.user)
            if g.remote_admin:
                g.visitor_available = True
            else:
                return route(*args, **kwargs)

        # Cookies can be set to force a specific view
        # and override the default operation mode
        if request.cookies.get("operation-mode") == "visitor-sign-in":
            return workday_route(route)(*args, **kwargs)
        elif request.cookies.get("operation-mode") == "sign-in":
            return route(*args, **kwargs)
        elif default_view == "visitor-sign-in":
            return workday_route(route)(*args, **kwargs)
        elif default_view == "sign-in":
            return route(*args, **kwargs)
        elif working_hours():
            return workday_route(route)(*args, **kwargs)
        else:
            return route(*args, **kwargs)

    return workday_checked_route


@view_blueprint.before_request
def setup_request_defaults():
    """Set up default parameters for all requests"""
    # Databases in the global context, required for most
    # requests
    g.scan_db = database.OOHDatabase()
    g.people_db = lookup.PeopleDatabase()
    g.token_db = tokens.TokenDatabase()
    g.visitor_db = visitors.VisitorDatabase()
    # Don't advertise visitor sign in or building sign in unless we go
    # through a workday checked route that says it's accessible
    g.sign_in_available = True
    g.visitor_available = False
    # Title at the top of page, can be modified by the
    # request
    g.mode = "sign-in"
    g.title = configuration.SITE_TITLE
    g.site_name = configuration.SITE_NAME
    g.site_name_suffix = configuration.SITE_NAME_SUFFIX
    # Sometimes there will need to be an emergency message,
    # which appears as a red banner at the top.
    g.emergency_message = configuration.EMERGENCY_MESSAGE
    # Current user and their administrative rights
    g.user = get_raven_user()
    g.admin = g.user in configuration.ADMINS
    # If we are remote and raven user is an admin, put them here
    g.remote_admin = None
    # Will usually need to know if we're in a kiosk
    g.kiosk = get_current_kiosk()
    # Contact mode can reduce prompts for interaction, if not
    # set then full contact mode is assumed.
    g.contact = g.kiosk and g.kiosk.options.get("contact")


@view_blueprint.before_request
def check_sessions() -> None:
    """
    Expire any timed-out sessions. Will clear all session
    data unless the expiry timestamp is set and is in the
    future. Refreshes idle timeout.

    Since this is called before the request, each request
    can assume a sanitised session.
    """
    refresh_session()


@view_blueprint.context_processor
def template_globals():
    """
    variables and functions that are needed by most templates
    inject into templates.
    """
    return dict(
        kiosks=KIOSKS,
        buildings=BUILDINGS,
        datetime=datetime,
        get_icon=get_icon,
        get_svg_icon=get_svg_icon,
        get_svg_qr_code=get_svg_qr_code,
        configuration=configuration,
    )


def scans_sort_people(scan_data):
    """
    Sort by display name, crsid or put at end. Required to deal
    with None types appearing for some values.
    """
    return (
        scan_data["person"].get("name", None) or "ZZZZZZZ",
        scan_data["person"].get("crsid", None) or "ZZZZZZZ",
    )


def get_signed_in_by_buildings(hide_invisible: bool = False) -> Dict[str, List[Dict]]:
    """
    Get the list of signed-in people and organise into
    buildings.
    """
    # lists per building
    people_data = {name: [] for name in BUILDINGS}  # type: Dict[str, List[Dict]]
    people_data["No Location"] = []
    people_data["All"] = []

    scan_events = [scan_event.to_dict() for scan_event in g.scan_db.signed_in()]
    people = [
        person.to_dict()
        for person in g.people_db.get_people(
            crsid=[scan_event["crsid"] for scan_event in scan_events],
            mifare=[scan_event["mifare"] for scan_event in scan_events],
        )
    ]
    # Index by both identifiers, as either may be missing
    people_by_crsid = {p["crsid"]: p for p in people if p["crsid"]}
    people_by_mifare = {p["mifare"]: p for p in people if p["mifare"]}

    # All signed in people, defined by last scan event
    for scan_event in scan_events:
        # get the kiosk, if there is one
        if scan_event["location"] in KIOSKS:
            scan_event["location"] = KIOSKS[scan_event["location"]]
        else:
            scan_event["location"] = None

        # get the person associated with the scan,
        # fall through crsid -> mifare -> unknown
        person_data = people_by_crsid.get(
            scan_event["crsid"],
            people_by_mifare.get(
                scan_event["mifare"],
                {
                    "crsid": scan_event["crsid"],
                    "mifare": scan_event["mifare"],
                    "buildings": [],
                    "invisible": False,
                },
            ),
        )

        # Skip hidden users if requested
        if person_data["invisible"] and hide_invisible:
            continue

        # which buildings the person can be found in
        person_buildings = set(person_data["buildings"])

        location = scan_event["location"]
        if not location and not person_buildings:
            # not scanned at a kiosk and empty buildings list
            person_buildings.add("No Location")
        elif location and location.building not in BUILDINGS and not person_buildings:
            # scan from unknown kiosk and no other buildings
            person_buildings.add("No Location")
        elif location:
            # add sign-in location to buildings
            person_buildings.add(location.building)

        # so that buildings contains all buildings in sorted order
        person_data["buildings"] = [
            building for building in BUILDINGS if building in person_buildings
        ]
        if "No Location" in person_buildings:
            person_data["buildings"].append("No Location")

        for building in person_buildings.union(["All"]):
            if building in people_data:
                people_data[building].append(
                    {"scan": scan_event, "person": person_data}
                )

    # Pre-sort in place to display - alphabetical by name or crsid
    for people_list in people_data.values():
        people_list.sort(key=scans_sort_people)

    return people_data


def get_scans(
    tracking: bool = False, crsid: Optional[str] = None
) -> Optional[List[Dict]]:
    """
    Get a list of all scans logged in the system and optionally
    narrow down to a single user by their CRSid.

    Scan events will have their location converted to a kiosk,
    and if a crsid is not given then the name of each user
    will be added to the scan.

    Parameters
    ----------
    tracking
        Whether tracking is enabled, return None if it's disabled.
    crsid
        If given, only show entries for the given CRSid

    Returns
    -------
    scans
        An ordered list of scan events
    """

    # Tracking disabled so we return None, rather than just an empty list
    if not tracking:
        return None

    scan_events = [scan_event.to_dict() for scan_event in g.scan_db.scans(crsid=crsid)]

    # add the location to all scans, gives None if it's not a kiosk
    for scan_event in scan_events:
        scan_event["location"] = KIOSKS.get(scan_event["location"])

    # Can break out here as they are all for the same person so
    # we will not add the name
    if crsid is not None:
        return scan_events

    # If continuing, need to find all names and add to the relevant rows
    people = g.people_db.get_people(
        crsid=[scan_event["crsid"] for scan_event in scan_events],
        mifare=[scan_event["mifare"] for scan_event in scan_events],
    )

    # Index by both identifiers, as either may be missing
    names_by_crsid = {p.crsid: p.name for p in people if p.crsid}
    names_by_mifare = {p.mifare: p.name for p in people if p.mifare}

    for scan_event in scan_events:
        scan_event["name"] = (
            names_by_crsid.get(scan_event["crsid"])
            or names_by_mifare.get(scan_event["mifare"])
            or "No Name"
        )

    return scan_events


@view_blueprint.route("/")
@workday_redirect
def index() -> str:
    """
    Landing page. Welcome text and instructions. List of people
    Currently signed in.
    """

    # Decide what to show if this is a kiosk
    if g.kiosk is not None:
        workday = working_hours()
        if (SHOW_LIST_WORKDAY and workday) or (SHOW_LIST_OOH and not workday):
            # kiosk mode, full output
            people_data = get_signed_in_by_buildings(hide_invisible=ENABLE_INVISIBILITY)
            building_tab = g.kiosk.building
            return render_template(
                "index.html", people_data=people_data, building_tab=building_tab
            )
        else:
            people_count = len(g.scan_db.signed_in())
            return render_template("index.html", people_count=people_count)

    crsid = g.user

    if crsid is None:
        return abort(403)

    # is user signed in?
    user_in = g.scan_db.user_signed_in(crsid=crsid)

    # Get all user's activity, if enabled
    scans = get_scans(tracking=configuration.ENABLE_TRACKING, crsid=crsid)

    # List is shown either on an authorised network (e.g. internal)
    # or is always shown to an Admin whenever they view the page.
    if on_site_ip(request.remote_addr) or g.admin:
        # Raven user on site, shows all people
        people_data = get_signed_in_by_buildings(hide_invisible=ENABLE_INVISIBILITY)
        if "building-tab" in request.cookies and (
            request.cookies["building-tab"] in BUILDINGS
            or request.cookies["building-tab"] == "All"
        ):
            building_tab = request.cookies["building-tab"]
        else:
            building_tab = BUILDINGS[0]
        return render_template(
            "index.html",
            people_data=people_data,
            building_tab=building_tab,
            user_in=user_in,
            scans=scans,
        )
    else:
        # cut down version, only my info and sign in/out
        return render_template("index.html", user_in=user_in, scans=scans)


@view_blueprint.route("/help/")
def instructions():
    """
    Full about page for user documentation.
    """
    return render_template("help.html")


@view_blueprint.route("/policy/")
def policy():
    """
    Static display of working policy. Either templated from the default
    templates or pulled from the path configured by SITE_POLICY.
    """
    if SITE_POLICY is not None:
        try:
            return render_template_string(Path(SITE_POLICY).read_text())
        except IOError:
            return abort(404)

    return render_template("policy.html")


@view_blueprint.route("/privacy/")
def privacy():
    """
    Static display of privacy notice. Should be generic enough to cover
    all installations.
    """
    return render_template("privacy.html")


@view_blueprint.route("/print/")
def printout():
    """
    Plain text list of people.
    """
    kiosk = g.kiosk
    # Block external access; add printers to kiosk list!
    if not kiosk:
        return abort(403)

    # From OOH
    print_order = []
    signed_in_data = {}
    # From visitor sign-in
    visitor_data = []

    if configuration.ENABLE_SIGN_IN:
        # Put current building first
        print_order = list(BUILDINGS) + ["No Location"]
        if kiosk.building in print_order:
            print_order.remove(kiosk.building)
            print_order.insert(0, kiosk.building)

        signed_in_data = get_signed_in_by_buildings()

        if len(signed_in_data.get("No Location", [])) == 0:
            print_order.remove("No Location")

        # Everyone in the same building, just used combined view
        if len(BUILDINGS) < 2:
            print_order = ["All"]

    if configuration.ENABLE_VISITOR_SIGN_IN:
        # sort the list by building and last name, but don't partition
        visitor_data = sorted(
            g.visitor_db.signed_in(),
            key=lambda x: (x.building, (x.visitor.name or "ZZZ").split()[-1].lower()),
        )

    def wrap_item(prefix, text):
        """Wrap text for thermal printer"""
        return "\n".join(
            textwrap.wrap(
                text,
                width=32,
                initial_indent=prefix,
                subsequent_indent=" " * len(prefix),
            )
        )

    # Send as plain text response with some control
    # codes to format on printer
    return (
        render_template(
            "print.txt",
            signed_in_data=signed_in_data,
            print_order=print_order,
            visitor_data=visitor_data,
            wrap_item=wrap_item,
            chr=chr,
        ),
        200,
        {"Content-Type": "text/plain; charset=UTF-8"},
    )


@view_blueprint.route("/action/", methods=["POST", "GET"])
@workday_redirect
def scan():
    """
    Register a scan and show the signed in page.

    Card readers send to a url that has fields properly encoded. Requests
    coming from the web interface only have CRSid field for now.
    """

    action_class = ""  # no colours
    data_mode = None
    qr_code = None
    nag_check = False  # Signal page if there is missing data

    # Sources of scans
    kiosk = g.kiosk  # None if not a kiosk
    user = g.user  # None if not Raven authenticated

    # Look for a valid person
    # CRSid, mifare and cardholder identify a person exactly
    # and can be used to create a new person
    crsid = request_get("crsid")
    # sanitise scans from Raven users;
    # only accept the mifare from a scanning kiosk
    if kiosk:
        mifare = request_get("mifare")
        cardholder = request_get("cardholder")
    elif g.admin:
        mifare = request_get("mifare")
        cardholder = request_get("cardholder")
    elif user == crsid:
        mifare = None
        cardholder = None
    else:
        # Not a kiosk, and CRSid doesn't match the logged in raven user!
        # Throw an error; something fishy going on!
        return render_template(
            "action.html", message="Error: Not allowed!", action_class="action-error",
        )

    # Scan failed or no information provided
    if not any([crsid, mifare, cardholder]):
        # failed scan
        return render_template(
            "action.html",
            message="Error registering information; Please try again!",
            action_class="action-error",
        )

    # Only show the edit screen if:
    # - card has been scanned
    # - manual login with raven authentication
    if mifare and kiosk:  # comes from scanning a card
        data_mode = "edit"
    elif g.admin:  # Admins can edit any user's data
        data_mode = "edit"
    elif crsid and user == crsid:  # Raven authenticated
        data_mode = "edit"

    # Retrieve person's information
    # Will also fill in missing mifare for a person
    # (missing information will only be available from a kiosk!)
    # Last active timestamp is updated for any action since the user
    # is actively interacting with the system.
    person = g.people_db.get_person(
        crsid=crsid,
        mifare=mifare,
        cardholder=cardholder,
        update_identifiers=True,
        touch=True,
    )

    # Are we able to create a person; minimum required info is a
    # mifare number for a visitor with no CRSid.
    # Card reader will always provide the CRSid if it is present so
    # can assume that there is no CRSid if it is blank.
    # Admins can create a person using any details with a flag to
    # force creation required
    if person is None and g.admin:
        if request_get("action") == "create":
            # Admins can create a user using only their CRSid or MIFARE
            person = g.people_db.create_person(
                crsid=crsid, mifare=mifare, cardholder=cardholder
            )
        else:
            # If creation is not explicitly requested then find has failed
            # and we can't do anything else here.
            message = (
                "Could not find person with identifiers:\n"
                + "CRSid: {}\n".format(crsid)
                + "MIFARE: {}\n".format(mifare)
            )
            return render_template(
                "action.html", message=message, action_class="action-error"
            )
    elif person is None and mifare:
        # Card scanned, can create new person using their mifare
        person = g.people_db.create_person(
            crsid=crsid, mifare=mifare, cardholder=cardholder
        )
    elif person is None and user:
        # Raven user but not known in the system, not allowed to create
        # profile from Raven alone.
        return render_template(
            "action.html",
            message="Error; Tap card at a terminal to register a new person.",
            action_class="action-error",
        )
    elif person is None:
        return render_template(
            "action.html",
            message="Error registering new user; Please try scanning again!",
            action_class="action-error",
        )

    # Event type:
    # - yellow = sign in
    # - purple = sign out;
    # - edit = edit (no action just show edit screen)
    reader = request_get("reader")
    if reader in ["purple", "yellow"]:
        # Either recorded time, or current time
        timestamp = request_get("timestamp", type=float) or time()

        # ip address encodes kiosk
        location = request.remote_addr

        scan_event = g.scan_db.register(
            crsid=person.crsid,
            mifare=person.mifare,
            reader=reader,
            timestamp=timestamp,
            location=location,
        )

        if scan_event is None:
            message = "Error signing in/out;\n try again!"
            return render_template(
                "action.html", message=message, action_class="action-error",
            )
        else:
            # ensure CRSid and MIFARE are consistent for all actions
            g.scan_db.backfill_users(crsid=person.crsid, mifare=person.mifare)
            # Enforce retention policy on database
            g.scan_db.clear_records(retention=RETENTION)
            # Register usage stats after database has been cleaned
            g.scan_db.register_stats()

        if reader == "yellow":
            message = "Signed in as {}.\nHave a nice day!".format(
                person.name or "New Person"
            )
            action_class = "action-sign-in"

            # If a no-contact mode is enabled on the kiosk then
            # hide the edit form again.
            if kiosk and mifare and g.contact == "low":
                data_mode = "ask"
            if kiosk and mifare and g.contact == "off":
                data_mode = "ask"

            # Check if any fields that would have email reminders are not
            # set and add a class to the page
            nag_check = any(
                [not getattr(person, field) for field in NAG_MISSING_FIELDS]
                + [NAG_EMERGENCY_PHONE and not person.emergency_telephone]
            )

            # Check if the card doesn't have a CRSid and offer
            # a qr code so that they can associate one with their
            # card by logging in with Raven
            if kiosk and not person.crsid:
                # Stored action will add any Raven login with the stored mifare
                action = "add_raven_to_mifare:{mifare}".format(mifare=mifare)
                # Expire after a few minutes as it's only applicable when
                # the user is in front of the kiosk.
                key = g.token_db.generate_token(action, valid_hours=0.17, max_uses=1)
                # Make url into an embeddable SVG file that's always
                # sent to the template.
                confirm_url = url_for("view.confirm", key=key, _external=True)
                qr_code = Markup(get_svg_qr_code(confirm_url))
        else:
            message = "Signed out {}.".format(person.name or "New Person")
            action_class = "action-sign-out"
            data_mode = None
            # Check if current building is empty so person knows they are
            # the last out.
            if kiosk:
                signed_in = get_signed_in_by_buildings()
                if len(BUILDINGS) > 1 and not signed_in.get(kiosk.building):
                    message += "\n\nBuilding is empty!"
                elif len(BUILDINGS) <= 1 and not signed_in.get("All"):
                    message += "\n\nSite is empty!"

    elif reader == "edit":
        message = "Edit personal data"
    else:
        message = "Unknown action"
        data_mode = None

    if data_mode == "edit":
        # Timeout resets while editing, so this
        # should be plenty.
        timeout = 30
    elif qr_code is not None:
        # Need enough time to get out phone and
        # open the camera. Shown every time so
        # can just sign in again to get a new one.
        timeout = 20
    else:
        # Just a success message
        timeout = 5

    return render_template(
        "action.html",
        message=message,
        action_class=action_class,
        person_data=person.to_dict(),
        data_mode=data_mode,
        qr_code=qr_code,
        nag_check=nag_check,
        timeout=timeout,
    )


@view_blueprint.route("/update/", methods=["POST", "GET"])
@workday_redirect
def update() -> Union[str, Response]:
    """
    Update a person's information. Pulls data from either form data
    or from a url encoded request.
    """

    # Sources of update information
    kiosk = g.kiosk  # None if not a kiosk
    user = g.user  # None if not Raven authenticated

    # New user creation is disabled for updates
    create = False

    # Look for a valid person
    # CRSid, mifare and cardholder identify a person exactly
    # and can be used to create a new person
    crsid = request_get("crsid")
    # sanitise scans from Raven users;
    # only accept the mifare from a scanning kiosk
    # to prevent changing the card to someone else's;
    # admins have the ability to alter mifare and crsids.
    if kiosk:
        mifare = request_get("mifare")
        cardholder = request_get("cardholder")
    elif g.admin:
        mifare = request_get("mifare")
        cardholder = request_get("cardholder")
        # Admins can create users through the web interface
        create = True
    elif user == crsid:
        mifare = None
        cardholder = None
    else:
        # Not authorised so show error and directions
        return render_template(
            "action.html",
            message=(
                "Not authorised to edit details!\n"
                "Use a card reader or login with Raven."
            ),
            action_class="action-error",
        )

    if not any([crsid, mifare, cardholder]):
        # Nothing to identify the user
        return render_template(
            "action.html",
            message="Error registering information!\nPlease try again!",
            action_class="action-error",
        )

    # Pull any data given in the form
    # Since request_get returns None for blanked entries,
    # explicitly set them as empty or defaults so that they
    # get updated properly.
    name = request_get("name") or ""
    rooms = request_get("rooms") or ""
    telephone = request_get("telephone") or ""
    emergency_telephone = request_get("emergency_telephone") or ""
    invisible = request_get("invisible", bool) or False
    buildings = request_getlist("buildings")
    # possibly updating CRSid
    if kiosk and not crsid:
        new_crsid = request_get("new_crsid")
        # Sanitise if not empty
        if new_crsid is not None:
            if new_crsid.isalnum():
                new_crsid = new_crsid.lower()
            else:
                new_crsid = None  # dodgy value
    else:
        new_crsid = None

    # sanitise buildings
    buildings = list({building for building in buildings if building in BUILDINGS})

    if g.admin and crsid and mifare:
        # Admins can change CRSid and MIFARE values of anyone, this needs
        # to be processed before updating any other fields so that we don't
        # run into integrity errors for duplicated records.
        # The confirm method will check if the crsid and mifare refer to
        # different records and combine them if required
        if g.people_db.confirm_crsid(crsid=crsid, mifare=mifare):
            g.scan_db.backfill_users(crsid=crsid, mifare=mifare)

    # Update the person's data (admins can also create new users)
    person_data = g.people_db.update_person(
        create=create,
        crsid=crsid,
        mifare=mifare,
        cardholder=cardholder,
        name=name,
        rooms=rooms,
        telephone=telephone,
        emergency_telephone=emergency_telephone,
        invisible=invisible,
        buildings=buildings,
    )

    if person_data is None:
        # Some kind of error, just fail. Person not updated.
        return render_template(
            "action.html",
            message="Unknown error updating information!\nPlease try again!",
            action_class="action-error",
        )
    elif g.admin:
        # Admin has updated the person, show their new details
        # and finish after a short timeout.
        return render_template(
            "action.html",
            message="Details successfully updated.",
            action_class="action-success",
            person_data=person_data.to_dict(),
            data_mode="ask",
            timeout=10,
        )
    elif person_data.crsid:
        # Person has gone through update details page so check their
        # personal information and nag if requested
        edit_url = url_for(
            "view.scan", crsid=person_data.crsid, reader="edit", _external=True
        )
        # Emergency number is None or empty string
        no_emergency = not (person_data.emergency_telephone or "").strip()
        # any required fields that are empty
        missing_fields = [
            field_name
            for field_name in NAG_MISSING_FIELDS
            if not (getattr(person_data, field_name) or "").strip()
        ]
        if NAG_EMERGENCY_PHONE and no_emergency:
            # Highest priority
            nag_emergency_number(
                crsid=person_data.crsid, url=edit_url, name=person_data.name
            )
        elif missing_fields:
            # Fill in the gaps, milder email
            nag_incomplete_profile(
                crsid=person_data.crsid,
                missing_fields=missing_fields,
                url=edit_url,
                name=person_data.name,
            )
    elif new_crsid:
        # Requested to link with a new CRSid
        # Store the values to be linked with a key to access them
        action = "confirm_crsid:{mifare}:{crsid}".format(mifare=mifare, crsid=new_crsid)
        # Expire key after a time limit, or when it is used
        key = g.token_db.generate_token(action, valid_hours=25, max_uses=1)
        success = confirm_crsid(
            crsid=new_crsid,
            url=url_for("view.confirm", key=key, _external=True),
            name=person_data.name,
        )
        if success:
            # sent successfully
            return render_template(
                "action.html",
                message=(
                    "Details updated.\n"
                    "An email has been sent to {crsid}@cam.ac.uk\n"
                    "to confirm your CRSid, {crsid}.".format(crsid=new_crsid)
                ),
                action_class="action-success",
            )
        else:
            # problem sending mail, delete key and give an error
            g.token_db.expire_token(key)
            return render_template(
                "action.html",
                message=(
                    "Error sending email to {crsid}@cam.ac.uk\n"
                    "please check the CRSid, {crsid}.".format(crsid=new_crsid)
                ),
                action_class="action-error",
            )

    # nothing to do, go back to the homepage
    return redirect(url_for("view.index"), 302)


@view_blueprint.route("/delete_profile/", methods=["POST", "GET"])
def delete_profile() -> Union[str, Response]:
    """
    Delete a user's profile completely from the system. Ensure that
    all their details and any activity are deleted.

    Note that using this method could remove data that would be
    required for tracking so make sure there is a confirmation
    dialog before coming here.
    """
    # Only an admin can wipe profiles
    if not g.admin:
        return abort(403)

    crsid = request_get("crsid")
    mifare = request_get("mifare")
    cardholder = request_get("cardholder")
    confirm_delete = request_get("confirm_delete", bool)

    # Ensures that the user has clicked through the modal
    if not confirm_delete:
        return abort(400)

    person = g.people_db.get_person(crsid=crsid, mifare=mifare, cardholder=cardholder)

    if not person:
        return render_template(
            "action.html",
            message="Person to delete does not exist",
            action_class="action-error",
        )

    # Records cleared in lookup and activity databases
    g.scan_db.purge_user_records(crsid=person.crsid, mifare=person.mifare)
    g.people_db.delete_person(
        crsid=person.crsid, mifare=person.mifare, cardholder=person.cardholder
    )

    return render_template(
        "action.html",
        message="Deleted all records for {person.name} ({person.crsid})".format(
            person=person
        ),
        action_class="action-success",
    )


@view_blueprint.route("/confirm/", methods=["POST", "GET"])
def confirm():
    """
    Confirm an action sent with a key via email or scanned from
    a QR code.
    """
    key = request_get("key")

    # Extract the information from the key database
    try:
        data = g.token_db.get_value(key)
    except KeyError:
        return render_template(
            "action.html",
            message="Invalid or expired confirmation key!",
            action_class="action-error",
        )

    # Stored data should have values separated by ':'
    action, payload = data.split(":", 1)

    if action == "confirm_crsid":
        # Confirmation action has encoded both the mifare and the
        # CRSid. Will be sent by email to confirm that the recipient
        # is the owner of the CRSid.
        mifare, crsid = payload.split(":")
        # Update ensures existing records are merged
        updated_person = g.people_db.confirm_crsid(crsid=crsid, mifare=mifare)
        if updated_person is not None:
            # Make sure any scans in the database are consistent
            g.scan_db.backfill_users(updated_person.crsid, updated_person.mifare)
            # Success
            return render_template(
                "action.html",
                message=(
                    "Your CRSid has been confirmed as {},\n"
                    "and linked with the card scanned at\n"
                    "the kiosk!".format(updated_person.crsid)
                ),
                action_class="action-success",
            )
    elif action == "add_raven_to_mifare" and g.user:
        # Mifare is known from scanning a card, use the logged in Raven
        # user's identity to add the CRSid to the card. The stored payload
        # will just be the MIFARE number of the card.
        mifare = payload
        crsid = g.user
        # Update ensures existing records are merged
        updated_person = g.people_db.confirm_crsid(crsid=crsid, mifare=mifare)
        if updated_person is not None:
            # Make sure any scans in the database are consistent
            g.scan_db.backfill_users(updated_person.crsid, updated_person.mifare)
            # Success
            return render_template(
                "action.html",
                message=(
                    "Your CRSid, {}, has been associated with the\n"
                    "card scanned at the kiosk!".format(updated_person.crsid)
                ),
                action_class="action-success",
            )

    # Fallback if no other return encountered
    # Don't put behind else!
    return render_template(
        "action.html",
        message="Unable to complete action from confirmation key!",
        action_class="action-error",
    )


@view_blueprint.route("/stats/")
def stats() -> Union[str, Response]:
    """
    Anonymous Usage stats for the service.
    """
    # Users must be registered in the system
    if not g.admin:
        return abort(403)

    # Usage returns a dict of ordered entries
    usage = g.scan_db.usage()

    # Inject as json into the page
    return render_template("stats.html", usage=dumps(usage))


@view_blueprint.route("/activity/")
def activity() -> Union[str, Response]:
    """
    Full activity log of all sign in and out events in the system.
    """
    if not configuration.ENABLE_TRACKING:
        return abort(404)

    # Admins only, users can see theirs through Raven
    if not g.admin:
        return abort(403)

    scans = get_scans(tracking=True)

    # Inject as json into the page
    return render_template("activity.html", scans=scans)


@view_blueprint.route("/admin/")
@workday_redirect
def admin() -> Union[str, Response]:
    """
    General admin things.

    Currently just shows all configured options.
    """
    # Users must be registered in the system
    if not g.admin:
        return abort(403)

    # Display most of the customisable attributes. Skip some of the more
    # difficult ones.
    attrs = [
        "ENABLE_SIGN_IN",
        "ENABLE_VISITOR_SIGN_IN",
        # "ENTRIES_DB",
        # "LOOKUP_DB",
        # "TOKEN_DB",
        # "VISITOR_DB",
        # "SECRET_KEY",
        "SITE_NAME",
        "SITE_NAME_SUFFIX",
        "SITE_TITLE",
        "EMERGENCY_MESSAGE",
        # "SITE_LOGO",
        # "SITE_POLICY",
        "KIOSKS",
        "BUILDINGS",
        "DEBUG_MAIL",
        "CONTACT_EMAIL",
        "CONTACT_NAME",
        "BOUNCE_EMAIL",
        "NAG_EMERGENCY_PHONE",
        "HIGHLIGHT_FIELDS",
        "NAG_MISSING_FIELDS",
        "START_OF_DAY",
        "END_OF_DAY",
        "NON_WORKDAYS",
        "WEEKEND",
        "NETWORKS",
        "SHOW_LIST_WORKDAY",
        "SHOW_LIST_OOH",
        "ENABLE_INVISIBILITY",
        "ENABLE_TRACKING",
        "RETENTION",
        "ADMINS",
        "BUS_ROUTES",
        "VISITOR_SITE_NAME",
        "VISITOR_SITE_NAME_SUFFIX",
        "VISITOR_SITE_TITLE",
        "VISITOR_SUPER_ADMIN_MIFARE",
        "SESSION_IDLE_TIMEOUT",
        # "EMAIL_TEMPLATES",
    ]

    # Render the interface
    return render_template("admin.html", attrs=attrs)


if SITE_LOGO is not None:

    @view_blueprint.route("/static/<string:_version>/images/logo.png")
    def custom_logo(_version: str):
        return send_file(SITE_LOGO)


for url, func in extras.items():
    view_blueprint.add_url_rule(url, endpoint=func.__name__, view_func=func)


@view_blueprint.after_request
def shutdown_request_databases(response: Response) -> Response:
    g.people_db.session.remove()
    g.scan_db.session.remove()
    g.token_db.session.remove()
    g.visitor_db.session.remove()
    return response
