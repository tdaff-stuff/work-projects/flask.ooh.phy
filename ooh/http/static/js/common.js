//
// General functions that can be used by any page.
//

// When manually switched to ooh or visitor mode, reset to the
// default after the set period of inactivity.
// This should be longer than idle timeout so that it doesn't
// refresh an admin session.
const idleTimeout = 360;

// Modify the attributes in an embedded SVG file
// constants are taken from `generate_favicons.py`
// Snapped to the current minute as shown on the clock
let setHands = function (hour_hand, minute_hand) {
  const minute_size = 30;
  const hour_size = 15;
  const cx = 50;
  const cy = 45;
  let date = new Date();
  let hours = (Math.PI / 6) * (date.getHours() + date.getMinutes() / 60.0);
  let minutes = (Math.PI / 30) * date.getMinutes();

  $(hour_hand)
    .attr("x2", cx + hour_size * Math.sin(hours))
    .attr("y2", cy - hour_size * Math.cos(hours));
  $(minute_hand)
    .attr("x2", cx + minute_size * Math.sin(minutes))
    .attr("y2", cy - minute_size * Math.cos(minutes));
};

// Set the cookie for the operation mode with the idle timeout
const setOperationModeCookie = function (mode, timeout) {
  document.cookie = "operation-mode=" + mode + ";max-age=" + timeout + ";path=/;";
  document.cookie = "operation-mode-expiry=" + (Date.now() + timeout * 1000) + ";path=/;";
  return timeout * 1000;
};

let watchOperationModeCookie = function () {
  // If the operation mode cookie is set, reset the duration and
  // redirect to the index page once it's timed-out.
  let expiry, operationMode, timeout;
  let getCookieKeyValue = function (cookie) {
    return {
      key: cookie.split("=")[0].trim(),
      value: cookie.split("=")[1].trim(),
    };
  };
  // Extract the mode and expiry from the cookies
  document.cookie.split(";").forEach(function (cookieStr) {
    // Splitting an empty string gives [ "" ] ????
    // So don't process anything that doesn't look like a cookie
    if (cookieStr.indexOf("=") === -1) {
      return;
    }
    let cookie = getCookieKeyValue(cookieStr);
    if (cookie.key === "operation-mode") {
      operationMode = cookie.value;
    } else if (cookie.key === "operation-mode-expiry") {
      expiry = cookie.value;
    }
  });
  // Actions if cookie exists
  if (operationMode !== undefined) {
    // Forced refresh in kiosk has no referrer, OOH timeout has no change
    if (document.referrer && document.referrer !== window.location.href) {
      // Reset cookie on navigation
      timeout = setOperationModeCookie(operationMode, idleTimeout);
    } else if (expiry !== undefined) {
      // Watch existing cookie
      timeout = expiry - Date.now();
    } else {
      // Cookie missing so just set default timeout
      timeout = idleTimeout * 1000;
    }
    setTimeout(function () {
      window.location.href = "/";
    }, timeout * 1000);
  }
};

// Once page is loaded
$(function () {
  //
  // Tick clock logos every minute
  //
  $(".active-clock").each(function (_idx, element) {
    // Capture hands in the function
    let hour_hand = $(".hour-hand", element);
    let minute_hand = $(".minute-hand", element);
    // set hands to initial time on page load
    setHands(hour_hand, minute_hand);
    // Self repeating timer
    let updateHands = function () {
      setHands(hour_hand, minute_hand);
      let now = new Date();
      // Seconds remaining until next minute
      let remaining = 60000 - (1000 * now.getSeconds() + now.getMilliseconds());
      setTimeout(updateHands, remaining);
    };
    updateHands();
  });
  //
  // Update text clocks
  //
  $(".active-time").each(function () {
    let timeSpan = $(this);
    let updateTime = function () {
      let now = new Date();
      timeSpan.text(
        ("0" + now.getHours()).slice(-2) +
          ":" +
          ("0" + now.getMinutes()).slice(-2)
      );
      // Seconds remaining until next minute
      let remaining = 60000 - (1000 * now.getSeconds() + now.getMilliseconds());
      setTimeout(updateTime, remaining);
    };
    updateTime();
  });
  //
  // Session expiry tracker
  //
  $("#btn-expiry").each(function (_idx, element) {
    let counter = $("#counter", element);
    let end = $(element).data("expiry") * 1000; // milliseconds
    // tick down to the expiry timestamp
    let setRemaining = function () {
      let remaining = end - Date.now();
      if (remaining < 0) {
        // go home and remove any args that will cause a re-login
        window.location = window.location.pathname;
      } else {
        counter.text(
          Math.floor(remaining / 60000) +
            ":" +
            ("0" + (Math.floor(remaining / 1000) % 60)).slice(-2)
        );
      }
      // Trigger after what remains of the current second
      setTimeout(setRemaining, Math.max(remaining % 1000, 100));
    };
    // Trigger countdown
    setRemaining();
  });
  //
  // Mode switching
  //
  // Set cookies and refresh page to change
  // between visitor and ooh modes
  $("a.switch-mode").on("click", function () {
    let new_mode = window.escape($(this).data("target-mode"));
    setOperationModeCookie(new_mode, idleTimeout);
    window.location.href = "/";
  });
  // Maintain forced operation mode and refresh when it's expired
  watchOperationModeCookie();

  //
  // Timeout for buttons or links to auto click
  //
  $(".timeout").each(function () {
    // click button after period of inactivity
    // Use the element's existing text and timeout length
    // must be provided in the data
    let btn = $(this);
    let defaultTimeout = btn.data("timeout");
    let buttonText = btn.text();
    let timer = defaultTimeout;

    $(window).on(
      "load mousemove mousedown touchstart click scroll keypress",
      function () {
        timer = defaultTimeout + 1;
      }
    );
    // update button every second
    btn.timer = setInterval(function () {
      timer--;
      if (timer < 1) {
        btn[0].click();
      } else {
        btn.text(buttonText + " (" + timer + ")");
      }
    }, 1000); // per-second-ish
  });

  //
  // Manual sign-in and sign-out fields
  //
  // Auto-focus when the fields are opened on kiosks
  $("#manual-sign-in").on("shown.bs.collapse", function () {
    $("#crsid-in").focus();
  });
  $("#manual-sign-out").on("shown.bs.collapse", function () {
    $("#crsid-out").focus();
  });
  // Change indicator when field is hidden/shown via Raven
  $("#manual-sign-in-out")
    .on("shown.bs.collapse", function () {
      $("#crsid-in-out").focus();
      $("#menu-drop-icon").css({ transform: "rotate(0deg)" });
    })
    .on("hidden.bs.collapse", function () {
      $("#menu-drop-icon").css({ transform: "rotate(-90deg)" });
    });

  //
  // Remember current building tab
  //
  $(".building-tab").on("click", function () {
    let building = $(this).data("building");
    let date = new Date();
    date.setTime(date.getTime() + 30 * 24 * 60 * 60 * 1000); // 30 days
    let expires = "; expires=" + date.toLocaleDateString();
    document.cookie = "building-tab=" + building + expires + ";path=/;";
  });

  //
  // Action and edit details page
  //
  // Visual validation feedback while typing
  $("#edit-details input[type=text]:not([readonly])")
    .on("input once", function () {
      let elem = $(this);
      if (!elem.data("highlight")) {
        return;
      }
      if (elem.val().length === 0) {
        elem.removeClass("is-valid").addClass("is-invalid");
      } else if (elem.hasClass("is-invalid")) {
        $(this).removeClass("is-invalid").addClass("is-valid");
      }
    })
    .trigger("once"); // trigger for invalid starting values
  // Visual feedback for invalid selection when no buildings selected
  $("#edit-details .building-checkbox")
    .on("change once", function () {
      $("#edit-details .building-label").toggleClass(
        "is-invalid",
        $(".building-checkbox:checked").length === 0
      );
    })
    .trigger("once");
  $("#edit-details")
    .on("reset", function () {
      // When triggering a form reset, need to manually reset the checkbox labels
      setTimeout(function () {
        $("#edit-details input[type=checkbox]:checked")
          .parent()
          .addClass("active");
        $("#edit-details input[type=checkbox]:not(:checked)")
          .parent()
          .removeClass("active");
      }, 10);
    });
  // Keep CRSid lowercase when typing
  $("#input_new_crsid").on("input", function () {
    this.value = this.value.toLowerCase();
  });

  //
  // Interactions with admin activity viewer
  //
  // Click on a user to only show their activity
  $(".scan-user").on("click", function () {
    let crsid = $(this).data("crsid");
    $("#narrow").val(crsid).trigger("input");
  });
  // input box updates list after any change
  $("#narrow").on("input change", function () {
    let crsid = $(this).val();
    $(".scan-row").each(function () {
      let row = $(this);
      row.toggleClass("d-none", row.data("crsid").indexOf(crsid) < 0);
    });
  });
});
