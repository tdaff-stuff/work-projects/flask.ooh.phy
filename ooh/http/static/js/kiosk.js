// TODO: does window.its_notify work?
var its_notify = [];
// When the keyboard pops up, scroll any focused input element into view
let page = $(window);
let id;
page.on("resize", function () {
  if (id) {
    return;
  }
  let armed = true;
  let n = 10;
  id = setInterval(function () {
    if (armed) {
      let offset = $("input:focus").offset();
      if (offset) {
        let input_y = offset.top;
        let screen_y = page.scrollTop();
        let screen_h = page.height();
        if (input_y - 50 < screen_y || input_y + 120 > screen_y + screen_h) {
          page.scrollTop(input_y - 50);
          armed = false;
        }
      }
    }
    if (--n < 0) {
      clearInterval(id);
      id = undefined;
    }
  }, 50);
});

let notify_input = function (data) {
  $.ajax("https://kiosk-notify/", { method: "GET", data: data });
  window.its_notify.push(data);
};

/* document loaded */
$(function () {
  /* decorate all editable text fields with keyboard call */
  $("input[type=text]:not([readonly])")
    .on("focus input click", function () {
      notify_input({
        focus: this.name,
        start:
          this.value.length === 0 ||
          this.selectionStart === 0 ||
          this.value.substr(this.selectionStart - 1, 1) === " ",
      });
    })
    .on("blur", function () {
      notify_input({});
    })
    .on("keydown", function (event) {
      // Make Return on the onscreen keyboard advance to the next
      // field rather than submit the form
      if (event.key === "Enter") {
        let form = $(event.target.form);
        let fields = form.find("input[type=text]:visible:not([readonly])");
        let next = fields.eq(fields.index(event.target) + 1);
        if (next.length) {
          next.focus();
          return false;
        } else {
          form.find("[type=submit]").click() || form.submit();
        }
      }
    });
  /* clear keyboard once document is loaded */
  notify_input({});
});
