"""
Additional functionality not directly related to the
sign in functions of the system.

"""

from datetime import datetime, timedelta

import requests
from bs4 import BeautifulSoup
from cachelib import SimpleCache
from flask import render_template

from ooh.data.configuration import BUS_ROUTES


# Simple in-process cache in case there are many concurrent requests
# but still refresh fairly often
bus_cache = SimpleCache(default_timeout=45)


def bus():
    """
    Bus timetables. Grab data and format into a table.
    """
    url = "http://www.cambridgeshirebus.info/Text/WebDisplay.aspx"

    def parse_time(time_str):
        if "due" in time_str.lower():
            expected_time = datetime.now()
            expected_str = expected_time.strftime("%H:%M")
            live_str = "Due"
        elif "mins" in time_str.lower():
            mins = int(time_str.split()[0])  # XX Mins
            expected_time = datetime.now() + timedelta(minutes=mins)
            expected_str = expected_time.strftime("%H:%M")
            live_str = "{} Mins".format(mins)
        elif ":" in time_str:
            expected_str = time_str
            live_str = ""  # No live data; so don't lie
        else:
            expected_str = ""
            live_str = ""

        return expected_str, live_str

    # Check for a cached version
    bus_routes = bus_cache.get("bus_routes") or []

    if not bus_routes:
        for bus_route in BUS_ROUTES:
            timetable = []
            params = {"stopRef": bus_route["stop_reference"]}
            soup = BeautifulSoup(requests.get(url, params).text, "lxml")

            for row in soup.select("#GridViewRTI tr")[1:]:
                cells = row.select("td")
                expected, live = parse_time(cells[4].text)
                timetable.append(
                    {
                        "service": cells[0].text,
                        "destination": cells[2].text,
                        "expected": expected,
                        "live": live,
                    }
                )

            # Mutating the route, but it's just rewriting the timetable
            # so shouldn't matter too much
            bus_route["timetable"] = timetable
            bus_routes.append(bus_route)
        bus_cache.set("bus_routes", bus_routes)
    else:
        print("cached")

    return render_template("bus.html", bus_routes=bus_routes)


# End goal is to have modules that will be automatically added
# to the left-nav button list, but this is not yet implemented!
extras = {"/bus/": bus}
