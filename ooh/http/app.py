"""
Landing point for application.

Create an application that can be passed to a wsgi server.

Also imports Blueprints from the endpoints and webpages.

"""

import locale

from flask import Flask

from ooh import __version__
from ooh.cli import db_cli, ooh_cli
from ooh.data.configuration import SECRET_KEY
from ooh.http.endpoints import api_blueprint
from ooh.http.views import view_blueprint

# Python ignores LC_ by default, enforce time to be
# system locale
locale.setlocale(locale.LC_TIME, "")


def create_app():
    """Create a flask app to run the service."""
    # The version is appended to the static path so that updated files
    # get sent to the kiosks immediately when they get updated
    app = Flask(__name__, static_url_path="/static/{}".format(__version__))
    app.config["SECRET_KEY"] = SECRET_KEY

    # remove whitespace from templating
    app.jinja_env.lstrip_blocks = True
    app.jinja_env.trim_blocks = True

    # Version identifier available to all templates
    app.jinja_env.globals["VERSION"] = __version__

    # All pages in blueprints
    app.register_blueprint(api_blueprint)
    app.register_blueprint(view_blueprint)

    app.cli.add_command(db_cli)
    app.cli.add_command(ooh_cli)

    return app
