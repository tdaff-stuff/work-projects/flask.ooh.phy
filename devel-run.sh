#!/bin/bash
cd "$(dirname "${0}")"
source ooh-env/bin/activate
export FLASK_APP=ooh.http.app
export FLASK_ENV=development
exec flask run
