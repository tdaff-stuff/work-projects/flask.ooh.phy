#!{{ ooh_venv_dir|default(None) or signage_venv_dir }}/bin/python

"""
Service status checker.

Parse the Apache log files and ensure that kiosks have requested pages
within the expected timeframe. Mark them as "down" if no page requests
have been seen for a while.

Report the output as as checkmk local plugin.

Run the script using the OOH interpreter and credentials and the names
of the kiosks will be guessed when they are identified from the
activity.

The database file may need to be edited manually to indicate non-kiosks,
but should be fine as it's formatted JSON.
"""

import json
import re
from datetime import datetime
from pathlib import Path
from time import time
from typing import Dict, Union

from pygtail import Pygtail

# These are configured by Ansible or can be entered if using
# the script manually.
HOSTNAME = "{{ inventory_hostname }}"  # Which logs to use
SITE_CONFIG = "{{ ooh_dir|default('.') }}/site_config.py"  # OOH config file
SIGNAGE_SETTINGS = "{{ signage_dir|default('.') }}/signage.cfg"  # Signage config file
MODE = "{{ 'ooh' if ooh_src is defined else 'signage' }}"

CACHE_DIR = Path("/var/lib/check_mk_agent/cache/")
LOG_DIR = Path("/var/log/apache2/")


# Apache common log format with no ip lookup and no remote_user
LOG_REGEX = re.compile(r"(?P<ip_address>[0-9.]+) - - \[(?P<time_str>[^]]+)")


def read_database(hostname: str) -> Dict[str, Union[str, float]]:
    """
    Read an existing database of client usage or create a blank
    one. The hostname is used to identify the database file and
    the parsed contents of the file are returned.
    """
    database_name = CACHE_DIR / "{}-clients.json".format(hostname)

    try:
        return json.load(database_name.open())
    except FileNotFoundError:
        # First failure should check cache directory exists
        if not CACHE_DIR.exists():
            CACHE_DIR.mkdir(parents=True)
        return {}


def write_database(database: Dict, hostname: str) -> None:
    """
    Write out the database to a file. Makes a JSON dump of the contents
    of the database to the file named by the hostname.
    """
    database_name = CACHE_DIR / "{}-clients.json".format(hostname)

    # Add indentation to the json so that it is human readable/writeable
    # and write directly to the file.
    database_name.write_text(json.dumps(database, indent=2, sort_keys=True))


def check_kiosk(client: Dict, log_time: float) -> None:
    """
    Determine if a client is acting like a kiosk. Kiosk behaviour is
    set as seeing requests for a page for 24 consecutive hours.
    """
    hour = log_time // 3600
    if "hours" not in client:
        client["hours"] = []

    if hour not in client["hours"]:
        client["hours"].append(hour)

    client["hours"].sort()

    # Not enough data
    if len(client["hours"]) < 25:
        return

    # If the last 24 hour timestamps fall within a 27 hour period then
    # the client is assumed to be an always on kiosk. Designate as such
    # and try to guess the name.
    if client["hours"][-1] - client["hours"][-25] < 27:
        client["is_kiosk"] = True
        if MODE == "ooh":
            client["name"] = get_kiosk_name_from_ooh(client["ip_address"])
        elif MODE == "signage":
            client["name"] = get_kiosk_name_from_signage(client["ip_address"])
        else:
            client["name"] = "Unknown Client"
        # Don't need to keep track of requests any more, remove the data
        del client["hours"]


def get_kiosk_name_from_ooh(ip_address: str) -> str:
    """
    Try to get OOH Kiosk names from the app. Returns a string
    with a description that includes the location and building,
    if there is more than one building. Returns "Unknown" if the
    kiosk is not configured.
    """
    import os

    try:
        # Set the environment to point to the current configuration.
        # Should be using the interpreter from the application so
        # that the configuration can be interpreted correctly.
        os.environ["FLASK_APP"] = "ooh.http.app"
        os.environ["SITE_CONFIG"] = SITE_CONFIG
        from ooh.data.configuration import KIOSKS, BUILDINGS

        # Construct the name according to the number of buildings
        kiosk = KIOSKS.get(ip_address)
        if kiosk is None:
            return "Unknown Kiosk"
        elif len(BUILDINGS) > 1:
            return "{} - {}".format(kiosk.building, kiosk.location)
        else:
            return "{}".format(kiosk.location)

    except (ImportError, KeyError, AttributeError):
        # Mis-configured, wrong app?
        return "Unknown Kiosk"


def get_kiosk_name_from_signage(ip_address: str) -> str:
    """
    Try to get the name of a signage screen from the deckchair
    database. Names are configured using the web interface and will
    be extracted from the database, if they are configured.
    """
    import os

    try:
        # Set the environment to point to the current configuration.
        # Should be using the interpreter from the application so
        # that the configuration can be interpreted correctly.
        os.environ["FLASK_APP"] = "signage.http.app"
        os.environ["SITE_CONFIG"] = SITE_CONFIG
        from flask import current_app
        from deckchair.slides import SlideManager

        # No method to get single screen from ip address yet,
        # so check them all.
        for display in SlideManager(current_app.slide_db).displays():
            if display["ident"] == ip_address:
                return display["name"]
        else:
            return "Unknown Screen"

    except ImportError:
        # Mis-configured, wrong app?
        return "Unknown Screen"


def update_database_from_line(database: Dict, line: str) -> None:
    """
    Parse the contents of the line and update the database with
    details of the kiosk or not kiosk.
    """
    # Parse the client address and access time from the logs
    # skip any lines that don't match.
    log_info = LOG_REGEX.match(line)
    if log_info is None:
        return

    ip_address = log_info.group("ip_address")
    try:
        log_time = datetime.strptime(log_info.group("time_str"), "%d/%b/%Y:%H:%M:%S %z")
    except ValueError:
        # Can't parse the date
        return
    timestamp = int(log_time.timestamp())

    # Ensure the client is known in the database, create
    # an empty entry if not.
    if ip_address not in database:
        client = {"ip_address": ip_address, "last_request": 0}
        database[ip_address] = client
    else:
        client = database[ip_address]

    # Always update the last seen time for reporting.
    if client["last_request"] < timestamp:
        client["last_request"] = timestamp

    # If client isn't a kiosk, check if it should be.
    if "is_kiosk" not in client:
        check_kiosk(client, timestamp)


def print_report(database: Dict, hostname: str) -> None:
    """
    For each kiosk in the database, print out a status indicator
    compatible with the checkmk plugin format.
    """
    now = time()
    for kiosk in database.values():
        if not kiosk.get("is_kiosk"):
            continue

        delta = now - kiosk["last_request"]
        # Get checkmk to dynamically determine the state of the service.
        # pass the exact delta value and thresholds for warn and crit of
        # 180 and 360.
        message = (
            "P "
            "kiosk-{kiosk[ip_address]} "
            "delta={delta:.0f};180;360 "
            "Last request {delta:.0f} seconds ago by '{kiosk[name]}' ({hostname})"
        )
        print(message.format(hostname=hostname, kiosk=kiosk, delta=delta))


def check_clients(hostname: str) -> None:
    """
    Read lines in the log files for the given host and determine which
    clients should be treated as kiosks and whether they are are currently
    requesting pages or not.

    This is the main method run by the script.
    """
    database = read_database(hostname)

    # log reading library used to deal with log rotation and
    # keeping track of already read lines.
    log_file = LOG_DIR / "{}.log".format(hostname)
    offset_file = CACHE_DIR / "{}-log-offset".format(hostname)
    log_reader = Pygtail(str(log_file), offset_file=str(offset_file))

    # Check each line in the log
    for line in log_reader:
        update_database_from_line(database, line)

    # Re-write database as soon as the logs have been processed
    write_database(database, hostname)

    # Output of the plugin
    print_report(database, hostname)


if __name__ == "__main__":
    check_clients(HOSTNAME)
