# Demo configuration
# Create the configuration for your site with the inventory and
# put it in the "files/" directory so that it gets found.
CONFIG = {
    # Whether to enable building sign in
    "ENABLE_SIGN_IN": True,
    # Whether to enable visitor sign in mode
    "ENABLE_VISITOR_SIGN_IN": False,
    # A fixed key is required for parts of the system to work,
    # and can be any string.
    # {% if secret_key is defined %}
    # Leave the templating brackets to use a secret_key defined
    # in the Ansible inventory.
    "SECRET_KEY": "{{ secret_key }}",
    # {% endif %}
    # Components of the name, generally NAME will be emphasised more than
    # the _SUFFIX. Include a space if they need to be separated.
    "SITE_NAME": "Site name",
    "SITE_NAME_SUFFIX": " here",
    #
    # {% if logo_png.stat.exists %}
    # Ansible will copy a file called logo.png, leave this line as-is
    # to let Ansible fill in the path and copy from the files/ directory
    "SITE_LOGO": "{{ ooh_dir }}/logo.png",
    # {% endif %}
    #
    # {% if policy_html.stat.exists %}
    # Ansible will copy a file called policy.html, leave this line as-is
    # to let Ansible fill in the path and copy from the files/ directory
    "SITE_POLICY": "{{ ooh_dir }}/policy.html",
    # {% endif %}
    #
    # Each Kiosk is identified by it's IP address
    # and has a ("Building", "Location", {options}) tuple as the
    # data.
    # If using more than one operation mode, the options can be used to
    # designate the purpose of the kiosk.
    # Use options to set low- or no- contact mode for any terminals.
    #   - "default":   Mode to use when none is selected by the user,
    #                  if unset, will switch during the workday
    #   - "exclusive": Only allow a single mode of operation.
    #   - "contact": Can be set to "low" to reduce the prompts for
    #                interaction or "off" to disable most interactions
    #                on the interface.
    # Valid modes are: "sign-in", "visitor-sign-in", "print".
    "KIOSKS": {
        "123.45.67.1": ("Building 1", "Reception", {"default": "visitor-sign-in"}),
        "123.45.67.2": ("Building 1", "Back door", {"exclusive": "sign-in"}),
        "123.45.67.3": ("Building 2", "Service Entrance", {"contact": "off"}),
        "127.0.0.1": ("Nowhere", "Testing PC", {}),
    },
    # All buildings, in order they should be displayed
    "BUILDINGS": ["Building 1", "Building 2"],
    # Print emails only (no sending)
    "DEBUG_MAIL": True,
    # Contact details for whoever is running the system
    "CONTACT_EMAIL": "contact-email@cam.ac.uk",
    "CONTACT_NAME": "Your Site Occupancy Register",
    # Send undeliverable mail here
    "BOUNCE_EMAIL": "sign-in-bounces@dept.cam.ac.uk",
    # Nag users for missing emergency contact number
    # by sending email reminders
    "NAG_EMERGENCY_PHONE": True,
    # Show a big red warning if any of these fields are empty when
    # completing details for an OOH user
    "HIGHLIGHT_FIELDS": [
        "name",
        "rooms",
        "telephone",
        "emergency_telephone",
        "crsid",
    ],
    # If any of the listed fields are empty, send a
    # nagging email reminder
    "NAG_MISSING_FIELDS": ["name", "rooms"],
    # The start of the day is the time at which all people
    # in the building are marked as signed out. It is a list
    # of times to allow for multiple sign-outs "HH:MM" 24h format
    "START_OF_DAY": ["7:30", "8:30"],
    # Time at which the day ends and people are expected
    # to begin using the terminals. Switches from visitor
    # mode at this time, if enabled.
    "END_OF_DAY": ["18:00"],
    # Any date for which the whole day should be considered out of hours,
    # and will not automatically sign out everyone.
    # Format for dates is "yyyy-mm-dd"
    "NON_WORKDAYS": [
        "2019-12-25",  # Christmas Day
        "2019-12-26",  # Boxing Day
        "2019-12-27",  # Christmas closure (Friday)
        "2019-12-30",  # Christmas closure (Monday)
        "2019-12-31",  # Christmas closure (Tuesday)
        "2020-1-1",  # New Year's Day
        "2020-4-10",  # Good Friday
        "2020-4-13",  # Easter Monday
        "2020-5-8",  # Early May bank holiday
        "2020-5-25",  # Spring bank holiday
        "2020-8-31",  # Summer Bank Holiday
    ],
    # Weekend days where 0 is Monday
    "WEEKEND": [5, 6],
    # IP ranges that are considered on-site and given access to the
    # list of currently signed in people
    # These defaults are all the entire University. Reduce these
    # to just your institution
    "NETWORKS": [
        "128.232.0.0/16",
        "129.169.0.0/16",
        "131.111.0.0/16",
        "192.18.195.0/24",
        "193.60.80.0/20",
        "193.63.252.0/23",
        "192.84.5.0/24",
        "192.153.213.0/24",
        "172.16.0.0/13",
        "172.24.0.0/14",
        "172.28.0.0/15",
        "172.30.0.0/16",
    ],
    # Whether to show signed-in list during the working day,
    # or during out-of-hours periods
    "SHOW_LIST_WORKDAY": True,
    "SHOW_LIST_OOH": True,
    # Allow users to hide their signed in status
    "ENABLE_INVISIBILITY": False,
    # Show events to users and admins
    "ENABLE_TRACKING": False,
    # How long to keep sign-in sign-out events, in days
    "RETENTION": 7,
    # How long to retain inactive profiles, in months
    "RETENTION_PROFILES": 24,
    # CRSid of users that can view additional information such as
    # stats and the system configuration
    "ADMINS": [],
    # Routes to show on the bus timetables
    "BUS_ROUTES": [
        {
            "stop_reference": "0500CCITY422",
            "title": ["Fitzwilliam Museum", "Towards Centre, Eddington"],
        },
        {
            "stop_reference": "0500CCITY421",
            "title": ["Fitzwilliam Museum", "Towards Addenbrookes"],
        },
        {
            "stop_reference": "0500CCITY320",
            "title": ["Regent Street", "Southbound"],
        },
        {
            "stop_reference": "0500CCITY321",
            "title": ["Regent Street", "Northbound"],
        },
    ],
    ##
    # Visitor sign-in options
    ##
    # Show a different site name in visitor mode. Leave empty to
    # keep the same name.
    "VISITOR_SITE_NAME": None,
    "VISITOR_SITE_NAME_SUFFIX": None,
    # Super admins are pre-seeded in the system so they can add other
    # people from the card-reader interface
    "VISITOR_SUPER_ADMIN_MIFARE": [],
    # When interacting using as an admin, how much idle time to wait
    # before automatically logging out (in minutes)
    "SESSION_IDLE_TIMEOUT": 5,
}
